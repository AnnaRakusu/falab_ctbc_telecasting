using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class FALabSettings
{
    public string serverIP;
    public string removeBG_Key;
    public Guest mainGuest;
    public DynamicAdjustment dynamicAdjustment;
    public VisualSetting visualSetting;
}

[System.Serializable]
public class Guest
{
    public string guestName;
    public string gender;
    public string company;
    public string job;
    public string lastVisit;
    public string thisVisit;
}

[System.Serializable]
public class DynamicAdjustment
{
    public string personTime;
    public string slideShowInterval; 
}

[System.Serializable]
public class VisualSetting
{
    public float smileRatio;
    public float quadBrightness;
    public Mode_SingleGuest mode_SingleGuest;
    public Mode_MultiGuest mode_MultiGuest;
    public Mode_SlideShow mode_SlideShow;
    public Mode_Passenger mode_Passenger;
    public ParicleSetting paricleSetting;
 }

#region ModesSetting
[System.Serializable]
public class Mode_SingleGuest
{
    public float targetFrostedValue;
    [Space(15)]
    public float detectAreaMinX;
    public float detectAreaMaxX;
    public float detectAreaMinY;
    public float detectAreaMaxY;
    [Space(15)]
    public float faceDetectionTimeThreshold;
    public string countDownTime;
    public float strokeWidth;
    public int cropDistance;
}

[System.Serializable]
public class Mode_MultiGuest
{
    public float targetFrostedValue;
    [Space(15)]
    public string firstCountDownTime;
    public string pluralCountDownTime;
    public float strokeWidth;
}

[System.Serializable]
public class Mode_SlideShow
{
    public float targetFrostedValue_default;
    public float targetFrostedValue_changed;
}

[System.Serializable]
public class Mode_Passenger
{
    public float targetFrostedValue;
    [Space(15)]
    public float detectAreaMinX;
    public float detectAreaMaxX;
    public float detectAreaMinY;
    public float detectAreaMaxY;
    [Space(15)]
    public float faceDetectionTimeThreshold;
    public float checkPhotoTime;
    [Space(15)]
    public float stagnantLimit01;
    public float stagnantLimit02;
}
#endregion

[System.Serializable]
public class ParicleSetting
{
    public Arc arc;
    public Number number;
    public Reverse reverse;
    public Split split;
}

#region ParticleSetting
[System.Serializable]
public class Arc
{
    public float minTime;
    public float maxTime;
    public int glowingObjectGroupNumber;
}

[System.Serializable]
public class Number
{
    public float minSize;
    public float maxSize;
    public float minEmit;
    public float maxEmit;
}

[System.Serializable]
public class Reverse
{
    public float minSize;
    public float maxSize;
}

[System.Serializable]
public class Split
{
    public float minSize;
    public float maxSize;
}
#endregion

public class SettingClass : MonoBehaviour
{
    [Space(15)]
    [Header("JSON Setting Contents")]
    [Space(5)]
    public FALabSettings falabSettings;
}