using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.Events;

public class SerialSaver : MonoBehaviour
{
    [Header("Running Utilities")]
    public bool isLoadFromStart = true;

    [Header("Setting JSON Name")]
    [Space(5)]
    public string settingName = "DefaultSettings";

    [Header("Setting JSON Setting Component")]
    [Space(5)]
    public SettingClass settingConponent;

    [Header("Loading Completed Callback")]
    public UnityEvent loadingCompleted;

    private void Awake()
    {
        if(isLoadFromStart)
            Load();
    }

    public void Save()
    {
        string saveString = JsonUtility.ToJson(settingConponent.falabSettings, true);

        if (!Directory.Exists(Application.streamingAssetsPath))
        {
            Directory.CreateDirectory(Application.streamingAssetsPath);
        }

        StreamWriter file = new StreamWriter(System.IO.Path.Combine(Application.streamingAssetsPath, settingName + ".json"));
        file.Write(saveString);
        file.Close();

        Debug.Log(" [ SAVING ] : " + settingName + ".json has been saved!");
    }
    
    public void Load()
    {
        string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, settingName + ".json");

        if (!File.Exists(filePath))
        {
            Debug.LogError("Could not find settings file. Please create one or check the json file name in StreamingAssets folder!");
            return;
        }

        StreamReader file = new StreamReader(filePath);
        string loadJson = file.ReadToEnd();
        file.Close();

        settingConponent.falabSettings = JsonUtility.FromJson<FALabSettings> (loadJson);
        Debug.Log(" [ LOADING ] : " + settingName + ".json has been loaded!");

        loadingCompleted.Invoke();
    }
}