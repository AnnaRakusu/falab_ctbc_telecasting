using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SerialSaver))]

public class SerialSaverEditor : Editor
{
    SerialSaver _basedScript;

    public enum SavedTypes
    {
        Numeric,
        String,
        Quaternion,
        Vector3
    }
    
    public SavedTypes savedType;

    void OnEnable()
    {
        //target returns Object, so we cast it
        _basedScript = (SerialSaver)target;
    }

    public override void OnInspectorGUI()
    {
        GUI.backgroundColor = new Color32(160, 231, 229, 255);

        base.OnInspectorGUI();

        GUILayout.Space(15);

        GUI.backgroundColor = new Color32(251, 231, 198, 255);

        if (GUILayout.Button("Save Settings") &&
            EditorUtility.DisplayDialog("請確認是否儲存目前 Inspector 內的 Settings？",
                "點擊儲存後無法復原上一份設定檔案，若不確定請先取消進行確認", "確認", "取消"))
        {
            _basedScript.Save();
        }
        
        GUI.backgroundColor = new Color32(255, 174, 188, 255);

        if (GUILayout.Button("Load Settings From JSON") &&
            EditorUtility.DisplayDialog("請確認是否覆寫目前 Inspector 內的 Settings？",
                "點擊覆寫後無法還原目前 Inspector 的設定檔案，若不確定請先取消進行確認", "確認", "取消"))
        {
            _basedScript.Load();
        }

        //savedType = (SavedTypes)EditorGUILayout.EnumPopup("Display", savedType)

        EditorGUILayout.Space();

        serializedObject.ApplyModifiedProperties();
    }
}
