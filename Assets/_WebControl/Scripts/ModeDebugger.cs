using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class ModeDebugger : MonoBehaviour
{
    public Text modeText;
    public float displayTime = 2;
    public float fadeInOutTime = 1;

    private string tweenID = "ModeName";

    public void ShowModeName(string modeName)
    {
        DOTween.Kill(tweenID);
        modeText.color = new Color(modeText.color.r, modeText.color.g, modeText.color.b, 0);

        modeText.text = modeName;

        modeText.DOFade(1, fadeInOutTime).OnComplete(() =>
        {
            DOVirtual.DelayedCall(displayTime, () => {

                modeText.DOFade(0, fadeInOutTime).SetId(tweenID);

            }).SetId(tweenID);

        }).SetId(tweenID);
    }
}
