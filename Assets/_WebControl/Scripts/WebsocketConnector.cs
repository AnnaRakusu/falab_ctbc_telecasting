using System;
using WebSocketSharp;
using UnityEngine;
using UnityEngine.Events;
using SimpleJSON;
using TMPro;
using DG.Tweening;

[System.Serializable]
public class GuestData
{
    public string name;
    public string gender;
    public string company;
    public string job;
    public string lastVisit;
    public string thisVisit;
}

[System.Serializable]
public class ButtonFunctionalEvents
{
    public string description;
    public UnityEvent calledEvents = new UnityEvent();
}
public class WebsocketConnector : MonoBehaviour
{
    public string serverIp = "192.168.1.101";
    private WebSocket ws;

    public ButtonFunctionalEvents[] callModeChange;
    public ButtonFunctionalEvents[] callActionEvents;

    public int currentMode = 5;

    private ModeDebugger modeDebugger;

    public JSONNode guestData;

    public GuestData currentGuestData;

    public SetWelcomePanel setWelcomePanel;
    public ActivitiesMainController activitiesMainController;
    public SettingClass settingClass;
    public TextMeshProUGUI personTime;

    private void Awake()
    {
        modeDebugger = FindObjectOfType<ModeDebugger>();
    }

    private void Start()
    {
        ConnectToServer();
    }

    private void ConnectToServer()
    {
        ws = new WebSocket("ws://" + serverIp);

        ws.OnOpen += (sender, e) => {
            Debug.Log("Connected to Server!");
            MainThreadWorker.Instance.AddAction(() =>
            {
                DOTween.Kill("Reconnect");
            });
        };

        ws.OnMessage += (sender, e) =>
        {
            MainThreadWorker.Instance.AddAction(() =>
            {
                var receivedJSON = JSON.Parse(e.Data);
                OnMessageReceived(receivedJSON);
            });
        };

        ws.OnClose += (sender, e) =>
        {
            Debug.Log("Server Connection Closed!");
            MainThreadWorker.Instance.AddAction(() =>
            {
                DOVirtual.DelayedCall(0.5f, () =>
                {
                    Debug.Log("Reconnection Tried...");
                    ws = null;
                    ConnectToServer();
                }).SetId("Reconnect");
            });
        };

        ws.Connect();
    }

    #region save server IP to Json - Anna
    public void ReloadServerIP()
    {
        serverIp = settingClass.falabSettings.serverIP;
    }
    #endregion

    private void OnMessageReceived(JSONNode receivedJSON)
    {
        string action = receivedJSON["action"];
        var jsonData = JSON.Parse(receivedJSON["data"]);
        Debug.Log("Action : " + action);
        switch (action)
        {
            case "ChangeMode":
                int modeNumber = (int)jsonData["mode"];
                if (modeNumber == 0)
                {
                    OnModeIn(modeNumber, jsonData["guestData"]);
                    //Debug.LogError(jsonData["guestData"]);
                }
                else
                   OnModeIn(modeNumber);
                break;
            case "CallAction":
                if (jsonData["event"] == "ShowModeName")
                    OnShowModeName();
                else if (jsonData["event"] == "HandsOnStart")
                    OnEventIn_HandsOnNext();
                else if (jsonData["event"] == "ClearSignature")
                    OnEventIn_ClearSignature();
                else if (jsonData["event"] == "FinishSignature")
                    OnEventIn_FinishSignature();
                break;
            case "UpdateInfo":
                if (jsonData["event"] == "MediaTimePeriod")
                    OnMediaTimePeriod(jsonData);
                if (jsonData["event"] == "VistorNumbers")
                    OnVistorNumbers(jsonData);
                break;
        }
    }

    private void OnModeIn(int whichMode)
    {
        Debug.Log("[Mode ChangeRequest] : Request to Mode " + whichMode);
        currentMode = whichMode;
        callModeChange[whichMode].calledEvents.Invoke();
    }

    private void OnModeIn(int whichMode, JSONNode inputGuestData)
    {
        Debug.Log("[Mode ChangeRequest] : Request to Mode " + whichMode);

        currentMode = whichMode;
        guestData = inputGuestData;
        currentGuestData.name = guestData["name"];
        currentGuestData.gender = guestData["gender"];
        currentGuestData.job = guestData["job"];
        currentGuestData.company = guestData["company"];
        currentGuestData.lastVisit = guestData["lastVisit"];
        currentGuestData.thisVisit = guestData["thisVisit"];
        callModeChange[whichMode].calledEvents.Invoke();

        #region save guest data to json
        if (currentMode == 0)
            setWelcomePanel.GetInformation();
        #endregion
    }

    private void OnEventIn_HandsOnNext()
    {
        Debug.Log("[Event CallAction] : OnEventIn_HandsOnNext");
        callActionEvents[0].calledEvents.Invoke();
    }

    private void OnEventIn_ClearSignature()
    {
        Debug.Log("[Event CallAction] : OnEventIn_ClearSignature");
        callActionEvents[1].calledEvents.Invoke();
    }

    private void OnEventIn_FinishSignature()
    {
        Debug.Log("[Event CallAction] : OnEventIn_FinishSignature");
        callActionEvents[2].calledEvents.Invoke();
    }

    private void OnShowModeName()
    {
        modeDebugger.ShowModeName(callModeChange[currentMode].description);
    }

    private void OnMediaTimePeriod(JSONNode inputJson)
    {
        if (currentMode == 0 || currentMode == 1)
            setWelcomePanel.getSlideShowInterval = inputJson["value"];
        
        settingClass.falabSettings.dynamicAdjustment.slideShowInterval = inputJson["value"];
        settingClass.gameObject.GetComponent<SerialSaver>().Save();

        if (currentMode == 4)
            activitiesMainController.imageDuration = float.Parse(inputJson["value"]);
        // Debug.Log("[Update Info] : Time updated to " + inputJson["value"]);
    }

    private void OnVistorNumbers(JSONNode inputJson)
    {
        if (currentMode == 0 || currentMode == 1)
            setWelcomePanel.getPersonTime = inputJson["value"];
        
        settingClass.falabSettings.dynamicAdjustment.personTime = inputJson["value"];
        settingClass.gameObject.GetComponent<SerialSaver>().Save();

        if (currentMode == 4 || currentMode == 5)
            personTime.text = "參觀人次：" + settingClass.falabSettings.dynamicAdjustment.personTime;

        // Debug.Log("[Update Info] : Vistor Numbers updated to " + inputJson["value"]);
    }

    private void OnApplicationQuit()
    {
        ws.Close();
    }

    public void PlayTouchDoorMusic()
    {
        GameObject.Find("Music").GetComponent<MusicController>().PlayTouchDoorMusic();
    }

    public void PlayFinishButtonSoundEffect()
    {
        GameObject.Find("Music").GetComponent<MusicController>().PlayFinishButtonSoundEffect();
    }

    public void ClickButtonSoundEffect()
    {
        GameObject.Find("Music").GetComponent<MusicController>().ClickButtonSoundEffect();
    }

    public void ShouldPlayBgm()
    {
        //GameObject.Find("Music").GetComponent<MusicController>().ShouldPlayBgm();
    }

    public void PassengerModeMusic()
    {
        GameObject.Find("Music").GetComponent<MusicController>().PassengerModeMusic();
    }

    public void UpdatePersonTime()
    {
        personTime.text = "參觀人次：" + settingClass.falabSettings.dynamicAdjustment.personTime;
    }
}