using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;

public class RequestInformationData : MonoBehaviour
{
    public string requestInformationPHP = "http://future-action.com/FALab/CTBCEntryWall/php/RequestIntroductionData.php";

    public string resultJsonString;

    public UnityEvent dataRequestCompletedEvent;
    public UnityEvent dataRequestFailedEvent;

    private void Start()
    {
        CallRequestInformation();
    }

    public void CallRequestInformation()
    {
        StartCoroutine(RequestInformation());
    }

    public IEnumerator RequestInformation()
    {
        UnityWebRequest www = UnityWebRequest.Get(requestInformationPHP);
        yield return www.SendWebRequest();

        if (www.error == null)
        {
            resultJsonString = www.downloadHandler.text;
            Debug.Log(resultJsonString);
            dataRequestCompletedEvent.Invoke();
        }
        else
        {
            Debug.LogError("Request data failed! Please check the network connection! Message ): " + www.downloadHandler.text);
            dataRequestFailedEvent.Invoke();
        }

    }
}
