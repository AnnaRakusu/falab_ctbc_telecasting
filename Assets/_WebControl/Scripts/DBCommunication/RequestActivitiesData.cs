using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;


public class RequestActivitiesData : MonoBehaviour
{
    public string requestActivitiesPHP = "http://future-action.com/FALab/CTBCEntryWall/php/RequestActivitiesData.php";

    public string resultJsonString;

    public UnityEvent dataRequestCompletedEvent;
    public UnityEvent dataRequestFailedEvent;

    private void Start()
    {
        CallRequestActivities();
    }

    public void CallRequestActivities()
    {
        StartCoroutine(RequestActivities());
    }

    public IEnumerator RequestActivities()
    {
        UnityWebRequest www = UnityWebRequest.Get(requestActivitiesPHP);
        yield return www.SendWebRequest();

        if (www.error == null)
        {
            resultJsonString = www.downloadHandler.text;
            Debug.Log(resultJsonString);
            dataRequestCompletedEvent.Invoke();
        }
        else
        {
            Debug.LogError("Request data failed! Please check the network connection! Message ): " + www.downloadHandler.text);
            dataRequestFailedEvent.Invoke();
        }

    }
}
