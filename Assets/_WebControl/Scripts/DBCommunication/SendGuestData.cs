using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;

public class SendGuestData : MonoBehaviour
{
    public string guestName;
    public string gender;
    public string company;
    public string job;
    public string lastVisit;
    public string thisVisit;
    public string imageName;

    public string createGuestUPHP = "http://future-action.com/FALab/CTBCEntryWall/php/UploadGuestData.php";

    public UnityEvent dataUploadedEvent;
    public UnityEvent dataUploadedFailedEvent;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Y))
        {
            CallCreateUser();
        }
    }

    public void CallCreateUser()
    {
        StartCoroutine(CreateUser());
    }

    public IEnumerator CreateUser()
    {
        WWWForm form = new WWWForm();
        form.AddField("name", guestName);
        form.AddField("gender", gender);
        form.AddField("company", company);
        form.AddField("job", job);
        form.AddField("lastVisit", lastVisit);
        form.AddField("thisVisit", thisVisit);
        form.AddField("imageName", imageName);

        UnityWebRequest www = UnityWebRequest.Post(createGuestUPHP, form);
        yield return www.SendWebRequest();

        if (www.error == null)
        {
            Debug.Log("Guest data uploaded! Message ): " + www.downloadHandler.text);
            dataUploadedEvent.Invoke();
        }
        else
        {
            Debug.LogError("Upload data failed! Please check the network connection! Message ): " + www.downloadHandler.text);
            dataUploadedFailedEvent.Invoke();
        }

    }
}
