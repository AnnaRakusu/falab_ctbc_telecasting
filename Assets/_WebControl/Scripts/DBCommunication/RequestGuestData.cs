using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;

public class RequestGuestData : MonoBehaviour
{
    public string requestGuestPHP = "http://future-action.com/FALab/CTBCEntryWall/php/RequestGuestData.php";

    public string resultJsonString;

    public UnityEvent dataRequestCompletedEvent;
    public UnityEvent dataRequestFailedEvent;

    private void Start()
    {
        CallRequestUsers();
    }

    public void CallRequestUsers()
    {
        StartCoroutine(RequestUsers());
    }

    public IEnumerator RequestUsers()
    {
        UnityWebRequest www = UnityWebRequest.Get(requestGuestPHP);
        yield return www.SendWebRequest();

        if (www.error == null)
        {
            resultJsonString = www.downloadHandler.text;
            Debug.Log(resultJsonString);
            dataRequestCompletedEvent.Invoke();
        }
        else
        {
            Debug.LogError("Request data failed! Please check the network connection! Message ): " + www.downloadHandler.text);
            dataRequestFailedEvent.Invoke();
        }

    }
}
