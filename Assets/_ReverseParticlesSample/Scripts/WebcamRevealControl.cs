﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class WebcamRevealControl : MonoBehaviour
{
    public Renderer webcamTextureRenderer;

    private float _MaskAmount;
    private float _SpecularIntensity;
    private float _Ambientlight;

    public float initDissolveShader_SpecularIntensity = 0.5f;
    public float initAmbientlight_SpecularIntensity = 2f;
    
    public float revealTime = 3;

    public float delayedWebcamRevealTime = 4;

    public Ease dissolveEaseType;

    public FALab_ParticleSystemTimeRemap fapstr;

    private WebCamTexture wct;
    public Renderer textureReceiver;
    public DlibFaceLandmarkDetectorExample.FacePointDetection fpd;

    public ParticleSystem[] allPS;
    private ParticleSystem.ShapeModule[] allPSShape;

    public float quadBrightness;
    public SettingClass settingClass;

    // Start is called before the first frame update
    void Start()
    {
        InitDissolveShader();

        //wct = new WebCamTexture(1920, 1080);
        //wct.Play();

        allPSShape = new ParticleSystem.ShapeModule[3];

        for (int i = 0; i < allPS.Length; i++)
        {
            allPSShape[i] = allPS[i].shape;
        }
    }

    private void Update()
    {
        textureReceiver.material.mainTexture = fpd.texture;

        if (Input.GetKeyDown(KeyCode.Q))
        {
            ParticleConverge();
        }
    }

    public void ParticleConverge()
    {
        for (int i = 0; i < allPS.Length; i++)
        {
            Texture2D currentWebcamtexture = new Texture2D(fpd.texture.width, fpd.texture.height);
            currentWebcamtexture.SetPixels(fpd.texture.GetPixels());
            currentWebcamtexture.Apply();

            allPSShape[i] = allPS[i].shape;
            allPSShape[i].texture = currentWebcamtexture;
        }
        fapstr.StartToPlayParticles();
        StartToRevealWebcam();
    }

    void InitDissolveShader()
    {
        webcamTextureRenderer.material.SetFloat("_MaskAmount", 0.24f);
        webcamTextureRenderer.material.SetFloat("_SpecularIntensity", initDissolveShader_SpecularIntensity);
        webcamTextureRenderer.material.SetFloat("_Ambientlight", initAmbientlight_SpecularIntensity);

        _MaskAmount = 0.24f;
        _SpecularIntensity = initDissolveShader_SpecularIntensity;
        _Ambientlight = initAmbientlight_SpecularIntensity;

        quadBrightness = settingClass.falabSettings.visualSetting.quadBrightness;
    }
    
    public void StartToRevealWebcam()
    {
        //﻿﻿﻿﻿﻿﻿﻿DOTween.To(()=> myVector, x=> myVector = x, new Vector3(3,4,8), 1);
        DOVirtual.DelayedCall(delayedWebcamRevealTime, () =>
        {
            DOTween.To(() => _MaskAmount, x => _MaskAmount = x, 1, revealTime).OnUpdate(() =>
            {
                webcamTextureRenderer.material.SetFloat("_MaskAmount", _MaskAmount);
            }).SetEase(dissolveEaseType);

            DOTween.To(() => _SpecularIntensity, x => _SpecularIntensity = x, 0, revealTime).OnUpdate(() =>
            {
                webcamTextureRenderer.material.SetFloat("_SpecularIntensity", _SpecularIntensity);
            }).SetEase(dissolveEaseType);

            DOTween.To(() => _Ambientlight, x => _Ambientlight = x, quadBrightness, revealTime).OnUpdate(() =>
            {
                webcamTextureRenderer.material.SetFloat("_Ambientlight", _Ambientlight);
            }).SetEase(dissolveEaseType);

            //webcamTextureRenderer.material.DOFloat(0.6f, "_EmissiveIntensity", revealTime).From().SetEase(dissolveEaseType);
        });
    }
}
