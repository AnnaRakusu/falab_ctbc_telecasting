using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Http;
using System.IO;
using DG.Tweening;

public class RemoveBackground : MonoBehaviour
{
    //API Key gef7BB6XAJmgDgq3saeHWH2j

    public string _APIKey = "gef7BB6XAJmgDgq3saeHWH2j";

    protected UnityThreading.ActionThread rmbgThread;

    private string dataPath = "";

    public string originalPhoto = "SampleImage/SampleImage.jpg";

    public string outputPhoto = "SampleImage/SampleImage_rmbg.png";

    public Texture2D dealingOverPNG;

    // public Renderer uiImage;

    private FileStream fileStream;

    #region Scripts - Anna
    public FALabScreenSaver fALabScreenSaver;
    public SettingClass settingClass;
    public SetWelcomePanel setWelcomePanel;
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        dataPath = Application.dataPath + "/../";
        _APIKey = settingClass.falabSettings.removeBG_Key;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            MakePhotoRemoveBackground();
        }

        if (Input.GetKeyDown(KeyCode.O))
        {
            OnRemoveBackgroundOver();
        }
    }

    public void MakePhotoRemoveBackground()
    {
        rmbgThread = UnityThreadHelper.CreateThread(Thread_MakePhotoRemoveBackground);
    }

    private void OnRemoveBackgroundOver()
    {
        // Debug.Log("[ RMBG ] Remove Background Success");
        dealingOverPNG = LoadPNG();
        // uiImage.material.mainTexture = dealingOverPNG;
        if (rmbgThread != null)
            rmbgThread.Abort();

        #region Anna
        byte[] fileData;
        string filePath = System.IO.Path.Combine(dataPath, outputPhoto);

        if (!File.Exists(filePath))
            return;

        // Debug.Log(filePath);
        fileData = File.ReadAllBytes(filePath);
        fALabScreenSaver.gameObject.GetComponent<FileUploader>().UploadImage(fileData, setWelcomePanel.getRandomFileName + "_RMBG");

        #endregion
    }

    private void Thread_MakePhotoRemoveBackground()
    {
        using (var client = new HttpClient())
        using (var formData = new MultipartFormDataContent())
        {
            formData.Headers.Add("X-Api-Key", _APIKey);
            formData.Add(new ByteArrayContent(File.ReadAllBytes(System.IO.Path.Combine(dataPath, originalPhoto))), "image_file", "file.jpg");
            formData.Add(new StringContent("auto"), "size");
            var response = client.PostAsync("https://api.remove.bg/v1.0/removebg", formData).Result;

            if (response.IsSuccessStatusCode)
            {
                fileStream = new FileStream(System.IO.Path.Combine(dataPath, outputPhoto), FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
                response.Content.CopyToAsync(fileStream).ContinueWith((copyTask) => {
                    fileStream.Close();
                    // Debug.Log("[ RMBG ] File Saved!");
                    UnityThreadHelper.Dispatcher.Dispatch(() => OnRemoveBackgroundOver());
                });
            }
            else
            {
                Debug.Log("[ RMBG ] Error: " + response.Content.ReadAsStringAsync().Result);
            }
        }
    }

    private Texture2D LoadPNG()
    {
        Texture2D tex = null;
        byte[] fileData;
        string filePath = System.IO.Path.Combine(dataPath, outputPhoto);

        if (File.Exists(filePath))
        {
            // Debug.Log(filePath);
            fileData = File.ReadAllBytes(filePath);
            tex = new Texture2D(2, 2);
            tex.LoadImage(fileData);
            tex.Apply();
        }
        return tex;
    }

    private void OnApplicationQuit()
    {
        if (rmbgThread != null)
            rmbgThread.Abort();
    }
}
