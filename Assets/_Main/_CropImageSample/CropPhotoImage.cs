using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;

public class CropPhotoImage : MonoBehaviour
{
    public RawImage photoImage;
    public Texture2D testTexture;
    public FALabScreenSaver fALabScreenSaver;
    public SettingClass settingClass;

    public int topDistance = 30;

    private void Start()
    {
        topDistance = settingClass.falabSettings.visualSetting.mode_SingleGuest.cropDistance;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
            photoImage.texture = CropImage(testTexture, new Vector2(960, 200));
    }

    public Texture2D CropImage(Texture2D inputTexture, Vector2 faceTopPosition)
    {
        int startY = (int)(faceTopPosition.y - topDistance);
        startY = (startY > 0) ? startY : 0;

        Debug.Log(startY);

        Color[] copiedArea = inputTexture.GetPixels(0, 0, 1920, 1080);
        Texture2D newCroppedTextue = new Texture2D(inputTexture.width, inputTexture.height);

        for (int x = 0; x < inputTexture.width; x++)
        {
            for (int y = 0; y < inputTexture.height; y++)
            {
                if (y < inputTexture.height - startY)
                    continue;

                int index = inputTexture.width * y + x;
                copiedArea[index] = new Color(1, 1, 1, 0);
            }
        }

        newCroppedTextue.SetPixels(copiedArea);
        newCroppedTextue.Apply();

        return newCroppedTextue;

    }

    public void CropImageAndSave(Texture2D inputTexture, Vector2 faceTopPosition)
    {
        testTexture = fALabScreenSaver.tempList[0];

        int startY = (int)(faceTopPosition.y - topDistance);
        startY = (startY > 0) ? startY : 0;

        Debug.Log(startY);

        Color[] copiedArea = inputTexture.GetPixels(0, 0, 1920, 1080);
        Texture2D newCroppedTextue = new Texture2D(inputTexture.width, inputTexture.height);

        for (int x = 0; x < inputTexture.width; x++)
        {
            for (int y = 0; y < inputTexture.height; y++)
            {
                // if (y < inputTexture.height - startY)
                if (y < startY)
                    continue;

                int index = inputTexture.width * y + x;
                copiedArea[index] = new Color(1, 1, 1, 0);
            }
        }

        newCroppedTextue.SetPixels(copiedArea);
        newCroppedTextue.Apply();

        //save file png
        SaveCropPhoto(newCroppedTextue);
        // newCroppedTextue;
    }

    private void SaveCropPhoto(Texture2D texture)
    {
        byte[] bytes = texture.EncodeToPNG();

        string cropPhotoPath = fALabScreenSaver.originalPhotoPath.Replace(".jpg", ".png");
        Debug.Log(cropPhotoPath);

        File.WriteAllBytes(cropPhotoPath, bytes);
    }
}
