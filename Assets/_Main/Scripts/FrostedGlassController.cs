using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class FrostedGlassController : MonoBehaviour
{
    public Image frostedGlass;
    private float frostedValue = 1;
    public float targetFrostedValue = 2;
    public float changeTime = 2f;
    // Start is called before the first frame update
    void Start()
    {
        frostedGlass.material.SetFloat("_Radius", 1);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BlurCity()
    {
        //frostedGlass.material.
        DOTween.To(x => frostedValue = x, frostedGlass.material.GetFloat("_Radius"), targetFrostedValue, changeTime).OnUpdate(() =>
        {
            frostedGlass.material.SetFloat("_Radius", frostedValue);
        });
    }

    public void ReverseBlurCity()
    {
        //frostedGlass.material.
        DOTween.To(x => frostedValue = x, frostedGlass.material.GetFloat("_Radius"), 1, changeTime).OnUpdate(() =>
        {
            frostedGlass.material.SetFloat("_Radius", frostedValue);
        });
    }

    private void OnApplicationQuit()
    {
        frostedGlass.material.SetFloat("_Radius", 1);
    }
}
