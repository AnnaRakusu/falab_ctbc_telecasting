using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using LitJson;

public class FetchVVIP : MonoBehaviour
{
    private string jsonString;
    private JsonData itemData;

    static public List<string> vVipJobTexts = new List<string>();
    static public List<string> vVipPhotoPaths = new List<string>();
    static public List<string> vVipSignaturePaths = new List<string>();

    void Awake()
    {
        //fetch Json from URL
        jsonString = File.ReadAllText("");
        itemData = JsonMapper.ToObject(jsonString);

        //Array Length
        for(int i = 0; i < 5; i++)
        {
            vVipJobTexts.Add(itemData[i].ToString());
        }
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
