using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystem_sea : MonoBehaviour
{
    public ParticleSystem particleSystemSea;
    private ParticleSystem.Particle[] particlesArray;
    public int seaResolution;
    public float spacing;
    public float noiseScale;
    public float heightScale;
    private float perlinNoiseAnimX = 1f;
    private float perlinNoiseAnimY = 1f;
    public Gradient colorGradient;
    void Start()
    {
        particlesArray = new ParticleSystem.Particle[seaResolution * seaResolution];
        particleSystemSea.maxParticles = seaResolution * seaResolution;
        particleSystemSea.Emit(seaResolution * seaResolution);
        particleSystemSea.GetParticles(particlesArray);
}

    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < seaResolution; i++)
        {
            for (int j = 0; j < seaResolution; j++)
            {
                float zPos = Mathf.PerlinNoise(i * noiseScale + perlinNoiseAnimX, j * noiseScale + perlinNoiseAnimY);
                particlesArray[i * seaResolution + j].color = colorGradient.Evaluate(zPos);
                particlesArray[i * seaResolution + j].position = new Vector3(i * spacing, zPos * heightScale, j * spacing);
            }
        }
        perlinNoiseAnimX += 0.01f; perlinNoiseAnimY += 0.01f;
        particleSystemSea.SetParticles(particlesArray, particlesArray.Length);
    }
}
