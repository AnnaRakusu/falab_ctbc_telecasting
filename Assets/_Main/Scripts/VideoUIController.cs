using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using DG.Tweening;
using UnityEngine.UI;

[System.Serializable]
public class VideoKeyFrame
{
    public int InToLoopFrame;
}

public class VideoUIController : MonoBehaviour
{
    [Header("Use Accurate Frame Number")]
    public VideoKeyFrame videoKeyFrame;
    public bool isAdjustAlpha = false;
    public float alphaValue;

    int frame;

    [Header("Simulated Animator Media Players")]
    public MediaPlayer loopMediaPlayer;
    public MediaPlayer outMediaPlayer;

    [Header("Media Players UI")]
    public DisplayUGUI loopMediaPlayerUI;
    public DisplayUGUI outMediaPlayerUI;

    [Header("Settings")]
    public float inFadeInOutTime = 0.5f;
    public float outFadeInOutTime = 0.5f;

    private void Start()
    {
        BindingEventsToVideo();
        InitVideosToAlphaZero();
        DisableRaycast();
    }

    void DisableRaycast()
    {
        loopMediaPlayerUI.raycastTarget = false;
        outMediaPlayerUI.raycastTarget = false;
    }

    void BindingEventsToVideo()
    {
        loopMediaPlayer.Events.AddListener(OnVideoEvent);
    }

    void InitVideosToAlphaZero()
    {
        loopMediaPlayerUI.color = new Color(loopMediaPlayerUI.color.r, loopMediaPlayerUI.color.g, loopMediaPlayerUI.color.b, 0);
        outMediaPlayerUI.color = new Color(outMediaPlayerUI.color.r, outMediaPlayerUI.color.g, outMediaPlayerUI.color.b, 0);
    }
    public void RewindLoop()
    {
        loopMediaPlayer.Control.Rewind();
    }

    public void TriggerIn()
    {
        if (isAdjustAlpha)
        {
            loopMediaPlayerUI.DOFade(alphaValue, inFadeInOutTime).SetEase(Ease.OutSine);
        }
        else
        {
            loopMediaPlayerUI.DOFade(1, inFadeInOutTime).SetEase(Ease.OutSine);
        }
        loopMediaPlayer.Control.Play();
    }

    public void TriggerOut()
    {
        loopMediaPlayerUI.DOFade(0, outFadeInOutTime*3).SetEase(Ease.OutSine).OnComplete(OnLoopVideoOutCompleted);
        if (isAdjustAlpha)
        {        

            outMediaPlayerUI.DOFade(alphaValue, outFadeInOutTime).SetEase(Ease.InSine).OnStart(outMediaPlayer.Control.Play).OnComplete(OnOutVideoOutCompleted);

        }
        else
        {
            outMediaPlayerUI.DOFade(1, outFadeInOutTime).SetEase(Ease.InSine).OnStart(outMediaPlayer.Control.Play).OnComplete(OnOutVideoOutCompleted);
        }
    }

    void OnLoopVideoOutCompleted()
    {
        loopMediaPlayer.Control.Stop();
    }

    void OnOutVideoOutCompleted()
    {

    }

    #region Event Settings

    public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.FinishedPlaying:
                GoBackToLoopInitFrame();
                break;
        }
      // Debug.Log("Event: " + et.ToString());
    }

    void GoBackToLoopInitFrame()
    {
        float frameRate = loopMediaPlayer.Info.GetVideoFrameRate();
        float time = Helper.ConvertFrameToTimeSeconds(videoKeyFrame.InToLoopFrame, frameRate);
        loopMediaPlayer.Control.Seek(time * 1000f);
    }

    #endregion
}
