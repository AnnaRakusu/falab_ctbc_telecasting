using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    public ParticleSystem PS_numbers;
    public ParticleSystem[] PS_arc;

    // Start is called before the first frame update
    void Start()
    {
        InitializeAll();
        StartCoroutine(ParticlePlay());
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    void InitializeAll()
    {
        var numbers_vel = PS_numbers.velocityOverLifetime;
        numbers_vel.enabled = false;
    }

    IEnumerator ParticlePlay()
    {
        yield return new WaitForSeconds(5);

        PS_numbers.Play();
        for(int i = 0; i < PS_arc.Length; i++)
        {
            PS_arc[i].Play();
        }
        yield return new WaitForSeconds(5);
        
        Numbers_Changed();
        yield return new WaitForSeconds(2);

        PS_numbers.Stop();
    }

    void Numbers_Changed()
    {
        var numbers_vel = PS_numbers.velocityOverLifetime;
        numbers_vel.enabled = true;
    }
}
