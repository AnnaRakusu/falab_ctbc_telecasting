using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetWelcomePanel : MonoBehaviour
{
    public WebsocketConnector websocketConnector;
    public SettingClass settingClass;
    public SerialSaver serialSaver;
    public FlowManager_0201 flowManager_0201;
    public FlowManager_0202 flowManager_0202;
    public FlowManager_0203 flowManager_0203;
    public FlowManager_0204 flowManager_0204;

    [Space(15)]
    #region Guest Data
    public string getWelcomeText;
    public string getGuestCompanyText;
    public string getGuestJobText;
    public string getGuestNameText;
    public string getGuestGenderText;
    public string getThisVisitingRecordText;
    public string getLastVisitingRecordText;
    public string getVisitingRecordText;

    public string getRandomFileName;
    public string getPhotoPath;

    [Space(15)]
    public string getSlideShowInterval;
    public string getPersonTime;
    #endregion

    public void GetInformation()
    {
        settingClass.falabSettings.mainGuest.guestName = websocketConnector.currentGuestData.name;
        settingClass.falabSettings.mainGuest.gender = websocketConnector.currentGuestData.gender;
        settingClass.falabSettings.mainGuest.company = websocketConnector.currentGuestData.company;
        settingClass.falabSettings.mainGuest.job = websocketConnector.currentGuestData.job;
        settingClass.falabSettings.mainGuest.lastVisit = websocketConnector.currentGuestData.lastVisit;
        settingClass.falabSettings.mainGuest.thisVisit = websocketConnector.currentGuestData.thisVisit;
        
        serialSaver.Save();
    }

    public void ReloadInfomation()
    {
        if(websocketConnector.currentMode == 0)
        {
            getWelcomeText = "歡迎蒞臨文薈館";
            getGuestCompanyText = settingClass.falabSettings.mainGuest.company;
            getGuestJobText = settingClass.falabSettings.mainGuest.job;
            getGuestNameText = settingClass.falabSettings.mainGuest.guestName;
            getGuestGenderText = settingClass.falabSettings.mainGuest.gender;
            getThisVisitingRecordText = settingClass.falabSettings.mainGuest.thisVisit;
            getLastVisitingRecordText = settingClass.falabSettings.mainGuest.lastVisit;

            flowManager_0201.applicationStatus = FlowManager_0201.ApplicationStatus.Init;
            flowManager_0202.applicationStatus = FlowManager_0202.ApplicationStatus.Init;
            flowManager_0203.applicationStatus = FlowManager_0203.ApplicationStatus.Init;
            flowManager_0204.applicationStatus = FlowManager_0204.ApplicationStatus.Init;

            if (getLastVisitingRecordText.Length <= 1)
                getVisitingRecordText = getThisVisitingRecordText + " 此次蒞臨";
            else
                getVisitingRecordText = getLastVisitingRecordText + " 上次蒞臨\r\n" + getThisVisitingRecordText + " 此次蒞臨";
        }
        else if(websocketConnector.currentMode == 1)
        {
            getWelcomeText = "";
            getGuestCompanyText = "";
            getGuestJobText = "";
            getGuestNameText = "";
            getGuestGenderText = "";
            getThisVisitingRecordText = "";
            getLastVisitingRecordText = "";
            getVisitingRecordText = "";

            flowManager_0201.applicationStatus = FlowManager_0201.ApplicationStatus.Init;
            flowManager_0202.applicationStatus = FlowManager_0202.ApplicationStatus.Init;
            flowManager_0203.applicationStatus = FlowManager_0203.ApplicationStatus.Init;
            flowManager_0204.applicationStatus = FlowManager_0204.ApplicationStatus.Init;
        }
    }
}