using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using DG.Tweening;
using UnityEngine.UI;

public class FlowManager_0302 : MonoBehaviour
{
    #region Process Management
    public enum ApplicationStatus
    {
        Init,
        SignTime,
        SaveSignature
    }
    public ApplicationStatus applicationStatus = ApplicationStatus.Init;
    #endregion

    #region Scripts
    public FALabScreenSaver fALabScreenSaver;
    public SettingClass settingClass;
    public PanelManager_03 panelManager_03;
    public FlowManager_0303 flowManager_0303;
    public UsefulFunc usefulFunc;
    public GraphicRaycasterRaycasterExample grre;
    #endregion

    #region GameObjects
    public Camera signCam;
    public GameObject signaturePrefab;
    public VideoUIController video_Background;
    private int strokeCount;
    private List<GameObject> Signatures = new List<GameObject>();
    private List<TrailRenderer> TrailRenderers = new List<TrailRenderer>();
    public RawImage rawImage;
    public ParticleSystem splitParticle;
    public GameObject finishSignQuad;
    #endregion

    #region UserActBool
    private bool isFirstMouseDown = true;
    #endregion

    private void Awake()
    {
        applicationStatus = ApplicationStatus.Init;
    }
    void Start()
    {
        strokeCount = -1;
    }

    void Update()
    {
        if (panelManager_03.canDraw)
            MouseAction();

        if (panelManager_03.panelTrigger == PanelManager_03.PanelTrigger.SignPanel)
        {
            applicationStatus = ApplicationStatus.SignTime;
            WhichFlow();
        }

        #region test
        if (Input.GetKeyDown(KeyCode.Z) && panelManager_03.canDraw)
            ClearSignCanvas();
        else if (Input.GetKeyDown(KeyCode.X) && panelManager_03.canDraw)
            SaveSignature();
        #endregion
    }

    #region Init
    public void InitializeAll()
    {
        ClearSignCanvas();
        var tempColor = new Color(255, 255, 255, 0);
        rawImage.color = tempColor;
        splitParticle.Stop(true, ParticleSystemStopBehavior.StopEmittingAndClear);

        // stroke width
        AnimationCurve curve = new AnimationCurve();
        curve.AddKey(0f, settingClass.falabSettings.visualSetting.mode_SingleGuest.strokeWidth);
        curve.AddKey(1f, settingClass.falabSettings.visualSetting.mode_SingleGuest.strokeWidth);
        signaturePrefab.GetComponent<TrailRenderer>().widthCurve = curve;
    }
    #endregion

    #region step 1 - sign
    void MouseAction()
    {
        if (Input.GetMouseButtonUp(0))
            isFirstMouseDown = true;

        if (!grre.isSignaturePanelTouched)
            return;

        if (Input.GetKey(KeyCode.Mouse0))
        {
            Vector3 newTrailPosition = signCam.ScreenToWorldPoint(Input.mousePosition);

            if (isFirstMouseDown)
            {
                strokeCount ++;
                isFirstMouseDown = false;

                //instantiate a stroke
                GameObject g = Instantiate(signaturePrefab, newTrailPosition, Quaternion.identity) as GameObject;
                Signatures.Add(g);
                TrailRenderers.Add(g.GetComponent<TrailRenderer>());
            }
            //update position when draging
            TrailRenderers[strokeCount].transform.position = new Vector3(newTrailPosition.x, newTrailPosition.y, 0);
        }
    }

    public void ClearSignCanvas()
    {
        if (applicationStatus != ApplicationStatus.SignTime && applicationStatus != ApplicationStatus.SaveSignature)
            return;

        for (int j = 0; j < Signatures.Count; j++)
        {
            Destroy(Signatures[j]);
        }
    }
    #endregion

    #region step 2 - save signature
    public void SaveSignature()
    {
        if (applicationStatus != ApplicationStatus.SignTime)
            return; 

        if (!fALabScreenSaver.gameObject.activeInHierarchy)
            return;

        applicationStatus = ApplicationStatus.SaveSignature;

        // make photo
        fALabScreenSaver.MakePhoto();
        fALabScreenSaver.SaveAllTheFinalToTheDiskFolder();
        rawImage.color = new Color(255, 255, 255, 255);
        rawImage.texture = fALabScreenSaver.tempList[0];
        finishSignQuad.GetComponent<Renderer>().material.mainTexture = fALabScreenSaver.tempList[0];
        ClearSignCanvas();

        // signature split
        var shape = splitParticle.shape;
        shape.texture = fALabScreenSaver.tempList[0];
        splitParticle.Play();
        rawImage.DOFade(0f, 1f).SetDelay(0.2f).SetEase(Ease.OutCubic);
        splitParticle.GetComponent<Renderer>().material.DOVector(new Vector4(255, 255, 255) * 0.03f, "_EmissionColor", 5f).SetEase(Ease.InQuint);
        StartCoroutine(NextFlow());
    }

    IEnumerator NextFlow()
    {
        yield return new WaitForSeconds(1.5f);
        panelManager_03.panelTrigger = PanelManager_03.PanelTrigger.CoolCardPanel;
        video_Background.TriggerOut();
        video_Background.gameObject.transform.parent.GetChild(1).gameObject.GetComponent<DisplayUGUI>().DOFade(0f, 1f);
        yield return new WaitForSeconds(1f);
        flowManager_0303.applicationStatus = FlowManager_0303.ApplicationStatus.Init;
    }
    #endregion
    
    private void WhichFlow()
    {
        switch (applicationStatus)
        {
            case ApplicationStatus.Init:
                InitializeAll();
                panelManager_03.canDraw = false;
                break;

            case ApplicationStatus.SignTime:
                panelManager_03.canDraw = true;
                break;

            case ApplicationStatus.SaveSignature:
                panelManager_03.canDraw = false;
                break;

            default:
                break;
        }
    }
}
