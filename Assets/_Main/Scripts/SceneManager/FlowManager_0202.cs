using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;

public class FlowManager_0202 : MonoBehaviour
{
    #region Process Management
    public enum ApplicationStatus
    {
        Init,
        FirstPhotoIn,
        FirstPhoto,
        PluralPhoto,
        CheckPhoto,
        SavePhoto,
        Idle
    }
    public ApplicationStatus applicationStatus = ApplicationStatus.Init;
    #endregion

    #region Scripts
    [Space(15)]
    public WebsocketConnector websocketConnector;
    public FALabScreenSaver fALabScreenSaver;
    public SettingClass settingClass;
    public PanelManager_02 panelManager_02;
    public SetWelcomePanel setWelcomePanel;
    public UsefulFunc usefulFunc;
    public DlibFaceLandmarkDetectorExample.FacePointDetection facePointDetection;
    public CTBCFaceDealer cTBCFaceDealer;
    public CropPhotoImage cropPhotoImage;
    public RemoveBackground removeBackground;
    #endregion

    #region Cameras and Render Textures
    [Space(15)]
    public Image mask;
    public RawImage rawImage;
    public Camera renderCam;
    public RenderTexture renderCamTex;
    #endregion

    #region Bools
    [Space(15)]
    public bool shouldPlayBG;
    private bool isHere = false;
    private bool isSmiling = false;
    public bool isAnimInDone = false;
    public bool startEnergyBar = false;
    private float faceDetectionTime = 0;
    public float faceDetectionTimeThreshold;
    public bool isPhotoTaken = false;
    #endregion

    #region GameObjects
    [Space(15)]
    public VideoUIController background;
    public List<VideoUIController> videos;
        // videos[0] -> Video_FaceScaning
        // videos[1] -> Video_Deco03
    public MediaPlayer video_smileEnergyBar;
    public Animator image_SmileHint;
    private Sequence seqIn;
    private Sequence seqOut;
    private int energyBarStart = 710;
    private int energyBarEnd = 7100;

    [Space(15)]
    public GameObject[] GameObjectFollowFace = new GameObject [2];
    private Vector3 nosePose = new Vector3();
    public Vector2 shotMomentEyebowPos;

    [Space(15)]
    public TextMeshProUGUI guestCompany;
    public TextMeshProUGUI guestJob;
    public TextMeshProUGUI guestName;
    public TextMeshProUGUI guestGender;
    public TextMeshProUGUI visitingRecordTitle;
    public TextMeshProUGUI visitingRecordText;

    [Space(15)]
    public RawImage cameraFlash;
    public TextMeshProUGUI countDownText;
    public Button cameraAgain;
    public Button cameraFinish;

    [Space(15)]
    public GameObject destroyObj;
    #endregion

    private void Awake()
    {
        applicationStatus = ApplicationStatus.Init;
    }

    void Update()
    {
        WhichFlow();
        this.gameObject.GetComponent<Canvas>().additionalShaderChannels = AdditionalCanvasShaderChannels.None;
        
        if (panelManager_02.shouldDetect)
        {
            LoadFaceInformation();
            DetectSmile();
        }
    }

    #region Init
    public void InitializeAll()
    {
        SetGuestInformation();
        
        rawImage.texture = renderCamTex;

        isHere = false;
        isSmiling = false;
        isAnimInDone = false;
        shouldPlayBG = false;

        var tempColor = Color.white;
        tempColor.a = 0;
        mask.color = tempColor;
        rawImage.color = tempColor;

        cameraAgain.gameObject.transform.localScale = new Vector3(0f, 0f, 0f);
        cameraFinish.gameObject.transform.localScale = new Vector3(0f, 0f, 0f);
        countDownText.text = settingClass.falabSettings.visualSetting.mode_SingleGuest.countDownTime;
        countDownText.gameObject.SetActive(false);
        
        video_smileEnergyBar.Events.AddListener(OnVideoEvent);

        faceDetectionTimeThreshold = settingClass.falabSettings.visualSetting.mode_SingleGuest.faceDetectionTimeThreshold;

        countDownText.gameObject.SetActive(false);

        InitializeAnim();
    }

    public void InitializeAnim()
    {
        seqIn = DOTween.Sequence();
        seqOut = DOTween.Sequence();

        var tempColor = Color.white;
        tempColor.a = 0;
        guestCompany.color = tempColor;
        guestJob.color = tempColor;
        guestName.color = tempColor;
        guestGender.color = tempColor;
        visitingRecordTitle.color = tempColor;
        visitingRecordText.color = tempColor;
        visitingRecordTitle.transform.localPosition = new Vector3(1200, -286, 0);
        visitingRecordText.transform.localPosition = new Vector3(1200, -436, 0);

        image_SmileHint.gameObject.GetComponent<Image>().color = tempColor;
        image_SmileHint.ResetTrigger("in");
        image_SmileHint.ResetTrigger("out");
    }

    // get guest information from <SetWelcomePanel>
    private void SetGuestInformation()
    {
        guestCompany.text = setWelcomePanel.getGuestCompanyText;
        guestJob.text = setWelcomePanel.getGuestJobText;
        guestName.text = "";
        guestGender.text = setWelcomePanel.getGuestGenderText;
        visitingRecordText.text = setWelcomePanel.getVisitingRecordText;

        if (websocketConnector.currentMode == 0)
            visitingRecordTitle.text = "�ѳX����";
        else if (websocketConnector.currentMode == 1)
            visitingRecordTitle.text = "";
    }
    #endregion

    #region step 1 - play video...
    private void VideoPlaying()
    {
        if (shouldPlayBG)
            {
                shouldPlayBG = false;
                StartCoroutine(OpenCamera());
            }
    }

    IEnumerator OpenCamera()
    {
        background.TriggerIn();

        yield return new WaitForSeconds(1f);
        mask.DOFade(1f, 1f);
        rawImage.DOFade(1f, 1f);

        yield return new WaitForSeconds(0.5f);
        panelManager_02.shouldDetect = true;
    }
    #endregion

    #region step 2 - panelManager_02.shouldDetect: smile analyzing
    private void DetectSmile()
    {
        //when guest's face detected
        if (applicationStatus == ApplicationStatus.FirstPhoto)
        {
            if (isHere)
            {
                // just do once
                if (!isAnimInDone)
                {
                    isAnimInDone = true;
                    StartCoroutine(AnimIn());
                }

                if (startEnergyBar)
                {
                    faceDetectionTime += Time.deltaTime;

                    if (isSmiling || 
                        video_smileEnergyBar.Control.GetCurrentTimeMs() < energyBarStart || video_smileEnergyBar.Control.GetCurrentTimeMs() > energyBarEnd || 
                        faceDetectionTime > faceDetectionTimeThreshold)
                    {
                        video_smileEnergyBar.Control.Play();
                        if (video_smileEnergyBar.Control.GetCurrentTimeMs() > energyBarEnd && !isPhotoTaken)
                        {
                            isPhotoTaken = true;
                            DOTween.Kill("flash");
                            usefulFunc.FadeAndBack(cameraFlash, 0.5f, 0.2f);
                            PhotoTaking();
                        }
                    }
                    else if (!isSmiling)
                        video_smileEnergyBar.Control.Pause();
                }
            }
            else if (!isHere)
            {
                video_smileEnergyBar.Control.Pause();
                video_smileEnergyBar.Control.Rewind();
                faceDetectionTime = 0;

                if (isAnimInDone)
                {
                    isAnimInDone = false;
                    AnimOut();
                }
            }
        }
    }

    // get and calc the information from <FacePointDetection>
    private void LoadFaceInformation()
    {
        isHere = cTBCFaceDealer.isHere;
        isSmiling = cTBCFaceDealer.isSmiling;
        nosePose = cTBCFaceDealer.realNosePos;
        //Debug.Log(facePos);
        if(applicationStatus == ApplicationStatus.FirstPhoto)
        {
            DOTween.Kill("GameObjectFollowFace");
            GameObjectFollowFace[0].transform.DOLocalMove(new Vector3(nosePose.x, nosePose.y, nosePose.z), 1.2f).SetId("GameObjectFollowFace");
            GameObjectFollowFace[1].transform.DOLocalMove(new Vector3(nosePose.x, nosePose.y, nosePose.z), 1.2f).SetId("GameObjectFollowFace");
        }
    }
    #endregion

    void PhotoTaking()
    {
        shotMomentEyebowPos.Set(cTBCFaceDealer.realEyebrowPos.x, cTBCFaceDealer.realEyebrowPos.y);
        Debug.Log(shotMomentEyebowPos);

        image_SmileHint.ResetTrigger("in");
        image_SmileHint.ResetTrigger("out");

        if(applicationStatus == ApplicationStatus.FirstPhoto)
        {
            usefulFunc.ScaleObj(GameObjectFollowFace[0].transform.GetChild(0).gameObject, 0f, 0.5f, Ease.InBack);
            usefulFunc.ScaleObj(GameObjectFollowFace[1].transform.GetChild(0).gameObject, 0f, 0.5f, Ease.InBack);
        }

        applicationStatus = ApplicationStatus.CheckPhoto;

        image_SmileHint.SetTrigger("out");
        usefulFunc.FadeImg(image_SmileHint.gameObject.GetComponent<Image>(), 0f, 0.5f, "taking", Ease.InOutCubic, PhotoTakingOut);

        if (cameraFlash.color.a != 0)
            cameraFlash.DOFade(0f, 0.1f);

        // if rePhoto, clear the list
        fALabScreenSaver.tempList.Clear();
        fALabScreenSaver.savedNameList.Clear();

        // screenshot
        fALabScreenSaver.MakePhoto();

        rawImage.texture = fALabScreenSaver.tempList[fALabScreenSaver.tempList.Count - 1];
        renderCam.targetTexture = renderCamTex;
    }

    void PhotoTakingOut()
    {
        usefulFunc.ScaleObj(cameraAgain.gameObject, 1f, 0.5f);
        usefulFunc.ScaleObj(cameraFinish.gameObject, 1f, 0.5f);

    }

    #region step 2 - FirstPhotoFirstIn -> FirstPhotoPluralIn
    IEnumerator AnimIn()
    {
        InitializeAnim();

        if (websocketConnector.currentMode == 0)
        {
            video_smileEnergyBar.Control.Rewind();
            video_smileEnergyBar.gameObject.GetComponent<DisplayUGUI>().DOFade(1f, 1f).SetEase(Ease.InSine);
        
            yield return new WaitForSeconds(1f);
            videos[0].TriggerIn();
        
            yield return new WaitForSeconds(1.8f);
            usefulFunc.FadeImg(image_SmileHint.gameObject.GetComponent<Image>(), 1f, 0.5f, "in");
            image_SmileHint.SetTrigger("in");

            yield return new WaitForSeconds(1f);
            if(websocketConnector.currentMode == 0)
                videos[1].TriggerIn();

            DOTween.Kill("faceUIAnim");
            seqIn = DOTween.Sequence().SetId("faceUIAnim");
            seqIn.Append(guestCompany.DOFade(1f, 1.2f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
                .Join(guestJob.DOFade(1f, 1.2f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
                .Append(guestName.DOText(setWelcomePanel.getGuestNameText, 1.2f).SetEase(Ease.OutSine).SetDelay(0.5f).SetId("faceUIAnim"))
                .Join(guestName.DOFade(1f, 0.5f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
                .Append(guestGender.DOText(setWelcomePanel.getGuestGenderText, 0.2f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
                .Join(guestGender.DOFade(1f, 0.2f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
                .Append(visitingRecordTitle.DOFade(1f, 0.75f).SetEase(Ease.OutSine).SetDelay(0.5f).SetId("faceUIAnim"))
                .Join(visitingRecordTitle.transform.DOLocalMove(new Vector3(1200, -336, 0), 1f).From().SetEase(Ease.OutBack).SetId("faceUIAnim"))
                .Join(visitingRecordText.DOFade(1f, 1.5f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
                .Join(visitingRecordText.transform.DOLocalMove(new Vector3(1200, -486, 0), 1f).From().SetEase(Ease.OutBack).SetId("faceUIAnim"))
                .AppendInterval(0.5f).SetEase(Ease.OutSine).SetId("faceUIAnim").OnComplete(SetStartEnergyBar);
        }
        else if(websocketConnector.currentMode == 1)
        {
            videos[0].gameObject.transform.parent.GetChild(0).GetComponent<MediaPlayer>().Control.Rewind();
            videos[0].gameObject.transform.parent.GetChild(1).GetComponent<MediaPlayer>().Control.Rewind();

            video_smileEnergyBar.Control.Rewind();
            video_smileEnergyBar.gameObject.GetComponent<DisplayUGUI>().DOFade(1f, 1f).SetEase(Ease.InSine);

            yield return new WaitForSeconds(1f);
            videos[0].TriggerIn();

            yield return new WaitForSeconds(1.8f);
            usefulFunc.FadeImg(image_SmileHint.gameObject.GetComponent<Image>(), 1f, 0.5f, "faceUIAnim");
            image_SmileHint.SetTrigger("in");

            yield return new WaitForSeconds(1f);
            SetStartEnergyBar();
        }

        
    }

    void SetStartEnergyBar()
    {
        startEnergyBar = true;
    }


    void AnimOut()
    {
        StopCoroutine("AnimIn");
        DOTween.Kill("faceUIAnim");

        faceDetectionTime = 0;
        startEnergyBar = false;
        
        videos[0].TriggerOut();
        if (websocketConnector.currentMode == 0)
            videos[1].TriggerOut();

        for (int i = 0; i < videos.Count; i++)
        {
            videos[i].gameObject.transform.parent.GetChild(0).GetComponent<MediaPlayer>().Control.Rewind();
            videos[i].gameObject.transform.parent.GetChild(1).GetComponent<MediaPlayer>().Control.Rewind();
        }

        image_SmileHint.SetTrigger("out");
        usefulFunc.FadeImg(image_SmileHint.gameObject.GetComponent<Image>(), 0f, 0.5f, "faceUIAnim", Ease.InOutCubic);
        video_smileEnergyBar.Control.Stop();

        seqOut = DOTween.Sequence().SetId("faceUIAnim");
        seqOut.Append(video_smileEnergyBar.gameObject.GetComponent<DisplayUGUI>().DOFade(0f, 0.5f).SetEase(Ease.OutCubic).SetId("faceUIAnim"))
            .Join(guestCompany.DOFade(0f, 0.5f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(guestJob.DOFade(0f, 0.5f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(guestName.DOFade(0f, 1f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(guestGender.DOFade(0f, 1f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(visitingRecordTitle.DOFade(0f, 1.5f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(visitingRecordText.DOFade(0f, 1.5f).SetEase(Ease.OutSine).SetId("faceUIAnim")).SetId("faceUIAnim");
    }
    #endregion

    #region step 3 - PluralPhoto: rephoto
    void CountDown()
    {
        if (int.Parse(countDownText.text) > 1)
        {            
            DOTween.Kill("count");
            usefulFunc.ScaleAndBack(countDownText.gameObject, 1.2f, 1f, null, "count");
            countDownText.gameObject.SetActive(true);
            countDownText.text = (int.Parse(countDownText.text) - 1).ToString();
        }
        else if (int.Parse(countDownText.text) == 1)
        {
            usefulFunc.FadeAndBack(cameraFlash, 0.5f, 0.2f);
            PhotoTaking();
            CancelInvoke("CountDown");
            countDownText.gameObject.SetActive(false);
            countDownText.text = settingClass.falabSettings.visualSetting.mode_SingleGuest.countDownTime;
        }
    }
    #endregion

    #region Buttons
    // when guest press the again button - count down from three and take photo
    public void RePhotoBtn()
    {
        applicationStatus = ApplicationStatus.PluralPhoto;
        
        usefulFunc.ScaleAndBack(cameraAgain.gameObject, 1.1f, 0.5f, RePhotoBtnOut);

        Destroy(GameObjectFollowFace[0]);
        Destroy(GameObjectFollowFace[1]);

        rawImage.texture = renderCamTex;
        InvokeRepeating("CountDown", 1, 1);
    }

    public void RePhotoBtnOut()
    {
        usefulFunc.ScaleObj(cameraAgain.gameObject, 0f, 0.5f);
        usefulFunc.ScaleObj(cameraFinish.gameObject, 0f, 0.5f);
        usefulFunc.FadeImg(image_SmileHint.gameObject.GetComponent<Image>(), 1f, 0.5f, null);
        image_SmileHint.SetTrigger("in");
    }

    // Panel Out - when guest press the finish button
    public void FinishPhotoBtn()
    {
        applicationStatus = ApplicationStatus.SavePhoto;
        panelManager_02.shouldDetect = false;
        usefulFunc.ScaleAndBack(cameraFinish.gameObject, 1.1f, 1f, FinishPhotoBtnOut);

        DOTween.Kill("faceUIAnim");
        usefulFunc.FadeImg(image_SmileHint.gameObject.GetComponent<Image>(),  0f, 0.5f ,"faceUIAnim");
        image_SmileHint.SetTrigger("out");
        Destroy(GameObjectFollowFace[0]);
        Destroy(GameObjectFollowFace[1]);

        // save photo
        fALabScreenSaver.SaveAllTheFinalToTheDiskFolder();

        // remove.bg set path
        string cropPhotoPath = fALabScreenSaver.originalPhotoPath.Replace(".jpg", ".png");
        Debug.Log(cropPhotoPath);
        cropPhotoImage.CropImageAndSave(fALabScreenSaver.tempList[0], shotMomentEyebowPos);
        removeBackground.originalPhoto = cropPhotoPath;
        removeBackground.outputPhoto = fALabScreenSaver.rmbgPhotoPath;
        removeBackground.MakePhotoRemoveBackground();

        // save path and randomName
        setWelcomePanel.getPhotoPath = fALabScreenSaver.rmbgPhotoPath;
        string[] randomName = fALabScreenSaver.savedNameList[fALabScreenSaver.savedNameList.Count - 1].Split('.');
        setWelcomePanel.getRandomFileName = randomName[0];

        // this flow out
        rawImage.DOFade(0f, 1f).SetEase(Ease.InCubic);
        mask.DOFade(0f, 1f).SetEase(Ease.InCubic).OnComplete(NextFlow);
        mask.transform.DOScale(new Vector3(0f, 0f, 0f), 2f).SetEase(Ease.InBack);
        Destroy(destroyObj);
    }

    public void FinishPhotoBtnOut()
    {
        usefulFunc.ScaleObj(cameraAgain.gameObject, 0f, 0.5f);
        usefulFunc.ScaleObj(cameraFinish.gameObject, 0f, 0.5f);
    }

    void NextFlow()
    {
        panelManager_02.panelTrigger = PanelManager_02.PanelTrigger.SignPanel;
        applicationStatus = ApplicationStatus.Init;
    }
    #endregion

    public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.FinishedPlaying:
                
                break;
        }
        // Debug.Log("Event: " + et.ToString());
    }



    private void WhichFlow()
    {
        switch (applicationStatus)
        {
            case ApplicationStatus.Init:
                applicationStatus = ApplicationStatus.Idle;
                InitializeAll();
                break;

            case ApplicationStatus.FirstPhotoIn:
                applicationStatus = ApplicationStatus.FirstPhoto;
                VideoPlaying();
                break;

            case ApplicationStatus.FirstPhoto:
                break;

            case ApplicationStatus.PluralPhoto:
                break;

            case ApplicationStatus.CheckPhoto:
                break;

            case ApplicationStatus.Idle:
                break;

            default:
                break;
        }
    }
}