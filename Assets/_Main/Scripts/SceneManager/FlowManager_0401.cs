using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.PostProcessing;

public class FlowManager_0401 : MonoBehaviour
{
    #region Process Management
    public enum ApplicationStatus
    {
        Init,
        SignTime,
        SaveSignature
    }
    public ApplicationStatus applicationStatus = ApplicationStatus.Init;
    #endregion

    #region Scripts
    public FALabScreenSaver fALabScreenSaver;
    public SettingClass settingClass;
    public ModeManager modeManager;
    public PanelManager_04 panelManager_04;
    public GraphicRaycasterRaycasterExample grre;
    public FrostedGlassController frostedGlassController;
    public UsefulFunc usefulFunc;
    #endregion

    #region GameObjects
    public Camera signCam;
    public GameObject signaturePrefab;
    public VideoUIController video_Background;
    public MediaPlayer MD_Background;
    public GameObject[] particles = new GameObject[5];
    private int strokeCount;
    public List<GameObject> Signatures = new List<GameObject>();
    private List<TrailRenderer> TrailRenderers = new List<TrailRenderer>();
    public RawImage rawImage;
    public GameObject finishSignQuad;
    public PostProcessingProfile postProcessingProfile;
    public Image blurImage;
    public Image logo;
    #endregion

    #region Bool
    private bool shouldPlayBG = false;
    private bool isFirstMouseDown = true;
    #endregion

    private void Awake()
    {
        applicationStatus = ApplicationStatus.Init;
    }
    void Start()
    {
        StartCoroutine(PlayBG());
        strokeCount = -1;
    }

    void Update()
    {
        WhichFlow();
        
        if (shouldPlayBG)
        {
            shouldPlayBG = false;
            StartCoroutine(PlayBG());
        }
        
        if(applicationStatus == ApplicationStatus.SignTime)
            MouseAction();

        #region test
        if (Input.GetKeyDown(KeyCode.Z) && panelManager_04.canDraw)
            ClearSignCanvas();
        else if (Input.GetKeyDown(KeyCode.X) && panelManager_04.canDraw)
            SaveSignature();
        #endregion
    }

    public void InitializeAll()
    {
        ClearSignCanvas();
        var tempColor = new Color(255, 255, 255, 0);
        rawImage.color = tempColor;
        blurImage.color = tempColor;
        logo.color = tempColor;

        InitializePartical();
        shouldPlayBG = true;

        video_Background.transform.parent.GetChild(0).GetComponent<MediaPlayer>().Events.AddListener(OnVideoEvent);
    }

    private void InitializePartical()
    {
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().glowingObjectGroupNumber = settingClass.falabSettings.visualSetting.paricleSetting.arc.glowingObjectGroupNumber;
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().effectRandomTime.minTime = settingClass.falabSettings.visualSetting.paricleSetting.arc.minTime;
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().effectRandomTime.maxTime = settingClass.falabSettings.visualSetting.paricleSetting.arc.maxTime;

        var mainNumber = particles[3].GetComponent<ParticleSystem>().main;
        mainNumber.startSize = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.number.minSize, settingClass.falabSettings.visualSetting.paricleSetting.number.maxSize);
        var emissionNumber = particles[3].GetComponent<ParticleSystem>().emission;
        emissionNumber.rateOverTime = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.number.minEmit, settingClass.falabSettings.visualSetting.paricleSetting.number.maxEmit);
    }

    IEnumerator PlayBG()
    {
        yield return new WaitForSeconds(3);

        video_Background.TriggerIn();
        logo.DOFade(1f, 2f);

        yield return new WaitForSeconds(0.3f);
        usefulFunc.SetAOZero(postProcessingProfile, blurImage);
        frostedGlassController.BlurCity();
        
        applicationStatus = ApplicationStatus.SignTime;
        yield return new WaitForSeconds(1);
    }

    void MouseAction()
    {
        if (Input.GetMouseButtonUp(0))
        {
            isFirstMouseDown = true;

            // Debug.Log(TrailRenderers);
            // Debug.Log(strokeCount);
        }

        if (!grre.isSignaturePanelTouched)
            return;
    
        if (Input.GetKey(KeyCode.Mouse0))
        {
            Vector3 newTrailPosition = signCam.ScreenToWorldPoint(Input.mousePosition);

            if (isFirstMouseDown)
            {
                strokeCount++;
                isFirstMouseDown = false;

                //instantiate a stroke
                GameObject g = Instantiate(signaturePrefab, newTrailPosition, Quaternion.identity) as GameObject;
                Signatures.Add(g);
                TrailRenderers.Add(g.GetComponent<TrailRenderer>());
            }
            //update position when draging
            TrailRenderers[strokeCount].transform.position = new Vector3( newTrailPosition.x, newTrailPosition.y, 0);
        }
    }

    public void ClearSignCanvas()
    {
        for (int j = 0; j < Signatures.Count; j++)
        {
            Destroy(Signatures[j]);
        }
    }
    public void SaveSignature()
    {
        if (!fALabScreenSaver.gameObject.activeInHierarchy)
            return;

        applicationStatus = ApplicationStatus.SaveSignature;

        // make photo
        fALabScreenSaver.MakePhoto();
        fALabScreenSaver.SaveAllTheFinalToTheDiskFolder();
        rawImage.color = new Color(255, 255, 255, 255);
        rawImage.texture = fALabScreenSaver.tempList[0];
        finishSignQuad.GetComponent<Renderer>().material.mainTexture = fALabScreenSaver.tempList[0];
        ClearSignCanvas();

        var shape = particles[4].GetComponent<ParticleSystem>().shape;
        shape.texture = fALabScreenSaver.tempList[0];
        particles[4].GetComponent<ParticleSystem>().Play();
        rawImage.DOFade(0f, 1f).SetDelay(0.2f).SetEase(Ease.OutCubic);
        particles[4].GetComponent<Renderer>().material.DOVector(new Vector4(255, 255, 255) * 0.03f, "_EmissionColor", 5f).SetEase(Ease.InQuint);
        StartCoroutine(NextFlow());
    }

    IEnumerator NextFlow()
    {
        yield return new WaitForSeconds(3);
        video_Background.transform.parent.GetChild(0).GetComponent<DisplayUGUI>().DOFade(0f, 1.5f).SetEase(Ease.OutQuart);
        video_Background.transform.parent.GetChild(1).GetComponent<DisplayUGUI>().DOFade(0f, 1.5f).SetEase(Ease.OutQuart);

        frostedGlassController.ReverseBlurCity();
        usefulFunc.ReverseAO(postProcessingProfile, blurImage);

        modeManager.modeStatus = ModeManager.ModeStatus.Init;
    }

    public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.FinishedPlaying:
                applicationStatus = ApplicationStatus.SignTime;
                break;
        }
        // Debug.Log("Event: " + et.ToString());
    }

    private void WhichFlow()
    {
        switch (applicationStatus)
        {
            case ApplicationStatus.Init:
                InitializeAll();
                panelManager_04.canDraw = false;
                break;

            case ApplicationStatus.SignTime:
                panelManager_04.canDraw = true;
                break;

            case ApplicationStatus.SaveSignature:
                panelManager_04.canDraw = false;
                break;

            default:
                break;
        }
    }
    


}
