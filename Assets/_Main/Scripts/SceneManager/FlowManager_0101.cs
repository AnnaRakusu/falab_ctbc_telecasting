using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using UnityEngine.PostProcessing;

public class FlowManager_0101 : MonoBehaviour
{
    #region Process Management
    public enum ApplicationStatus
    {
        Init,
        Opening,
        OnHandPlacing,
        Particles,
        Idle
    }
    public ApplicationStatus applicationStatus = ApplicationStatus.Init;
    #endregion

    #region Scripts
    public WebsocketConnector websocketConnector;
    public SettingClass settingClass;
    public PanelManager_01 panelManager_01;
    public FlowManager_0102 flowManager_0102;
    public FrostedGlassController frostedGlassController;
    public LoadSceneSmoother loadSceneSmoother;
    public UsefulFunc usefulFunc;
    #endregion

    #region GameObjects
    [Space(15)]
    public GameObject trailParent;
    public Camera uICamera;
    private Sequence seq;
    public Image logo;
    public GameObject destroyObj;
    public PostProcessingProfile postProcessingProfile;
    public Image blurImage;

    [Space(15)]
    public VideoUIController[] videos = new VideoUIController[2];
    // videos[0] -> Video_Door
    // videos[1] -> Video_Deco02

    [Space(15)]
    public List<GameObject> particles;
    // particles[0] -> arc
    // particles[1] -> halo
    // particles[2] -> rising
    // particles[3] -> numbers
    // particles[4] -> webCamReveal;
    #endregion

    #region Bools
    public bool canClick  = true;
    public float stagnantTime;
    public float stagnantLimit;
    private bool isBackToSlideShow = false;
    #endregion

    void Start()
    {
        applicationStatus = ApplicationStatus.Init;
        //EnhancedTouchSupport.Enable();
        //EnhancedTouchSupport.Disable();
    }

    void Update()
    {
        WhichFlow();
        if (canClick)
        {
            TouchPos();
            BackToSlideShow();
        }

        if (Input.GetKeyDown(KeyCode.H))
        {
            OnHandPlacing();
            websocketConnector.PlayTouchDoorMusic();
        }
    }

    #region Init -> step 1
    public void InitializeAll()
    {
        InitializeAnim();
        InitializePartical();
        applicationStatus = ApplicationStatus.Opening;

        stagnantTime = 0;
        stagnantLimit = settingClass.falabSettings.visualSetting.mode_Passenger.stagnantLimit01;
        isBackToSlideShow = false;
    }

    private void InitializePartical()
    {
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().glowingObjectGroupNumber = settingClass.falabSettings.visualSetting.paricleSetting.arc.glowingObjectGroupNumber;
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().effectRandomTime.minTime = settingClass.falabSettings.visualSetting.paricleSetting.arc.minTime;
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().effectRandomTime.maxTime = settingClass.falabSettings.visualSetting.paricleSetting.arc.maxTime;

        var mainNumber = particles[3].GetComponent<ParticleSystem>().main;
        mainNumber.startSize = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.number.minSize, settingClass.falabSettings.visualSetting.paricleSetting.number.maxSize);
        var emissionNumber = particles[3].GetComponent<ParticleSystem>().emission;
        emissionNumber.rateOverTime = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.number.minEmit, settingClass.falabSettings.visualSetting.paricleSetting.number.maxEmit);

        var mainReverse = particles[5].transform.GetChild(0).GetComponent<ParticleSystem>().main;
        mainReverse.startSize = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.reverse.minSize, settingClass.falabSettings.visualSetting.paricleSetting.reverse.maxSize);
    }

    private void InitializeAnim()
    {
        seq = DOTween.Sequence();
        var tempColor = new Color(255, 255, 255, 0);
        logo.color = tempColor;
        blurImage.color = tempColor;
    }
    #endregion

    #region step 1 - while single guest mode
    private void Opening()
    {
        videos[0].TriggerIn();
    }
    #endregion

    #region step 2 - when center touched
    public void TouchPos()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Vector3 newTouchPosition = uICamera.ScreenToWorldPoint(Input.mousePosition);
            // Debug.Log(newTouchPosition);

            if (TouchCenter(newTouchPosition))
            {
                applicationStatus = ApplicationStatus.OnHandPlacing;
                canClick = false;
            }
            else
                RotateToTrail();
        }
    }

    private bool TouchCenter(Vector3 pos)
    {
        var max = 1.3f;
        var min = -1.3f;

        if (pos.x > min && pos.y > min && pos.x < max && pos.y < max)
            return true;
        else
            return false;
    }

    private void RotateToTrail()
    {
        DOTween.Kill("hint");
        seq = DOTween.Sequence();
        seq.Append(trailParent.transform.DORotate(new Vector3(0, 0, 360), 0.5f, RotateMode.FastBeyond360).SetEase(Ease.OutSine).SetId("hint"))
            .Append(trailParent.transform.DORotate(new Vector3(0, 0, 360), 1f, RotateMode.FastBeyond360).SetEase(Ease.OutSine).SetId("hint"));
    }
    #endregion

    #region count freeze time, and back to slide show scene
    public void BackToSlideShow()
    {
        if (stagnantTime > stagnantLimit)
        {
            isBackToSlideShow = true;
            stagnantTime = 0;
        }

        if (isBackToSlideShow)
        {
            isBackToSlideShow = false;
            loadSceneSmoother.FadeInScene("Mode_Slide Show");
            websocketConnector.ShouldPlayBgm();
            return;
        }

        if (UnityEngine.InputSystem.EnhancedTouch.Touch.activeFingers.Count == 0 && stagnantTime < stagnantLimit)
            stagnantTime += Time.deltaTime;
        else
            stagnantTime = 0;
    }
    #endregion

    public void OnHandPlacing()
    {        
        StartCoroutine(VideoPlaying());
    }

    private IEnumerator VideoPlaying()
    {
        videos[0].TriggerOut();
        videos[1].TriggerIn();
        // GameObject.Find("Music").GetComponent<MusicController>().PlayTouchDoorMusic();
        Destroy(destroyObj);

        yield return new WaitForSeconds(1f);
        for (int i = 0; i< particles.Count; i++)
            particles[i].SetActive(true);

        logo.DOFade(1f, 2f);

        yield return new WaitForSeconds(3f);
        videos[1].gameObject.transform.parent.gameObject.transform.GetChild(0).GetComponent<DisplayUGUI>().DOFade(0f, 1f);
        applicationStatus = ApplicationStatus.Particles;
        
        yield return new WaitForSeconds(7f);
        particles[4].GetComponent<WebcamRevealControl>().ParticleConverge();

        yield return new WaitForSeconds(2f);
        frostedGlassController.targetFrostedValue = settingClass.falabSettings.visualSetting.mode_Passenger.targetFrostedValue;
        usefulFunc.SetAOZero(postProcessingProfile, blurImage);
        frostedGlassController.BlurCity();
    }

    #region step 3 - particle ...
    private IEnumerator ParticleIn()
    {
        yield return new WaitForSeconds(4);
        NextFlow();
    }
    #endregion

    #region step 4 - go to camera panel...
    private void NextFlow()
    {
        panelManager_01.panelTrigger = PanelManager_01.PanelTrigger.CameraPanel;
        flowManager_0102.applicationStatus = FlowManager_0102.ApplicationStatus.Detecting;
        flowManager_0102.shouldPlayBG = true;
    }
    #endregion

    private void WhichFlow()
    {
        switch (applicationStatus)
        {
            case ApplicationStatus.Init:
                InitializeAll();
                break;

            case ApplicationStatus.Opening:
                applicationStatus = ApplicationStatus.Idle;
                Opening();
                break;

            case ApplicationStatus.OnHandPlacing:
                applicationStatus = ApplicationStatus.Idle;
                OnHandPlacing();
                break;

            case ApplicationStatus.Particles:
                applicationStatus = ApplicationStatus.Idle;
                StartCoroutine(ParticleIn());
                break;

            case ApplicationStatus.Idle:
                break;

            default:
                break;
        }
    }
}
