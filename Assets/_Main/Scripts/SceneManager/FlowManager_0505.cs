using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowManager_0505 : MonoBehaviour
{
    public float[] eventLastingTime;
    public string[] eventUrl;
    public string[] eventFormat;

    // Start is called before the first frame update
    void Start()
    {
        if(eventUrl != null)
        {
            GetFormat();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void GetFormat()
    {
        for(int i = 0; i < eventUrl.Length; i++)
        {
            string[] urls = eventUrl[i].Split('.');
            string format = urls[urls.Length - 1].ToLower();

            if (format == "png" || format == "jpg")
                eventFormat[i] = "image";
            else if (format == "wav" || format == "mp4")
                eventFormat[i] = "video";
        }
    }
}
