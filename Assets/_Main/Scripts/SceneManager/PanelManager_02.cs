using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager_02 : MonoBehaviour
{
    #region Scipts
    public ModeManager modeManager;
    public WebsocketConnector websocketConnector;
    public FlowManager_0201 flowManager_0201;
    public FlowManager_0202 flowManager_0202;
    public FlowManager_0203 flowManager_0203;
    public FlowManager_0204 flowManager_0204;
    #endregion

    #region Process Management
    public enum PanelTrigger
    {
        Init,
        WelcomePanel,
        CameraPanel,
        SignPanel,
        CoolCardPanel,
        Idle
    }
    public PanelTrigger panelTrigger = PanelTrigger.Idle;

    public GameObject[] panels = new GameObject[7];
    #endregion

    public bool shouldDetect = false;
    public bool canDraw = false;
    public bool isRegister = false;

    private void Awake()
    {
        panelTrigger = PanelTrigger.Init;
    }

    void Start()
    {
        // check single mode has data or not
        if (websocketConnector.currentMode == 0)
            isRegister = true;
        else if (websocketConnector.currentMode == 1)
            isRegister = false;
    }

    void Update()
    {
        WhichPanel();
    }


    public void InitializeAll()
    {
        flowManager_0201.InitializeAll();
        flowManager_0202.InitializeAll();
        flowManager_0203.InitializeAll();
        flowManager_0204.InitializeAll();

        panelTrigger = PanelTrigger.WelcomePanel;
    }

    private void WhichPanel()
    {        
        switch (panelTrigger)
        {
            case PanelTrigger.Idle:
                break;

            case PanelTrigger.WelcomePanel:
                break;

            case PanelTrigger.CameraPanel:
                break;

            case PanelTrigger.SignPanel:
                shouldDetect = false;
                break;

            case PanelTrigger.CoolCardPanel:
                canDraw = false;
                break;

            case PanelTrigger.Init:
                InitializeAll();
                SwitchPanel();
                break;

            default:
                break;
        }
    }

    public void SwitchPanel()
    {
        panelTrigger = PanelTrigger.Idle;

        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].SetActive(true);
        }
    }
}
