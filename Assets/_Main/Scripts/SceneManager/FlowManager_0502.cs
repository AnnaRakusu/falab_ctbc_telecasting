using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FlowManager_0502 : MonoBehaviour
{
    public Animator bg_animator;
    public Text guestJob;
    public Image guestPhoto;
    public Image guestSignature;

    void Awake()
    {
        InitializeAll();
    }

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void InitializeAll()
    {
        guestJob.gameObject.SetActive(true);
        guestPhoto.gameObject.SetActive(true);
        guestSignature.gameObject.SetActive(true);

        var colorBlack = Color.black;
        var colorWhite = Color.white;
        colorBlack.a = colorWhite.a = 0;
        guestJob.color = colorBlack;
        guestPhoto.color = colorWhite;
        guestSignature.color = colorWhite;

        guestJob.gameObject.SetActive(false);
        guestPhoto.gameObject.SetActive(false);
        guestSignature.gameObject.SetActive(false);
    }

    public void VVIPSlideShow()
    {
        //for(int i = 0; )
    }
}
