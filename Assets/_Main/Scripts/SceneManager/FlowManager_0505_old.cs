﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using DG.Tweening;

//Event Slide Show Function
public class FlowManager_0505_old : MonoBehaviour
{
    public Image eventImage;
    public MediaPlayer eventVideo;

    void Awake()
    {
        InitializeAll();
    }

    void Start()
    {
        StartCoroutine(EventSlideShow());
    }

    void Update()
    {

    }

    void InitializeAll()
    {
        //set image and video alpha = 0
        eventImage.gameObject.SetActive(true);
        eventVideo.gameObject.SetActive(true);

        var tempColor = Color.white;
        tempColor.a = 0;
        eventImage.color = tempColor;
        eventVideo.gameObject.GetComponent<DisplayUGUI>().color = tempColor;

        eventImage.gameObject.SetActive(false);
        eventVideo.gameObject.SetActive(false);
    }

    IEnumerator EventSlideShow()
    {
        //
        yield return new WaitForSeconds(1);

        for (int i = 0; i < FetchEvent.eventInformationPaths.Count; i++)
        {
            if (FetchEvent.isImages[i])
            {
                eventImage.gameObject.SetActive(true);
                eventVideo.gameObject.SetActive(false);

                ChangeImage(i);
                FadeIn(i);
            }
            else
            {
                eventImage.gameObject.SetActive(false);
                eventVideo.gameObject.SetActive(true);

                StartCoroutine(ChangeVideo(i));
                FadeIn(i);
            }

            yield return new WaitForSeconds(1f);
            yield return new WaitForSeconds(FetchEvent.lastTime[i] - 1f);
            FadeOut(i);
            yield return new WaitForSeconds(1);

        }
    }

    void ChangeImage(int i)
    {
        byte[] pngBytes = System.IO.File.ReadAllBytes(Application.streamingAssetsPath + "/Event/" + i + ".png");
        Texture2D tex = new Texture2D(100, 100);
        tex.LoadImage(pngBytes);

        Sprite s = Sprite.Create(tex, new Rect(0f, 0f, tex.width, tex.height), Vector2.zero);
        eventImage.sprite = s;
        eventImage.preserveAspect = true;
    }

    IEnumerator ChangeVideo(int i)
    {
        eventVideo.OpenVideoFromFile(MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder, "Event/" + i + ".mp4");

        yield return new WaitForSeconds(1f);
        
        float duration = eventVideo.Info.GetDurationMs() / 1000;
        FetchEvent.lastTime[i] = duration;
    }

    void FadeIn(int i)
    {
            eventImage.DOFade(1f, 1f);
            eventVideo.gameObject.GetComponent<DisplayUGUI>().DOFade(1f, 1f);
    }

    public void FadeOut(int i)
    {
            eventImage.DOFade(0f, 1f);
            eventVideo.gameObject.GetComponent<DisplayUGUI>().DOFade(0f, 1f);
    }
}
