﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using UnityEngine.PostProcessing;


public class FlowManager_0103 : MonoBehaviour
{
    #region Process Management
    public enum ApplicationStatus
    {
        Init,
        ShowContent,
        QRCodeScaningTime,
        Idle,
        Finish
    }
    public ApplicationStatus applicationStatus = ApplicationStatus.Init;
    #endregion

    #region Scripts
    [Space(15)]
    public ModeManager modeManager;
    public PanelManager_01 panelManager_01;
    public FALabScreenSaver[] fALabScreenSavers = new FALabScreenSaver[2];
    public FrostedGlassController frostedGlassController;
    public WebsocketConnector websocketConnector;
    public LoadSceneSmoother loadSceneSmoother;
    public UsefulFunc usefulFunc;
    #endregion

    #region GameObjects
    [Space(15)]
    private Sequence seq;

    public VideoUIController[] videos = new VideoUIController[2];
    // videos[0] -> Video_Ribbon_glow
    // videos[1] -> Video_Ticket_glow
    // videos[2] -> Video_Ticket

    public RawImage qRCodeImg;
    public RawImage guestPhoto;
    public RawImage guestPhotoMask;
    //public Image glowDot;
    public GameObject glowSphere;
    public GameObject ticket;

    private Vector3 startScaleValue = new Vector3(0f, 0f, 0f);
    private Vector3 endScaleValue = new Vector3(1f, 1f, 1f);
    private Vector3 startPosValue = new Vector3(-1040, -750, 0);
    private Vector3 endPosValue = new Vector3(800, -300, 0);
    private Quaternion startRotateValue = Quaternion.Euler(0f, 0f, 0f);
    private Vector3 endRotateValue = new Vector3(0f, 0f, -365f);

    [Space(15)]
    public TextMeshProUGUI qRCodeTitle;
    public TextMeshProUGUI dateTime;
    public DisplayUGUI countDown_DU;
    public MediaPlayer countDown_MP;
    public TextMeshProUGUI countDownNumber;
    public PostProcessingProfile postProcessingProfile;
    public Image blurImage;
    #endregion

    public bool isPlayed = false;
    private void Awake()
    {
        InitializeAll();
    }

    void Update()
    {
        if(panelManager_01.panelTrigger == PanelManager_01.PanelTrigger.CoolCardPanel && !isPlayed)
        {
            isPlayed = true;
            applicationStatus = ApplicationStatus.ShowContent;
        }
        WhichFlow();
    }

    #region Init
    public void InitializeAll()
    {
        ticket.transform.localScale = startScaleValue;
        ticket.transform.localPosition = startPosValue;
        ticket.transform.localRotation = startRotateValue;

        seq = DOTween.Sequence();

        var tempColor = Color.white;
        tempColor.a = 0;
        qRCodeTitle.color = tempColor;
        qRCodeImg.color = tempColor;
        guestPhoto.color = tempColor;
        guestPhotoMask.color = tempColor;
        dateTime.color = tempColor;
        dateTime.text = DateTime.Now.Year.ToString("0000") + "/" + DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Day.ToString("00");
        countDownNumber.color = tempColor;
        //glowDot.color = tempColor;
    }
    #endregion

    #region PlayVideo(), AnimIn(), AnimOut()
    IEnumerator ShowContent()
    {
        frostedGlassController.ReverseBlurCity();
        usefulFunc.ReverseAO(postProcessingProfile, blurImage);

        guestPhoto.texture = fALabScreenSavers[0].tempList[0];

        yield return new WaitForSeconds(0.5f);
        seq = DOTween.Sequence();
        seq.Append(glowSphere.transform.DOScale(new Vector3(0.08f, 0.08f, 0.08f), 1f).SetId("sendTicket"))
            .Append(glowSphere.transform.DOScale(new Vector3(0f, 0f, 0f), 1f).SetId("sendTicket"))
            .Append(videos[1].gameObject.transform.parent.gameObject.transform.GetChild(0).GetComponent<DisplayUGUI>().DOFade(0.25f, 1f).SetId("sendTicket"))
            .Join(videos[2].gameObject.transform.parent.gameObject.transform.GetChild(0).GetComponent<DisplayUGUI>().DOFade(1f, 1f).SetId("sendTicket"))
            .Join(guestPhoto.DOFade(1f, 1f).SetId("sendTicket"))
            .Join(guestPhotoMask.DOFade(0.2f, 1f).SetId("sendTicket"))
            .Join(ticket.transform.DOScale(endScaleValue, 1f).SetEase(Ease.OutBack).SetId("sendTicket"))
            .Join(ticket.transform.DOLocalMove(endPosValue, 1f).SetEase(Ease.OutBack).SetId("sendTicket"))
            .Join(ticket.transform.DORotate(endRotateValue, 1f, RotateMode.FastBeyond360).SetEase(Ease.OutBack).SetId("sendTicket"));

        yield return new WaitForSeconds(3f);
        videos[0].TriggerIn();
        //videos[1].TriggerIn();

        DOTween.Kill("sendTicket");
        
        dateTime.DOFade(1f, 3f).OnComplete(CoolCardShot);            
    }

    public void QRCodeIn()
    {
        countDown_MP.Control.Play();
        InvokeRepeating("CountDownNum", 1f, 1f);
        DOTween.Kill("dateTime");
        seq = DOTween.Sequence();
        seq.Append(qRCodeTitle.DOFade(1f, 1f).SetId("qrcode"))
            .Join(qRCodeImg.DOFade(1f, 1f).SetId("qrcode"))
            .Join(countDownNumber.DOFade(1f, 1f).SetId("qrcode"))
            .Join(countDown_DU.DOFade(0.5f, 1f).SetId("qrcode"));
    }
    void CoolCardShot()
    {
        fALabScreenSavers[1].MakePhoto();
        fALabScreenSavers[1].SaveAllTheFinalToTheDiskFolder();
        applicationStatus = ApplicationStatus.QRCodeScaningTime;
    }

    private void CountDownNum()
    {
        int lastTime = int.Parse(countDownNumber.text);
        DOTween.Kill("countDown");
        Sequence seqCount = DOTween.Sequence();

        if (lastTime % 2 == 0)
            seqCount.Append(countDown_DU.gameObject.transform.parent.parent.transform.DOScale(new Vector3(0.475f, 0.475f, 0.475f), 1f).SetEase(Ease.InOutSine).SetId("countDown"));
        else
            seqCount.Append(countDown_DU.gameObject.transform.parent.parent.transform.DOScale(new Vector3(0.5f, 0.5f, 0.5f), 1f).SetEase(Ease.InOutSine).SetId("countDown"));

        if (lastTime >= 1)
            countDownNumber.text = (lastTime-1).ToString();
        else
        {
            this.CancelInvoke();
            OnAnimOutComplete();
        }
    }

    void AnimOut()
    {
        seq = DOTween.Sequence();
        seq.Append(guestPhoto.DOFade(0f, 2f))
            .Join(guestPhotoMask.DOFade(0f, 2f))
            .Join(countDown_DU.DOFade(0f, 2f))
            .Join(qRCodeTitle.DOFade(0f, 2f))
            .Join(qRCodeImg.DOFade(0f, 2f))
            .Join(dateTime.DOFade(0f, 2f))
            .Join(videos[0].gameObject.transform.parent.gameObject.GetComponent<DisplayUGUI>().DOFade(0f, 2f))
            //.Join(videos[1].gameObject.transform.parent.gameObject.GetComponent<DisplayUGUI>().DOFade(0f, 2f))
            .Join(videos[1].gameObject.transform.parent.gameObject.GetComponent<DisplayUGUI>().DOFade(0f, 2f))
            .Join(videos[2].gameObject.transform.parent.gameObject.GetComponent<DisplayUGUI>().DOFade(0f, 2f));
        OnAnimOutComplete();
    }

    void OnAnimOutComplete()
    {
        applicationStatus = ApplicationStatus.Finish;
        websocketConnector.ShouldPlayBgm();
        loadSceneSmoother.FadeInScene("Mode_Map Start");
    }
    #endregion

    private void WhichFlow()
    {
        switch (applicationStatus)
        {
            case ApplicationStatus.Init:
                InitializeAll();
                break;

            case ApplicationStatus.ShowContent:
                applicationStatus = ApplicationStatus.Idle;
                StartCoroutine(ShowContent());
                break;

            case ApplicationStatus.QRCodeScaningTime:
                applicationStatus = ApplicationStatus.Idle;
                break;

            case ApplicationStatus.Finish:
                applicationStatus = ApplicationStatus.Init;
                panelManager_01.panelTrigger = PanelManager_01.PanelTrigger.Init;
                modeManager.modeStatus = ModeManager.ModeStatus.Init;
                break;

            case ApplicationStatus.Idle:
                break;

            default:
                break;
        }
    }
}
