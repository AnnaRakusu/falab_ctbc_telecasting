using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using UnityEngine.PostProcessing;

public class FlowManager_0201 : MonoBehaviour
{
    #region Process Management
    public enum ApplicationStatus
    {
        Init,
        Opening,
        OnHandPlacing,
        Particles,
        Idle
    }
    public ApplicationStatus applicationStatus = ApplicationStatus.Init;
    #endregion

    #region Scripts
    public WebsocketConnector websocketConnector;
    public SettingClass settingClass;
    public SetWelcomePanel setWelcomePanel;
    public ModeManager modeManager;
    public PanelManager_02 panelManager_02;
    public FlowManager_0202 flowManager_0202;
    public FrostedGlassController frostedGlassController;
    public UsefulFunc usefulFunc;
    public PostProcessingProfile postProcessingProfile;
    #endregion

    #region GameObjects
    [Space(15)]
    public Image logo;
    public Image blurImage;

    [Space(15)]
    private bool canPlacing = false;
    public VideoUIController[] videos = new VideoUIController[2];
    // videos[0] -> Video_Door
    // videos[1] -> Video_Deco01
    public MediaPlayer video_doorTop;
    private Sequence seq;

    [Space(15)]
    public TextMeshProUGUI museumText;
    public TextMeshProUGUI guestCompanyText;
    public TextMeshProUGUI guestJobText;
    public TextMeshProUGUI guestNameText;

    [Space(15)]
    public List<GameObject> particles;
    // particles[0] -> arc
    // particles[1] -> halo
    // particles[2] -> rising
    // particles[3] -> numbers
    // particles[4] -> webCamReveal
    // particles[5] -> reverse
    // particles[6] -> split
    #endregion
    
    void Start()
    {
        applicationStatus = ApplicationStatus.Init;
        
    }

    void Update()
    {
        WhichFlow();

        #region for debug
        if (modeManager.isDebugging && canPlacing && Input.GetKeyDown(KeyCode.H))
        {
            OnHandPlacing();
            websocketConnector.PlayTouchDoorMusic();
        }
        #endregion
    }

    #region Initialize -> step 1
    public void InitializeAll()
    {
        InitializeAnim();
        InitializePartical();
        SetGuestInformation();
        usefulFunc.InitAO(postProcessingProfile, blurImage);
        //first step auto start
        applicationStatus = ApplicationStatus.Opening;
    }

    private void InitializeAnim()
    {
        seq = DOTween.Sequence();

        canPlacing = false;

        // init - color white & alpha 0
        var tempColor = new Color(255, 255, 255, 0);
        guestCompanyText.color = tempColor;
        guestJobText.color = tempColor;
        logo.color = tempColor;
        blurImage.color = tempColor;

        // init - color white
        museumText.color = Color.white;
        guestNameText.color = Color.white;
    }

    private void InitializePartical()
    {
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().glowingObjectGroupNumber = settingClass.falabSettings.visualSetting.paricleSetting.arc.glowingObjectGroupNumber;
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().effectRandomTime.minTime = settingClass.falabSettings.visualSetting.paricleSetting.arc.minTime;
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().effectRandomTime.maxTime = settingClass.falabSettings.visualSetting.paricleSetting.arc.maxTime;

        var mainNumber = particles[3].GetComponent<ParticleSystem>().main;
        mainNumber.startSize = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.number.minSize, settingClass.falabSettings.visualSetting.paricleSetting.number.maxSize);
        var emissionNumber = particles[3].GetComponent<ParticleSystem>().emission;
        emissionNumber.rateOverTime = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.number.minEmit, settingClass.falabSettings.visualSetting.paricleSetting.number.maxEmit);

        var mainReverse = particles[5].transform.GetChild(0).GetComponent<ParticleSystem>().main;
        mainReverse.startSize = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.reverse.minSize, settingClass.falabSettings.visualSetting.paricleSetting.reverse.maxSize);

        var mainSplit = particles[6].transform.GetChild(0).GetComponent<ParticleSystem>().main;
        mainSplit.startSize = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.split.minSize, settingClass.falabSettings.visualSetting.paricleSetting.split.maxSize);
    }

    private void SetGuestInformation()
    {
        museumText.text = "";
        guestNameText.text = "";
        guestCompanyText.text = setWelcomePanel.getGuestCompanyText;
        guestJobText.text = setWelcomePanel.getGuestJobText;

        if (websocketConnector.currentMode == 0)
        {
            videos[1].gameObject.transform.parent.GetChild(0).GetComponent<MediaPlayer>().m_VideoPath = "Videos/Deco01_loop.mov";
            videos[1].gameObject.transform.parent.GetChild(1).GetComponent<MediaPlayer>().m_VideoPath = "Videos/Deco01_out.mov";
        }
        else if (websocketConnector.currentMode == 1)
        {
            videos[1].gameObject.transform.parent.GetChild(0).GetComponent<MediaPlayer>().m_VideoPath = "Videos/Deco02_loop.mov";
            videos[1].gameObject.transform.parent.GetChild(1).GetComponent<MediaPlayer>().m_VideoPath = "Videos/Deco02_out.mov";
        }
    }
    #endregion 

    #region step 1 - while single guest mode
    private void Opening()
    {
        videos[0].TriggerIn();
        canPlacing = true;
    }
    #endregion

    #region step 2 - when staff click the start button...
    public void OnHandPlacing()
    {        
        if(canPlacing)
        {
            canPlacing = false;
            Anim();
            StartCoroutine(VideoPlaying());
        }
    }
    #endregion

    #region step 3 - welcome anim playing...
    private void Anim()
    {
        DOTween.Kill("welcomeAnim");
        seq = DOTween.Sequence();
        seq.SetId("welcomeAnim");

        seq.Append(museumText.DOText(setWelcomePanel.getWelcomeText, 0.7f).SetDelay(0.8f).SetEase(Ease.OutCubic).SetId("welcomeAnim"))
            .AppendInterval(0.5f).SetId("welcomeAnim")
            .Append(guestCompanyText.DOFade(1f, 0.8f).SetEase(Ease.InOutCubic).SetId("welcomeAnim"))
            .Join(guestCompanyText.transform.DOLocalMove(new Vector3(-520, -205, 0), 1f).From().SetEase(Ease.OutBack).SetId("welcomeAnim"))
            .Join(guestJobText.DOFade(1f, 1.2f).SetEase(Ease.InOutCubic).SetId("welcomeAnim"))
            .Join(guestJobText.transform.DOLocalMove(new Vector3(-520, -265, 0), 1f).From().SetEase(Ease.OutBack).SetId("welcomeAnim"))
            .Append(guestNameText.DOText(setWelcomePanel.getGuestNameText, 1f).SetEase(Ease.InOutCubic).SetId("welcomeAnim"))
            
            .AppendInterval(5f).SetId("welcomeAnim")
            .Append(museumText.DOFade(0f, 0.5f).SetEase(Ease.InOutCubic).SetId("welcomeAnim"))
            .Join(guestCompanyText.DOFade(0f, 0.8f).SetEase(Ease.InOutCubic).SetId("welcomeAnim"))
            .Join(guestJobText.DOFade(0f, 0.8f).SetEase(Ease.InOutCubic).SetDelay(0.2f).SetId("welcomeAnim"))
            .Join(guestNameText.DOFade(0f, 1.5f).SetEase(Ease.InOutCubic).SetId("welcomeAnim"))
            .AppendInterval(3f).SetId("welcomeAnim").OnComplete(OnAnimComplete);
    }

    private IEnumerator VideoPlaying()
    {
        videos[1].TriggerIn();
        videos[0].TriggerOut();
        video_doorTop.Control.Play();
        yield return new WaitForSeconds(7f);

        for (int i = 0; i < particles.Count; i++)
            particles[i].SetActive(true);

        logo.DOFade(1f, 2f);

        yield return new WaitForSeconds(3f);
        videos[1].gameObject.transform.parent.gameObject.transform.GetChild(0).GetComponent<DisplayUGUI>().DOFade(0f, 1f);
        
        yield return new WaitForSeconds(6f);
        particles[4].GetComponent<WebcamRevealControl>().ParticleConverge();

        yield return new WaitForSeconds(2f);
        usefulFunc.SetAOZero(postProcessingProfile, blurImage);
        
        frostedGlassController.targetFrostedValue = settingClass.falabSettings.visualSetting.mode_SingleGuest.targetFrostedValue;
        frostedGlassController.BlurCity();
    }
    
    private void OnAnimComplete()
    {
        applicationStatus = ApplicationStatus.Particles;
    }
    #endregion

    #region step 4 - particle ...
    private IEnumerator ParticleIn()
    {
        yield return new WaitForSeconds(4f);
        NextFlow();
    }
    #endregion

    #region step 5 - go to camera panel...
    private void NextFlow()
    {
        panelManager_02.panelTrigger = PanelManager_02.PanelTrigger.CameraPanel;
        flowManager_0202.applicationStatus = FlowManager_0202.ApplicationStatus.FirstPhotoIn;
        flowManager_0202.shouldPlayBG = true;
    }
    #endregion

    private void WhichFlow()
    {
        switch (applicationStatus)
        {
            case ApplicationStatus.Init:
                InitializeAll();
                break;

            case ApplicationStatus.Opening:
                applicationStatus = ApplicationStatus.Idle;
                Opening();
                break;

            case ApplicationStatus.OnHandPlacing:
                applicationStatus = ApplicationStatus.Idle;
                OnHandPlacing();
                break;

            case ApplicationStatus.Particles:
                applicationStatus = ApplicationStatus.Idle;
                StartCoroutine(ParticleIn());
                break;

            case ApplicationStatus.Idle:
                break;

            default:
                break;
        }
    }
}
