using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager_03 : MonoBehaviour
{
    #region Scipts
    public ModeManager modeManager;
    public WebsocketConnector websocketConnector;
    public FlowManager_0301 flowManager_0301;
    public FlowManager_0302 flowManager_0302;
    public FlowManager_0303 flowManager_0303;
    #endregion

    #region Process Management
    public enum PanelTrigger
    {
        Init,
        CameraPanel,
        SignPanel,
        CoolCardPanel,
        Idle
    }
    public PanelTrigger panelTrigger = PanelTrigger.Idle;

    public GameObject[] panels = new GameObject[7];
    #endregion

    public bool shouldDetect = false;
    public bool canDraw = false;

    private void Awake()
    {
        panelTrigger = PanelTrigger.Init;
    }

    void Update()
    {
        WhichPanel();
    }


    public void InitializeAll()
    {
        flowManager_0301.InitializeAll();
        flowManager_0302.InitializeAll();
        flowManager_0303.InitializeAll();

        panelTrigger = PanelTrigger.CameraPanel;
    }

    private void WhichPanel()
    {        
        switch (panelTrigger)
        {
            case PanelTrigger.Idle:
                break;

            case PanelTrigger.CameraPanel:
                break;

            case PanelTrigger.SignPanel:
                shouldDetect = false;
                break;

            case PanelTrigger.CoolCardPanel:
                canDraw = false;
                break;

            case PanelTrigger.Init:
                panelTrigger = PanelTrigger.Idle;
                InitializeAll();
                SwitchPanel();
                break;

            default:
                break;
        }
    }

    public void SwitchPanel()
    {
        panelTrigger = PanelTrigger.Idle;

        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].SetActive(true);
        }
    }
}
