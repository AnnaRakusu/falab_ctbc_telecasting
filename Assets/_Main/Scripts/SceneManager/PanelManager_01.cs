using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager_01 : MonoBehaviour
{
    #region Scipts
    public WebsocketConnector websocketConnector;
    public FlowManager_0101 flowManager_0101;
    //public FlowManager_0102 flowManager_0102;
    //public FlowManager_0103 flowManager_0103;
    #endregion

    #region Process Management
    public enum PanelTrigger
    {
        Init,
        WelcomePanel,
        CameraPanel,
        CoolCardPanel,
        Idle
    }
    public PanelTrigger panelTrigger = PanelTrigger.Idle;

    public GameObject[] panels = new GameObject[7];
    #endregion

    public bool shouldDetect = false;

    private void Awake()
    {
        panelTrigger = PanelTrigger.Init;
    }

    void Start()
    {
        SwitchPanel();
    }

    void Update()
    {
        WhichPanel();
    }


    public void InitializeAll()
    {
        flowManager_0101.InitializeAll();
        //flowManager_0102.InitializeAll();
        //flowManager_0103.InitializeAll();

        panelTrigger = PanelTrigger.WelcomePanel;
    }

    private void WhichPanel()
    {        
        switch (panelTrigger)
        {
            case PanelTrigger.Idle:
                break;

            case PanelTrigger.WelcomePanel:
                shouldDetect = false;
                break;

            case PanelTrigger.CameraPanel:
                panelTrigger = PanelTrigger.Idle;
                break;

            case PanelTrigger.CoolCardPanel:
                shouldDetect = false;
                break;

            case PanelTrigger.Init:
                InitializeAll();
                SwitchPanel();
                break;

            default:
                break;
        }
    }

    public void SwitchPanel()
    {
        panelTrigger = PanelTrigger.Idle;

        for (int i = 0; i < panels.Length; i++)
        {
            panels[i].SetActive(true);
        }
    }
}
