using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelManager_04 : MonoBehaviour
{
    #region Scipts
    public ModeManager modeManager;
    public FlowManager_0401 flowManager_0401;

    #endregion

    #region Process Management
    public enum PanelTrigger
    {
        Init,
        SignPanel,
        Idle
    }

    public WebsocketConnector websocketConnector;
    public PanelTrigger panelTrigger = PanelTrigger.Idle;

    public bool canDraw = false;
    public GameObject[] panels = new GameObject[1];
    #endregion

    private void Awake()
    {
        panelTrigger = PanelTrigger.Init;
        InitializeAll();
    }

    public void InitializeAll()
    {
        SwitchPanel();
    }

    private void WhichPanel()
    {        
        switch (panelTrigger)
        {
            case PanelTrigger.Init:
                panelTrigger = PanelTrigger.SignPanel;
                InitializeAll();
                break;

            case PanelTrigger.SignPanel:
                break;

            case PanelTrigger.Idle:
                break;            

            default:
                break;
        }
    }

    public void SwitchPanel()
    {
        panelTrigger = PanelTrigger.Init;

        for (int j = 0; j < panels.Length; j++)
        {
            panels[j].SetActive(true);
        }
    }
}
