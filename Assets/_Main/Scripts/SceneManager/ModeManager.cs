using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ModeManager : MonoBehaviour
{
    #region Process Management
    public WebsocketConnector websocketConnector;

    // check which mode is playing
    public enum ModeStatus
    {
        Init,
        PassengerMode,
        SingleGuestMode,
        MultiGuestMode,
        MultiSignMode,
        SildeShowMode,
    }
    public ModeStatus modeStatus = ModeStatus.Init;

    private float skyboxRotation = 205f;
    public bool isDebugging = true;
    private int lastMode = -1;
    #endregion

    void Start()
    {
        lastMode = websocketConnector.currentMode;
        RenderSettings.skybox.SetFloat("_Rotation", skyboxRotation);
    }

    void Update()
    {
        if (skyboxRotation < 360)
            skyboxRotation -= 0.01f;
        else
            skyboxRotation = 360;
        RenderSettings.skybox.SetFloat("_Rotation", skyboxRotation);

        TestKeycode();
        GetSignal();

        if (Input.GetKeyDown(KeyCode.D))
            isDebugging = !isDebugging;

        if (Input.GetKeyDown(KeyCode.M))
            Cursor.visible = !Cursor.visible;
    }

    void GetSignal()
    {
        if (lastMode != websocketConnector.currentMode)
            SwitchMode();
        
        if (websocketConnector.currentMode == 0 || websocketConnector.currentMode == 1)
            modeStatus = ModeStatus.SingleGuestMode;
        else if (websocketConnector.currentMode == 2)
            modeStatus = ModeStatus.MultiGuestMode;
        else if (websocketConnector.currentMode == 3)
            modeStatus = ModeStatus.MultiSignMode;
        else if (websocketConnector.currentMode == 4)
            modeStatus = ModeStatus.PassengerMode;
        else if (websocketConnector.currentMode == 5)
            modeStatus = ModeStatus.SildeShowMode;
        
        lastMode = websocketConnector.currentMode;
    }

    void TestKeycode()
    {
        if (Input.GetKeyDown(KeyCode.Keypad0))
            websocketConnector.currentMode = 0;
        else if (Input.GetKeyDown(KeyCode.Keypad1))
            websocketConnector.currentMode = 1;
        else if (Input.GetKeyDown(KeyCode.Keypad2))
            websocketConnector.currentMode = 2;
        else if (Input.GetKeyDown(KeyCode.Keypad3))
            websocketConnector.currentMode = 3;
        else if (Input.GetKeyDown(KeyCode.Keypad4))
            websocketConnector.currentMode = 4;
        else if (Input.GetKeyDown(KeyCode.Keypad5))
            websocketConnector.currentMode = 5;
    }

    void SwitchMode()
    {
        websocketConnector.callModeChange[websocketConnector.currentMode].calledEvents.Invoke();
    }

    void WhichMode()
    {
        switch (modeStatus)
        {
            case ModeStatus.PassengerMode:
                ShowMode("Passenger Mode");
                break;

            case ModeStatus.SingleGuestMode:
                ShowMode("Single Guest Mode");
                break;

            case ModeStatus.MultiGuestMode:
                ShowMode("Multi Guest Mode");
                break;

            case ModeStatus.MultiSignMode:
                ShowMode("Multi Sign Mode");
                break;

            case ModeStatus.SildeShowMode:
                ShowMode("Silde Show Mode");
                break;

            default:
                break;
        }
    }

    void ShowMode(string s)
    {
        Debug.Log(s);
    }

    public void LoadSingleGuestScene()
    {
        SceneManager.LoadScene("Mode_Single Guest");
    }

    public void LoadMultiGuestScene()
    {
        SceneManager.LoadScene("Mode_Multi Guest");
    }

    public void LoadMultiSignScene()
    {
        SceneManager.LoadScene("Mode_Multi Sign");
    }

    public void LoadPassengerScene()
    {
        SceneManager.LoadScene("Mode_Passenger");
    }

    public void LoadSlideShowScene()
    {
        SceneManager.LoadScene("Mode_Slide Show");
    }

}