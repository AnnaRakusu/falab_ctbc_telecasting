﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using UnityEngine.PostProcessing;


public class FlowManager_0303 : MonoBehaviour
{
    #region Process Management
    public enum ApplicationStatus
    {
        Init,
        VideoPlaying,
        ShowContent,
        QRCodeScaningTime,
        Idle,
        Finish
    }
    public ApplicationStatus applicationStatus = ApplicationStatus.Init;
    #endregion

    #region Scripts
    [Space(15)]
    public ModeManager modeManager;
    public PanelManager_03 panelManager_03;
    public FALabScreenSaver[] fALabScreenSavers = new FALabScreenSaver[3];
    public FrostedGlassController frostedGlassController;
    public UsefulFunc usefulFunc;
    public RandomNameGenerator randomNameGenerator;
    #endregion

    #region GameObjects
    [Space(15)]
    private Sequence seq;

    public VideoUIController[] videos = new VideoUIController[4];
    // videos[0] -> Video_ Frontground
    // videos[1] -> Video_BallBackground01
    // videos[2] -> Video_BallBackground02
    // videos[3] -> Video_BallAdd01
    public RawImage qRCodeImg;
    public Image guestPhoto;
    public RawImage gusetSignature;
    public PostProcessingProfile postProcessingProfile;
    public Image blurImage;

    [Space(15)]
    public TextMeshProUGUI qRCodeTitle;
    public TextMeshProUGUI dateTime;
    #endregion

    public bool isPlayed = false;
    
    private void Awake()
    {
        InitializeAll();
    }

    void Update()
    {
        if(panelManager_03.panelTrigger == PanelManager_03.PanelTrigger.CoolCardPanel && !isPlayed)
        {
            isPlayed = true;
            applicationStatus = ApplicationStatus.VideoPlaying;
        }
        WhichFlow();
    }

    #region Init
    public void InitializeAll()
    {
        applicationStatus = ApplicationStatus.Init;

        InitializeAnim();
    }

    public void InitializeAnim()
    {
        seq = DOTween.Sequence();

        var tempColorWhite = new Color(255, 255, 255, 0);
        qRCodeTitle.color = tempColorWhite;
        qRCodeImg.color = tempColorWhite;
        guestPhoto.color = tempColorWhite;
        gusetSignature.color = tempColorWhite;
        dateTime.color = tempColorWhite;
        dateTime.text = DateTime.Now.Year.ToString("0000") + "/" + DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Day.ToString("00");
    }

    #endregion

    #region PlayVideo(), ShowContent(), AnimIn(), QRCodeIn(), CoolCardShot()
    IEnumerator PlayVideo()
    {
        for(int i = 0; i < videos.Length - 1; i++)
        {
            videos[i].TriggerIn();
        }

        frostedGlassController.ReverseBlurCity();
        usefulFunc.ReverseAO(postProcessingProfile, blurImage);

        yield return new WaitForSeconds(3.75f);
        applicationStatus = ApplicationStatus.ShowContent;
    }

    IEnumerator ShowContent()
    {
        yield return new WaitForSeconds(1f);
        videos[3].TriggerIn();
        
        usefulFunc.SetTexture(gusetSignature, fALabScreenSavers[1].tempList[0]);
        usefulFunc.SetImage(guestPhoto, fALabScreenSavers[0].rmbgPhotoPath, fALabScreenSavers[0].originalPhotoPath);

        yield return new WaitForSeconds(1.5f);
        AnimIn();
    }

    void AnimIn()
    {
        Debug.Log("AnimIn");

        seq = DOTween.Sequence();

        seq.Append(guestPhoto.DOFade(1f, 0.8f).SetId("AnimIn"))
            .Join(gusetSignature.DOFade(1f, 1.2f).SetDelay(0.6f).SetId("AnimIn"))
            .Join(gusetSignature.transform.DOLocalMove(new Vector3(350, 620, 0), 1.2f).From().SetId("AnimIn").SetEase(Ease.OutQuad))
            .Join(dateTime.DOFade(1f, 1.2f).SetId("AnimIn")).OnComplete(CoolCardShot);
    }

    public void QRCodeIn()
    {
        DOTween.Kill("AnimIn");
        seq = DOTween.Sequence();
        seq.Append(qRCodeTitle.DOFade(1f, 1f))
            .Join(qRCodeImg.DOFade(1f, 1f));
    }
    void CoolCardShot()
    {
        // UploadRmbgPhoto(fALabScreenSavers[0].rmbgPhotoPath);

        fALabScreenSavers[2].MakePhoto();
        fALabScreenSavers[2].SaveAllTheFinalToTheDiskFolder();

        applicationStatus = ApplicationStatus.QRCodeScaningTime;
    }

    /*
    void UploadRmbgPhoto(string path)
    {
        if (!File.Exists(path))
            return;

        Debug.Log("loading");
        //建立檔案讀取流
        FileStream fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
        fileStream.Seek(0, SeekOrigin.Begin);
        
        //建立檔案長度緩衝區
        byte[] rmbgPhoto = new byte[fileStream.Length];

        //讀取檔案
        fileStream.Read(rmbgPhoto, 0, (int)fileStream.Length);

        //釋放檔案讀取流
        fileStream.Close();
        fileStream.Dispose();
        fileStream = null;

        fALabScreenSavers[0].gameObject.GetComponent<FileUploader>().UploadImage(rmbgPhoto, setWelcomePanel.getRandomFileName + "_RMBG");
    }
    */

    IEnumerator WaitSecond(int i)
    {
        yield return new WaitForSeconds(i);
        AnimOut();
    }

    void AnimOut()
    {
        for(int i = 0; i < videos.Length; i++)
        {
            videos[i].TriggerOut();
        }
        
        seq = DOTween.Sequence();
        seq.Append(gusetSignature.DOFade(0f, 2f))
            .Join(guestPhoto.DOFade(0f, 2f))
            .Join(qRCodeTitle.DOFade(0f, 2f))
            .Join(qRCodeImg.DOFade(0f, 2f))
            .Join(dateTime.DOFade(0f, 2f)).OnComplete(OnAnimOutComplete);
    }

    void OnAnimOutComplete()
    {
        applicationStatus = ApplicationStatus.Finish;
    }
    #endregion

    private void WhichFlow()
    {
        switch (applicationStatus)
        {
            case ApplicationStatus.Init:
                InitializeAll();
                break;

            case ApplicationStatus.VideoPlaying:
                applicationStatus = ApplicationStatus.Idle;
                StartCoroutine(PlayVideo());
                break;

            case ApplicationStatus.ShowContent:
                applicationStatus = ApplicationStatus.Idle;
                StartCoroutine(ShowContent());
                break;

            case ApplicationStatus.QRCodeScaningTime:
                applicationStatus = ApplicationStatus.Idle;
                break;

            case ApplicationStatus.Finish:
                applicationStatus = ApplicationStatus.Init;
                panelManager_03.panelTrigger = PanelManager_03.PanelTrigger.Init;
                modeManager.modeStatus = ModeManager.ModeStatus.Init;
                break;

            case ApplicationStatus.Idle:
                break;

            default:
                break;
        }
    }
}
