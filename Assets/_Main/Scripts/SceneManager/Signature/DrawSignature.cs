using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawSignature : MonoBehaviour
{
    public Camera signatureCamera;
    //public TrailRenderer signatureTrail;
    public GameObject signaturePrefab;

    private int i;
    private List<GameObject> Signatures = new List<GameObject>();
    private List<TrailRenderer> TrailRenderers = new List<TrailRenderer>();

    public GraphicRaycasterRaycasterExample grre;
    private bool isFirstMouseDown = true;

    void Start()
    {
        i = -1;
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            isFirstMouseDown = true;

            Debug.Log(TrailRenderers);
            Debug.Log(i);
        }

        if (!grre.isSignaturePanelTouched)
            return;
        /*
        if (Input.GetMouseButtonDown(0))
        {
        }
        */
    
        if (Input.GetKey(KeyCode.Mouse0))
        {
            Vector3 newTrailPosition = signatureCamera.ScreenToWorldPoint(Input.mousePosition);

            if (isFirstMouseDown)
            {
                i++;
                isFirstMouseDown = false;

                //instantiate a stroke
                GameObject g = Instantiate(signaturePrefab, newTrailPosition, Quaternion.identity) as GameObject;
                Signatures.Add(g);
                TrailRenderers.Add(g.GetComponent<TrailRenderer>());
            }
            //update position when draging
            TrailRenderers[i].transform.position = new Vector3( newTrailPosition.x, newTrailPosition.y, 0);
        }


    }

    public void ClearSignCanvas()
    {
        for (int j = 0; j < Signatures.Count; j++)
        {
            Destroy(Signatures[i]);
        }
        Debug.Log(gameObject.transform.childCount);
    }
}
