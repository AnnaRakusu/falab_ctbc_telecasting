using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AlphaThreshold : MonoBehaviour
{
    private void Awake()
    {
        Image mask = gameObject.GetComponent<Image>();
        mask.alphaHitTestMinimumThreshold = 1f;
    }
}
