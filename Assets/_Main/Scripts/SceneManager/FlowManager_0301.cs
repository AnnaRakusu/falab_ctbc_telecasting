using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;
using UnityEngine.PostProcessing;


public class FlowManager_0301 : MonoBehaviour
{
    #region Process Management
    public enum ApplicationStatus
    {
        Init,
        FirstPhoto,
        PluralPhoto,
        CheckPhoto,
        SavePhoto,
        Idle
    }
    public ApplicationStatus applicationStatus = ApplicationStatus.Init;
    #endregion

    #region Scripts
    [Space(15)]
    public WebsocketConnector websocketConnector;
    public FALabScreenSaver fALabScreenSaver;
    public SettingClass settingClass;
    public PanelManager_03 panelManager_03;
    public UsefulFunc usefulFunc;
    public RemoveBackground removeBackground;
    public DlibFaceLandmarkDetectorExample.FacePointDetection facePointDetection;
    public FrostedGlassController frostedGlassController;
    #endregion

    #region Cameras and Render Textures
    [Space(15)]
    public Image mask;
    public RawImage rawImage;
    public Camera renderCam;
    public RenderTexture renderCamTex;
    #endregion

    #region Bools
    [Space(15)]
    private bool isAnimInDone;
    public bool shouldPlayBG;

    #endregion

    #region GameObjects
    [Space(15)]
    public VideoUIController video_background;
    public Animator image_SmileHint;
    private Sequence seqIn;
    private Sequence seqOut;
    public PostProcessingProfile postProcessingProfile;
    public Image blurImage;

    [Space(15)]
    public RawImage cameraFlash;
    public TextMeshProUGUI countDownText;
    public Button cameraAgain;
    public Button cameraFinish;

    [Space(15)]
    public GameObject[] particles = new GameObject[6];

    [Space(15)]
    public GameObject destroyObj;
    #endregion

    private void Awake()
    {
        applicationStatus = ApplicationStatus.Init;
    }

    void Update()
    {
        WhichFlow();
    }

    #region Init
    public void InitializeAll()
    {
        rawImage.texture = renderCamTex;
        shouldPlayBG = false;
        countDownText.gameObject.SetActive(false);

        InitializeAnim();
        InitializePartical();
    }

    public void InitializeAnim()
    {
        seqIn = DOTween.Sequence();
        seqOut = DOTween.Sequence();
        var tempColor = new Color(255f, 255f, 255f, 0);
        blurImage.color = tempColor;

        tempColor = new Color(255f, 255f, 255f, 255f);
        mask.color = tempColor;
        rawImage.color = tempColor;

        image_SmileHint.gameObject.GetComponent<Image>().color = tempColor;
        image_SmileHint.ResetTrigger("in");
        image_SmileHint.ResetTrigger("out");

        cameraAgain.gameObject.transform.localScale = new Vector3(0f, 0f, 0f);
        cameraFinish.gameObject.transform.localScale = new Vector3(0f, 0f, 0f);
        countDownText.text = settingClass.falabSettings.visualSetting.mode_MultiGuest.firstCountDownTime;
        countDownText.gameObject.SetActive(false);

        shouldPlayBG = true;
    }


    private void InitializePartical()
    {
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().glowingObjectGroupNumber = settingClass.falabSettings.visualSetting.paricleSetting.arc.glowingObjectGroupNumber;
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().effectRandomTime.minTime = settingClass.falabSettings.visualSetting.paricleSetting.arc.minTime;
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().effectRandomTime.maxTime = settingClass.falabSettings.visualSetting.paricleSetting.arc.maxTime;

        var mainNumber = particles[3].GetComponent<ParticleSystem>().main;
        mainNumber.startSize = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.number.minSize, settingClass.falabSettings.visualSetting.paricleSetting.number.maxSize);
        var emissionNumber = particles[3].GetComponent<ParticleSystem>().emission;
        emissionNumber.rateOverTime = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.number.minEmit, settingClass.falabSettings.visualSetting.paricleSetting.number.maxEmit);

        var mainReverse = particles[5].transform.GetChild(0).GetComponent<ParticleSystem>().main;
        mainReverse.startSize = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.reverse.minSize, settingClass.falabSettings.visualSetting.paricleSetting.reverse.maxSize);

        var mainSplit = particles[6].transform.GetChild(0).GetComponent<ParticleSystem>().main;
        mainSplit.startSize = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.split.minSize, settingClass.falabSettings.visualSetting.paricleSetting.split.maxSize);
    }
    #endregion

    #region step 1 - play video...
    private  void VideoPlaying()
    {
        if (shouldPlayBG)
        {
            shouldPlayBG = false;
            StartCoroutine(OpenCamera());
        }
    }

    IEnumerator OpenCamera()
    {
        yield return new WaitForSeconds(5f);
        particles[4].GetComponent<WebcamRevealControl>().ParticleConverge();
        
        
        yield return new WaitForSeconds(2f);
        StartCoroutine(OpenCamera());
        video_background.TriggerIn();

        yield return new WaitForSeconds(0.3f); 
        frostedGlassController.targetFrostedValue = settingClass.falabSettings.visualSetting.mode_MultiGuest.targetFrostedValue;
        usefulFunc.SetAOZero(postProcessingProfile, blurImage);
        frostedGlassController.BlurCity();

        yield return new WaitForSeconds(1f);
        mask.DOFade(1f, 1f);
        rawImage.DOFade(1f, 1f);
        PhotoTakingIn();
    }
    #endregion

    #region step 2 - take photo
    private void PhotoTakingIn()
    {
        //when guest's face detected
        if (applicationStatus == ApplicationStatus.FirstPhoto)
        {
            if (!isAnimInDone)
            {
                isAnimInDone = true;
                DOTween.Kill("hint");
                usefulFunc.FadeImg(image_SmileHint.gameObject.GetComponent<Image>(), 1f, 0.5f, "hint", Ease.InOutCubic);
                image_SmileHint.SetTrigger("in");
                InvokeRepeating("CountDown", 1, 1);
            }
        }
    }

    void PhotoTaking()
    {
        applicationStatus = ApplicationStatus.CheckPhoto;

        DOTween.Kill("hint");
        usefulFunc.FadeImg(image_SmileHint.gameObject.GetComponent<Image>(), 0f, 0.5f, "hint", Ease.InOutCubic, PhotoTakingOut);
        image_SmileHint.SetTrigger("out");

        // if rePhoto, clear the list
        fALabScreenSaver.tempList.Clear();
        fALabScreenSaver.savedNameList.Clear();

        // screenshot
        fALabScreenSaver.MakePhoto();

        rawImage.texture = fALabScreenSaver.tempList[fALabScreenSaver.tempList.Count - 1];
        renderCam.targetTexture = renderCamTex;
    }

    void PhotoTakingOut()
    {
        usefulFunc.ScaleObj(cameraAgain.gameObject, 1f, 0.5f, Ease.OutBack);
        usefulFunc.ScaleObj(cameraFinish.gameObject, 1f, 0.5f, Ease.OutBack);
    }
    #endregion

    #region count down
    void CountDown()
    {
        if (int.Parse(countDownText.text) > 1)
        {
            countDownText.gameObject.SetActive(true);
            usefulFunc.ScaleAndBack(countDownText.gameObject, 1.2f, 1f);
            countDownText.gameObject.SetActive(true);
            countDownText.text = (int.Parse(countDownText.text) - 1).ToString();
        }
        else if (int.Parse(countDownText.text) == 1)
        {
            usefulFunc.FadeAndBack(cameraFlash, 0.5f, 0.2f, PhotoTaking);
            CancelInvoke("CountDown");
            countDownText.gameObject.SetActive(false);
            countDownText.text = settingClass.falabSettings.visualSetting.mode_MultiGuest.pluralCountDownTime;
        }
    }
    #endregion

    #region Buttons
    // when guest press the again button - count down from three and take photo
    public void RePhotoBtn()
    {
        applicationStatus = ApplicationStatus.PluralPhoto;
        
        usefulFunc.ScaleAndBack(cameraAgain.gameObject, 1.1f, 0.5f, RePhotoBtnOut);

        rawImage.texture = renderCamTex;
        InvokeRepeating("CountDown", 1, 1);
    }

    public void RePhotoBtnOut()
    {
        usefulFunc.ScaleObj(cameraAgain.gameObject, 0f, 0.5f);
        usefulFunc.ScaleObj(cameraFinish.gameObject, 0f, 0.5f);
        usefulFunc.FadeImg(image_SmileHint.gameObject.GetComponent<Image>(), 1f, 0.5f, "hint");
        image_SmileHint.SetTrigger("in");
    }

    // Panel Out - when guest press the finish button
    public void FinishPhotoBtn()
    {
        applicationStatus = ApplicationStatus.SavePhoto;
        usefulFunc.ScaleAndBack(cameraFinish.gameObject, 1.1f, 1f, FinishPhotoBtnOut);

        // save photo
        fALabScreenSaver.SaveAllTheFinalToTheDiskFolder();

        // remove.bg set path
        removeBackground.originalPhoto = fALabScreenSaver.originalPhotoPath;
        removeBackground.outputPhoto = fALabScreenSaver.rmbgPhotoPath;
        removeBackground.MakePhotoRemoveBackground();

        // this flow out
        rawImage.DOFade(0f, 1f).SetEase(Ease.InCubic);
        mask.DOFade(0f, 1f).SetEase(Ease.InCubic).OnComplete(NextFlow);
        mask.transform.DOScale(new Vector3(0f, 0f, 0f), 2f).SetEase(Ease.InBack);
        Destroy(destroyObj);
    }

    public void FinishPhotoBtnOut()
    {
        usefulFunc.ScaleObj(cameraAgain.gameObject, 0f, 0.5f);
        usefulFunc.ScaleObj(cameraFinish.gameObject, 0f, 0.5f);
    }

    void NextFlow()
    {
        panelManager_03.panelTrigger = PanelManager_03.PanelTrigger.SignPanel;
    }
    #endregion

    private void WhichFlow()
    {
        switch (applicationStatus)
        {
            case ApplicationStatus.Init:
                InitializeAll();
                applicationStatus = ApplicationStatus.FirstPhoto;
                break;

            case ApplicationStatus.FirstPhoto:
                VideoPlaying();
                break;

            case ApplicationStatus.PluralPhoto:
                break;

            case ApplicationStatus.CheckPhoto:
                break;

            case ApplicationStatus.Idle:
                break;

            default:
                break;
        }
    }
}