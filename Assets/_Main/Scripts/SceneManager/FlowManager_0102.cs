using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using RenderHeads.Media.AVProVideo;

public class FlowManager_0102 : MonoBehaviour
{
    #region Process Management
    public enum ApplicationStatus
    {
        Init,
        Detecting,
        CheckPhoto,
        Idle
    }
    public ApplicationStatus applicationStatus = ApplicationStatus.Init;
    #endregion

    #region Scripts
    [Space(15)]
    public WebsocketConnector websocketConnector;
    public FALabScreenSaver fALabScreenSaver;
    public SettingClass settingClass;
    public PanelManager_01 panelManager_01;
    public UsefulFunc usefulFunc;
    public DlibFaceLandmarkDetectorExample.FacePointDetection facePointDetection;
    public CTBCFaceDealer cTBCFaceDealer;
    public LoadSceneSmoother loadSceneSmoother;
    #endregion

    #region Cameras and Render Textures
    [Space(15)]
    public Image mask;
    public RawImage rawImage;
    public Camera renderCam;
    public RenderTexture renderCamTex;
    #endregion

    #region Bools
    [Space(15)]
    public bool shouldPlayBG;
    private bool isDetected = false;
    private bool isHere = false;
    private bool isSmiling = false;
    private bool isAnimInDone = false;
    private bool startEnergyBar = false;
    private float faceDetectionTime = 0;
    private float faceDetectionTimeThreshold;
    public float stagnantTime = 0;
    public float stagnantLimit;
    private bool isBackToSlideShow = false;
    #endregion

    #region GameObjects
    [Space(15)]
    public List<VideoUIController> videos;
    // videos[0] -> Video_FaceScaning
    // videos[1] -> Video_PassengerSuccess
    public VideoUIController video_background;
    public MediaPlayer video_smileEnergyBar;
    public Animator image_SmileHint;
    public TextMeshProUGUI text_FaceHint;
    private int energyBarStart = 710;
    private int energyBarEnd = 7100;
    public RawImage cameraFlash;

    [Space(15)]
    public GameObject destroyObj;
    #endregion

    private void Awake()
    {
        applicationStatus = ApplicationStatus.Init;
    }

    void Update()
    {
        WhichFlow();

        if (panelManager_01.shouldDetect && applicationStatus == ApplicationStatus.Detecting)
        {
            DetectSmile();
            LoadFaceInformation();
            BackToSlideShow();
        }
    }

    #region Init
    public void InitializeAll()
    {
        rawImage.texture = renderCamTex;

        isDetected = false;
        isHere = false;
        isSmiling = false;
        isAnimInDone = false;
        shouldPlayBG = false;

        var tempColor = new Color(255, 255, 255, 0);
        mask.color = tempColor;
        rawImage.color = tempColor;
        text_FaceHint.color = tempColor;

        stagnantLimit = settingClass.falabSettings.visualSetting.mode_Passenger.stagnantLimit02;

        video_smileEnergyBar.Events.AddListener(OnVideoEvent);

        faceDetectionTimeThreshold = settingClass.falabSettings.visualSetting.mode_SingleGuest.faceDetectionTimeThreshold;

        InitializeAnim();
    }

    public void InitializeAnim()
    {
        var tempColor = new Color(255, 255, 255, 0);
        mask.color = tempColor;
        image_SmileHint.gameObject.GetComponent<Image>().color = tempColor;
        image_SmileHint.ResetTrigger("in");
        image_SmileHint.ResetTrigger("out");
    }
    #endregion

    public void BackToSlideShow()
    {
        if (stagnantTime > stagnantLimit)
        {
            isBackToSlideShow = true;
            stagnantTime = 0;
        }

        if (isBackToSlideShow)
        {
            isBackToSlideShow = false;
            loadSceneSmoother.FadeInScene("Mode_Slide Show");
            websocketConnector.ShouldPlayBgm();
            return;
        }

        if (!isHere && stagnantTime < stagnantLimit)
            stagnantTime += Time.deltaTime;
        else
            stagnantTime = 0;
    }

    #region step 1 - play video...
    private void VideoPlaying()
    {
        if (shouldPlayBG)
        {
            shouldPlayBG = false;
            StartCoroutine(OpenCamera());
        }
    }

    IEnumerator OpenCamera()
    {
        yield return new WaitForSeconds(4f);
        video_background.TriggerIn();

        yield return new WaitForSeconds(1f);
        mask.DOFade(1f, 1f);
        rawImage.DOFade(1f, 1f);

        yield return new WaitForSeconds(0.5f);
        image_SmileHint.gameObject.SetActive(true);
        image_SmileHint.SetTrigger("in");
        videos[0].TriggerIn();

        yield return new WaitForSeconds(1f);
        panelManager_01.shouldDetect = true;

        yield return new WaitForSeconds(3f);
        startEnergyBar = true;
    }
    #endregion

    private void DetectSmile()
    {
        //when guest's face detected
        if (panelManager_01.shouldDetect && isDetected)
        {
            if (isHere)
            {
                // just do once
                if (!isAnimInDone)
                {
                    isAnimInDone = true;
                    FaceHintOut();
                    SuccessIn();
                }

                if (startEnergyBar)
                {
                    faceDetectionTime += Time.deltaTime;

                    if (isSmiling || 
                        video_smileEnergyBar.Control.GetCurrentTimeMs() < energyBarStart || video_smileEnergyBar.Control.GetCurrentTimeMs() > energyBarEnd || 
                        faceDetectionTime > faceDetectionTimeThreshold)
                    {
                        video_smileEnergyBar.Control.Play();
                        if (video_smileEnergyBar.Control.GetCurrentTimeMs() > energyBarEnd)
                        {
                            usefulFunc.FadeAndBack(cameraFlash, 0.5f, 0.2f);
                            PhotoTaking();
                        }

                    }
                    else if (!isSmiling)
                        video_smileEnergyBar.Control.Pause();
                }
            }
            else if(!isHere)
            {
                video_smileEnergyBar.Control.Pause();
                faceDetectionTime = 0;

                if (isAnimInDone)
                {
                    isAnimInDone = false;
                    videos[1].gameObject.transform.parent.gameObject.transform.GetChild(0).GetComponent<MediaPlayer>().Control.Rewind();
                    FaceHintIn();
                    SuccessOut();
                }
            }
        }
        else if (panelManager_01.shouldDetect && !isDetected)
            FaceHintIn();
    }

    private void PhotoTaking()
    {
        image_SmileHint.ResetTrigger("in");
        image_SmileHint.ResetTrigger("out");

        usefulFunc.ScaleObj(videos[0].gameObject.transform.parent.gameObject, 0f, 0.5f, Ease.InBack);
        usefulFunc.ScaleObj(videos[1].gameObject.transform.parent.gameObject, 0f, 0.5f, Ease.InBack);

        applicationStatus = ApplicationStatus.CheckPhoto;
        panelManager_01.shouldDetect = false;
        
        image_SmileHint.SetTrigger("out");
        usefulFunc.FadeImg(image_SmileHint.gameObject.GetComponent<Image>(), 0f, 0.5f, "taking", Ease.InOutCubic);

        if (cameraFlash.color.a != 0)
            cameraFlash.DOFade(0f, 0.1f);

        fALabScreenSaver.MakePhoto();

        rawImage.texture = fALabScreenSaver.tempList[fALabScreenSaver.tempList.Count - 1];
        renderCam.targetTexture = renderCamTex;
        
        video_smileEnergyBar.transform.GetComponent<DisplayUGUI>().DOFade(0f, 0.5f).SetEase(Ease.OutCubic).OnComplete(PhotoTakingOut);

        
    }

    private void PhotoTakingOut()
    {
        StartCoroutine(Str_PhotoTakingOut());
    }

    IEnumerator Str_PhotoTakingOut()
    {
        Destroy(destroyObj);
        
        yield return new WaitForSeconds(settingClass.falabSettings.visualSetting.mode_Passenger.checkPhotoTime);
        
        Sequence seq = DOTween.Sequence();
        seq.Append(rawImage.DOFade(0f, 1f))
            .Join(mask.DOFade(0f, 1f))
            .Join(mask.gameObject.transform.DOScale(new Vector3(0f, 0f, 0f), 1.5f).SetEase(Ease.InBack)).OnComplete(NextFlow);
    }
    
    void NextFlow()
    {
        video_background.TriggerOut();
        video_background.gameObject.transform.parent.gameObject.transform.GetChild(1).GetComponent<DisplayUGUI>().DOFade(0f, 0.5f);
        panelManager_01.panelTrigger = PanelManager_01.PanelTrigger.CoolCardPanel;
        applicationStatus = ApplicationStatus.Init;
    }

    #region isSmiling, isDerected, isHere 
    // get and calc the information from <FacePointDetection>
    private void LoadFaceInformation()
    {
        isDetected = cTBCFaceDealer.isDetected;
        isHere = cTBCFaceDealer.isHere;
        isSmiling = cTBCFaceDealer.isSmiling;
    }

    #endregion

    #region FaceHintIn(), FaceHintOut(), SuccessIn(), SuccessOut();
    private void FaceHintIn()
    {
        DOTween.Kill("FaceHint");
        text_FaceHint.DOFade(1f, 1.2f).SetId("FaceHint");
    }
    
    private void FaceHintOut()
    {
        DOTween.Kill("FaceHint");
        text_FaceHint.DOFade(0f, 1.2f).SetId("FaceHint");
    }

    private void SuccessIn()
    {
        DOTween.Kill("Success");
        videos[1].gameObject.GetComponent<DisplayUGUI>().DOFade(1f, 0.5f).SetId("Success");
        videos[1].TriggerIn();
    }
    
    private void SuccessOut()
    {
        Debug.Log("inifgjlg");
        DOTween.Kill("Success");
        videos[1].transform.parent.GetChild(0).GetComponent<MediaPlayer>().Control.Rewind();
        video_smileEnergyBar.Control.Rewind();

        videos[1].gameObject.GetComponent<DisplayUGUI>().DOFade(0f, 1f).SetId("Success");
        videos[1].TriggerOut();
    }
    #endregion
    
    public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.FinishedPlaying:
                break;
        }
        // Debug.Log("Event: " + et.ToString());
    }

    private void WhichFlow()
    {
        switch (applicationStatus)
        {
            case ApplicationStatus.Init:
                applicationStatus = ApplicationStatus.Idle;
                InitializeAll();
                break;

            case ApplicationStatus.Detecting:
                VideoPlaying();
                break;

            case ApplicationStatus.CheckPhoto:
                break;

            case ApplicationStatus.Idle:
                break;

            default:
                break;
        }
    }
}