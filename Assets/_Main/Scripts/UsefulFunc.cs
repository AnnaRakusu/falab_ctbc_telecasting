using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using UnityEngine.PostProcessing;


public class UsefulFunc : MonoBehaviour
{
    public void ScaleAndBack(GameObject gameObject, float scaleSize, float duration, Action completeFunc = null, string id = null)
    {
        Sequence seqBtn = DOTween.Sequence();
        
        seqBtn.Append(gameObject.transform.DOScale(new Vector3(scaleSize, scaleSize, scaleSize), duration / 2).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutCubic).SetId(id))
            .OnComplete(() => {
                if (completeFunc != null)
                    completeFunc();
            });
    }

    public void ScaleObj(GameObject gameObject, float scaleSize, float duration, Ease ease = Ease.OutCubic ,Action completeFunc = null)
    {
        Sequence seqBtn = DOTween.Sequence();

        seqBtn.Append(gameObject.transform.DOScale(new Vector3(scaleSize, scaleSize, scaleSize), duration).SetEase(ease))
            .OnComplete(() => {
                if (completeFunc != null)
                    completeFunc();
            });
    }

    public void FadeAndBack(RawImage rawImage, float endValue, float duration, Action completeFunc = null)
    {
        Sequence seqBtn = DOTween.Sequence();

        seqBtn.Append(rawImage.DOFade(endValue, duration / 2).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutCubic))
            .OnComplete(() => {
                if (completeFunc != null)
                {
                    completeFunc();
                }
            });
    }

    public void FadeImg(RawImage rawImage, float endValue, float duration, string id, Ease ease = Ease.OutCubic, Action completeFunc = null)
    {
        Sequence seqBtn = DOTween.Sequence();

        seqBtn.Append(rawImage.DOFade(endValue, duration).SetEase(ease).SetId(id))
            .OnComplete(() => {
                if (completeFunc != null)
                    completeFunc();
            });
    }

    public void FadeAndBack(Image image, float endValue, float duration, Action completeFunc = null)
    {
        Sequence seqBtn = DOTween.Sequence();

        seqBtn.Append(image.DOFade(endValue, duration / 2).SetLoops(2, LoopType.Yoyo).SetEase(Ease.OutCubic))
            .OnComplete(() => {
                if (completeFunc != null)
                    completeFunc();
            });
    }

    public void FadeImg(Image image, float endValue, float duration, string id, Ease ease = Ease.OutCubic, Action completeFunc = null)
    {
        Sequence seqBtn = DOTween.Sequence();

        seqBtn.Append(image.DOFade(endValue, duration).SetEase(ease).SetId(id))
            .OnComplete(() => {
                if (completeFunc != null)
                    completeFunc();
            });
    }

    public void SetTexture(RawImage targetImg, Texture texture)
    {
        targetImg.texture = texture;
    }

    public void SetImage(Image img, string mainPath, string overwritePath)
    {
        string path;
        if (File.Exists(mainPath))
            path = mainPath;
        else
            path = overwritePath;

        byte[] pngBytes = System.IO.File.ReadAllBytes(path);
        Texture2D tex = new Texture2D(100, 100);
        tex.LoadImage(pngBytes);

        Sprite s = Sprite.Create(tex, new Rect(0f, 0f, tex.width, tex.height), Vector2.zero);
        img.sprite = s;
        img.preserveAspect = true;
    }

    public void SetAOZero(PostProcessingProfile postProcessingProfile, Image blurImage)
    {
        var PPAO = postProcessingProfile;
        AmbientOcclusionModel.Settings ao = postProcessingProfile.ambientOcclusion.settings;
        DOTween.To(() => ao.intensity, x => ao.intensity = x, 0f, 1f).OnUpdate(() =>
        {
            postProcessingProfile.ambientOcclusion.settings = ao;
        });

        blurImage.DOFade(1f, 5f).SetDelay(0.5f);
    }

    public void ReverseAO(PostProcessingProfile postProcessingProfile, Image blurImage)
    {
        var PPAO = postProcessingProfile;
        AmbientOcclusionModel.Settings ao = postProcessingProfile.ambientOcclusion.settings;
        DOTween.To(() => ao.intensity, x => ao.intensity = x, 0.5f, 1f).OnUpdate(() =>
        {
            postProcessingProfile.ambientOcclusion.settings = ao;
        }).SetDelay(1f);

        blurImage.DOFade(0f, 1f).SetEase(Ease.OutQuint);
    }

    public void InitAO(PostProcessingProfile postProcessingProfile, Image blurImage)
    {
        var PPAO = postProcessingProfile;
        AmbientOcclusionModel.Settings ao = postProcessingProfile.ambientOcclusion.settings;
        DOTween.To(() => ao.intensity, x => ao.intensity = x, 0.5f, 0f).OnUpdate(() =>
        {
            postProcessingProfile.ambientOcclusion.settings = ao;
        });
        blurImage.DOFade(0f, 0f);
    }
}
