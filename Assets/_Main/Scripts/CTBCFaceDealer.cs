using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DlibFaceLandmarkDetectorExample;
using OpenCVForUnity.UnityUtils.Helper;

public class CTBCFaceDealer : MonoBehaviour
{
    #region Scripts
    public FacePointDetection facePointDetection;
    public WebCamTextureToMatHelper webCamTextureToMatHelper;
    public SettingClass settingClass;
    public WebsocketConnector websocketConnector;
    #endregion

    #region GameObjects
    public Camera faceDetectionCam;
    #endregion

    #region Loaded Data
    private Vector2 magicPoint;
    private float smileRatio;
    private float minX;
    private float maxX;
    private float minY;
    private float maxY;
    #endregion

    #region Output Data
    private Vector2 closestNosePos;
    private Vector2 closesEyebrowPos;
    public Vector3 realNosePos;
    public Vector3 realEyebrowPos;

    public bool isHere = false;
    public bool isSmiling = false;
    public bool isForceSmile = false;
    public bool isDetected = false;
    #endregion

    private List<float> facesDistances;

    private void Start()
    {
        smileRatio = settingClass.falabSettings.visualSetting.smileRatio;

        if (websocketConnector.currentMode == 0 || websocketConnector.currentMode == 1)
            InitializeSingleGuestMode();
        else if (websocketConnector.currentMode == 4)
            InitializePassengerMode();
    }

    private void InitializeSingleGuestMode()
    {
        magicPoint = new Vector2(webCamTextureToMatHelper.requestedWidth / 2, webCamTextureToMatHelper.requestedHeight / 2);

        minX = settingClass.falabSettings.visualSetting.mode_SingleGuest.detectAreaMinX;
        maxX = settingClass.falabSettings.visualSetting.mode_SingleGuest.detectAreaMaxX;
        minY = settingClass.falabSettings.visualSetting.mode_SingleGuest.detectAreaMinY;
        maxY = settingClass.falabSettings.visualSetting.mode_SingleGuest.detectAreaMaxY;
    }

    private void InitializePassengerMode()
    {
        minX = settingClass.falabSettings.visualSetting.mode_Passenger.detectAreaMinX;
        maxX = settingClass.falabSettings.visualSetting.mode_Passenger.detectAreaMaxX;
        minY = settingClass.falabSettings.visualSetting.mode_Passenger.detectAreaMinY;
        maxY = settingClass.falabSettings.visualSetting.mode_Passenger.detectAreaMaxY;

        magicPoint = new Vector2((maxX + minX) / 2, (maxY + minY) / 2);
        // Debug.Log(magicPoint);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
            isForceSmile = !isForceSmile;
    }

    public void OnFaceDataDetected(List<Rect> facesList)
    {
        if(facesDistances != null)
            facesDistances.Clear();

        if (facesList.Count == 0)
            return;

        int closestPoint = 0;
        float closestDistance = 0;

        for(int i = 0; i < facesList.Count; i++)
        {
            // detect landmark points
            List<Vector2> points = facePointDetection.faceLandmarkDetector.DetectLandmark(facesList[i]);
            
            // calc every rect to magicPoint distance
            float distance = Vector2.Distance(magicPoint, points[34]);

            // compare the distance and pick the closest one
            if (i == 0)
            {
                closestDistance = distance;
                closestPoint = 0;
            }
            else
            {
                closestDistance = (distance < closestDistance) ? distance : closestDistance;
                closestPoint = (distance <= closestDistance) ? i : closestPoint;
            }
        }

        closestNosePos.Set(facePointDetection.faceLandmarkDetector.DetectLandmark(facesList[closestPoint])[34].x, facePointDetection.faceLandmarkDetector.DetectLandmark(facesList[closestPoint])[34].y * -1);
        closesEyebrowPos.Set((facePointDetection.faceLandmarkDetector.DetectLandmark(facesList[closestPoint])[20].x + facePointDetection.faceLandmarkDetector.DetectLandmark(facesList[closestPoint])[25].x) / 2, (facePointDetection.faceLandmarkDetector.DetectLandmark(facesList[closestPoint])[20].y + facePointDetection.faceLandmarkDetector.DetectLandmark(facesList[closestPoint])[25].y) / 2 * -1);
        realNosePos.Set(closestNosePos.x * 2 -960, closestNosePos.y * 2 + 1080f, 0f);
        realEyebrowPos.Set(closesEyebrowPos.x * 2 -960, closesEyebrowPos.y + 1080f, 0f);
        FaceInsideDetector(facesList);
        SmileDetector(facesList, closestPoint);
        FaceIsDetected(facesList);
         Debug.Log(closestNosePos);
    }

    public void SmileDetector(List<Rect> facesList, int closestPoint)
    {
        List<Vector2> closestFace = facePointDetection.faceLandmarkDetector.DetectLandmark(facesList[closestPoint]);
        
        // lipWidth / jawWidth
        float lipWidth = Mathf.Abs(closestFace[49].x - closestFace[55].x);
        float jawWidth = Mathf.Abs(closestFace[3].y - closestFace[15].y);
        float ratio = lipWidth / jawWidth;

        if (ratio > smileRatio || isForceSmile)
            isSmiling = true;
        else
            isSmiling = false;
    }

    public void FaceInsideDetector(List<Rect> facesList)
    {
        if (facesList.Count == 0)
            isHere = false;
        else
        {
            if (closestNosePos.x > minX && closestNosePos.x < maxX && closestNosePos.y > minY && closestNosePos.y < maxY)
                isHere = true;
            else
                isHere = false;
        }
    }

    public void FaceIsDetected(List<Rect> facesList)
    {
        if (facesList.Count > 0)
            isDetected = true;
        else
            isDetected = false;
    }
}
