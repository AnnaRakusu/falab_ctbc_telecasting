using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class MusicController : MonoBehaviour
{
    public AudioSource backgroundMusic01;
    public AudioSource backgroundMusic02;
    public AudioSource backgroundMusic03;
    public AudioSource slideShowBgm;
    public AudioSource buttonSoundEffect;
    public AudioSource touchDoorMusic_long;
    public AudioSource touchDoorMusic_short;
    public AudioSource finishSignMusic;
    public AudioSource vipMusic;

    public WebsocketConnector websocketConnector;

    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this);

        //backgroundMusic01.Play();
        //backgroundMusic02.Play();
        //slideShowBgm.volume = 0.2f;
        //slideShowBgm.Play();
    }

    public void PlayTouchDoorMusic()
    {
        if(SceneManager.GetActiveScene().name != "Mode_Passenger")
        {
            touchDoorMusic_long.Play();
            vipMusic.Play();
        }
        else
        {
            touchDoorMusic_short.Play();
        }
    }

    public void PlayFinishButtonSoundEffect()
    {
        finishSignMusic.Play();
    }

    public void ClickButtonSoundEffect()
    {
        buttonSoundEffect.Play();
    }

    public void ShouldPlayBgm()
    {
        websocketConnector = GameObject.Find("WebsocketConnector").GetComponent<WebsocketConnector>();

        Debug.Log(SceneManager.GetActiveScene().name);
        if (websocketConnector.currentMode == 4 || websocketConnector.currentMode == 5)
        {
            backgroundMusic03.DOFade(0f, 2f);
            vipMusic.DOFade(0f, 2f).OnComplete(vipMusic.Stop);
        }
        else
            backgroundMusic03.DOFade(1f, 2f);
        /*
        websocketConnector = GameObject.Find("WebsocketConnector").GetComponent<WebsocketConnector>();
        
        if (websocketConnector.currentMode == 4 || websocketConnector.currentMode == 5)
            slideShowBgm.DOFade(0.2f, 2f);
        else
            slideShowBgm.DOFade(0f, 1f);
        */
    }

    public void PassengerModeMusic()
    {
        backgroundMusic03.DOFade(1f, 2f);
    }
}
