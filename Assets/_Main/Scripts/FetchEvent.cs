using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;


public class FetchEvent : MonoBehaviour
{
    static public List<string> eventInformationPaths = new List<string>();
    static public List<bool> isImages = new List<bool>();
    static public float[] lastTime;
    void Awake()
    {
        /*
        //eventInformationPaths.Insert(int index, T item);
        eventInformationPaths.Add("https://wall.bahamut.com.tw/B/32/emihqwwysc8butqu2ytfm0t5iwbcnl5rahelkes7.JPG");
        eventInformationPaths.Add("https://s.yimg.com/zp/MerchandiseImages/2DF23A1ACD-SP-8785291.jpg");
        eventInformationPaths.Add("https://test-videos.co.uk/vids/bigbuckbunny/mp4/h264/360/Big_Buck_Bunny_360_10s_5MB.mp4");
        //lastTime = new float[eventInformationPaths.Count];
        lastTime = new float[] { 5.0f, 2.0f, 2.0f };
        isImages.Add(true);
        isImages.Add(true);
        isImages.Add(false);
        */
    }

    void Start()
    {
        /*
        DeleteFiles();
        GetFormat();
        */
    }

    void Update()
    {
        
    }

    //if the event information is a image, set the List<bool> isImage "true"
    void GetFormat()
    {
        /*
        if (eventInformationPaths != null)
        {
            for (int i = 0; i < eventInformationPaths.Count; i++)
            {
                string url = eventInformationPaths[i].ToString();
                string[] urls = url.Split('.');
                string format = urls[urls.Length - 1].ToLower();
                
                if (format == "png" || format == "jpg")
                {
                    isImages.Insert(i, true);
                    StartCoroutine(ImageBundle(i, url));
                }
                else if (format == "wav" || format == "mp4")
                {
                    isImages.Insert(i, false);
                    StartCoroutine(VideoBundle(i, url));
                }
            }
        }
        */
    }

    /*
    //Empty the Folder (Assets >　StreamingAssets > Event)
    public bool DeleteFiles()
    {
        if (Directory.Exists(Application.streamingAssetsPath + "/Event/"))
        {
            DirectoryInfo direction = new DirectoryInfo(Application.streamingAssetsPath + "/Event/");
            FileInfo[] files = direction.GetFiles("*", SearchOption.AllDirectories);

            for (int i = 0; i < files.Length; i++)
            {
                string FilePath = Application.streamingAssetsPath + "/Event/" + "/" + files[i].Name;
                File.Delete(FilePath);
            }
            return true;
        }
        return false;
    }

    //Get downloaded asset bundle
    IEnumerator ImageBundle(int i, string url)
    {
        string savePath = Application.streamingAssetsPath + "/Event/" +  i + ".png";

        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("錯誤: " + www.error);
        }
        else
        {
            Texture2D loadedTexture = DownloadHandlerTexture.GetContent(www);
            Sprite sprite = Sprite.Create(loadedTexture, new Rect(0f, 0f, loadedTexture.width, loadedTexture.height), Vector2.zero);
            byte[] spriteBytes = sprite.texture.EncodeToPNG();
            File.WriteAllBytes(savePath, spriteBytes);
            Debug.Log("圖片已經下載到" + savePath);
        }
    }

    //Get downloaded asset bundle
    IEnumerator VideoBundle(int i, string url)
    {
        string savePath = Application.streamingAssetsPath + "/Event/" + i + ".mp4";

        UnityWebRequest www = UnityWebRequest.Get(url);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("錯誤: " + www.error);
        }
        else
        {
            File.WriteAllBytes(savePath, www.downloadHandler.data);
            UnityWebRequest video = new UnityWebRequest(savePath);
            Debug.Log("影片已經下載到" + savePath);
        }
    }
    */
}
