using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorResetTrigger : MonoBehaviour
{
    public Animator thisAnimator;

    private void Start()
    {
        thisAnimator = GetComponent<Animator>();
    }

    public void CallInTrigger()
    {
        if (thisAnimator.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
        {
            thisAnimator.SetTrigger("in");
        }
    }

    public void CallOutTrigger()
    {
        if (thisAnimator.GetCurrentAnimatorStateInfo(0).IsName("loop"))
        {
            thisAnimator.SetTrigger("out");
        }
    }
}
