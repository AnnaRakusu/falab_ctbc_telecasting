﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SeqAnimatorSet
{
    public Animator thisAnimator;
    public string triggerName;
}

public class SeqAnimatorController : MonoBehaviour
{
    public SeqAnimatorSet[] seqAnimators;

    public void CallSeqAnimator(int whichOne)
    {
        if (seqAnimators[whichOne].thisAnimator.GetBool(seqAnimators[whichOne].triggerName))
        {
            seqAnimators[whichOne].thisAnimator.ResetTrigger(seqAnimators[whichOne].triggerName);
        }

        seqAnimators[whichOne].thisAnimator.SetTrigger(seqAnimators[whichOne].triggerName);
    }
}
