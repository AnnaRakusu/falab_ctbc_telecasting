using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using DG.Tweening;
using TMPro;
using UnityEngine.UI;

public class FacePointTest : MonoBehaviour
{
    public CTBCFaceDealer cTBCFaceDealer;
    public UsefulFunc usefulFunc;

    public bool isHere = false;
    private bool isSmiling = false;
    private Vector3 nosePose = new Vector3();

    public GameObject[] gameObjectFollowFace = new GameObject[2];
    public List<VideoUIController> videos;
    public TextMeshProUGUI guestCompany;
    public TextMeshProUGUI guestJob;
    public TextMeshProUGUI guestName;
    public TextMeshProUGUI guestGender;
    public TextMeshProUGUI visitingRecordTitle;
    public TextMeshProUGUI visitingRecordText;
    public Animator image_SmileHint;
    public MediaPlayer video_smileEnergyBar;
    public bool isPhotoTaken = false;
    public bool isAnimInDone = false;
    public bool startEnergyBar = false;
    private float faceDetectionTime = 0;
    public float faceDetectionTimeThreshold = 8;
    private Sequence seqIn;
    private Sequence seqOut;

    private int energyBarStart = 360;
    private int energyBarEnd = 4200;

    void Start()
    {
        seqIn = DOTween.Sequence();
        seqOut = DOTween.Sequence();

        var tempColor = Color.white;
        tempColor.a = 0;
        guestCompany.color = tempColor;
        guestJob.color = tempColor;
        guestName.color = tempColor;
        guestGender.color = tempColor;
        visitingRecordTitle.color = tempColor;
        visitingRecordText.color = tempColor;

        image_SmileHint.gameObject.GetComponent<Image>().color = tempColor;
        image_SmileHint.ResetTrigger("in");
        image_SmileHint.ResetTrigger("out");

    }

    void Update()
    {
        LoadFaceInformation();
        DetectSmile();
    }

    private void LoadFaceInformation()
    {
        //isHere = cTBCFaceDealer.isHere;
        isSmiling = cTBCFaceDealer.isSmiling;
        nosePose = cTBCFaceDealer.realNosePos;

        gameObjectFollowFace[0].transform.localPosition = nosePose;
        gameObjectFollowFace[1].transform.localPosition = nosePose;
    }

    private void DetectSmile()
    {
        if (isHere)
        {
            // just do once
            if (!isAnimInDone)
            {
                isAnimInDone = true;
                StartCoroutine(AnimIn());
            }
            /*
            if (startEnergyBar)
            {
                faceDetectionTime += Time.deltaTime;

                if (isSmiling ||
                    video_smileEnergyBar.Control.GetCurrentTimeMs() < energyBarStart || video_smileEnergyBar.Control.GetCurrentTimeMs() > energyBarEnd ||
                    faceDetectionTime > faceDetectionTimeThreshold)
                {
                    video_smileEnergyBar.Control.Play();
                    if (video_smileEnergyBar.Control.GetCurrentTimeMs() > energyBarEnd && !isPhotoTaken)
                    {
                        isPhotoTaken = true;
                    }
                }
                else if (!isSmiling)
                    video_smileEnergyBar.Control.Pause();
            }
            */
        }
        else if (!isHere)
        {
            /*
            video_smileEnergyBar.Control.Pause();
            video_smileEnergyBar.Control.Rewind();
            faceDetectionTime = 0;
            */
            if (isAnimInDone)
            {
                isAnimInDone = false;
                AnimOut();
            }
        }
    }
    public void InitializeAnim()
    {
        seqIn = DOTween.Sequence();
        seqOut = DOTween.Sequence();

        var tempColor = Color.white;
        tempColor.a = 0;

        guestCompany.color = tempColor;
        guestJob.color = tempColor;
        guestName.color = tempColor;
        guestGender.color = tempColor;
        visitingRecordTitle.color = tempColor;
        visitingRecordText.color = tempColor;

        image_SmileHint.gameObject.GetComponent<Image>().color = tempColor;
        image_SmileHint.ResetTrigger("in");
        image_SmileHint.ResetTrigger("out");

    }

    private IEnumerator AnimIn()
    {
        InitializeAnim();
        video_smileEnergyBar.Control.Rewind();

        DOTween.KillAll();
        seqIn = DOTween.Sequence().SetId("faceUIAnim");
        seqIn.Append(guestCompany.DOFade(1f, 1.2f).SetDelay(3.8f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(guestJob.DOFade(1f, 1.2f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Append(guestName.DOText("���ة�", 1.2f).SetEase(Ease.OutSine).SetDelay(0.5f).SetId("faceUIAnim"))
            .Join(guestName.DOFade(1f, 0.5f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Append(guestGender.DOText("�k�h", 0.2f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(guestGender.DOFade(1f, 0.2f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Append(visitingRecordTitle.DOFade(1f, 0.75f).SetEase(Ease.OutSine).SetDelay(0.5f).SetId("faceUIAnim"))
            .Join(visitingRecordTitle.transform.DOLocalMove(new Vector3(1165, -395f, 0), 1f).From().SetEase(Ease.OutBack).SetId("faceUIAnim"))
            .Join(visitingRecordText.DOFade(1f, 1.5f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(visitingRecordText.transform.DOLocalMove(new Vector3(1165, -545, 0), 1f).From().SetEase(Ease.OutBack).SetId("faceUIAnim"))
            .AppendInterval(0.5f).SetEase(Ease.OutSine).SetId("faceUIAnim").OnComplete(SetStartEnergyBar);
            
       
        yield return new WaitForSeconds(1f);
        videos[0].TriggerIn();

        yield return new WaitForSeconds(1.8f);
        usefulFunc.FadeImg(image_SmileHint.gameObject.GetComponent<Image>(), 1f, 0.5f, "in");
        image_SmileHint.SetTrigger("in");

        yield return new WaitForSeconds(1f);
        videos[1].TriggerIn();

            
    }

    void SetStartEnergyBar()
    {
        startEnergyBar = true;
    }


    void AnimOut()
    {
        StopCoroutine("AnimIn");
        DOTween.KillAll();
        faceDetectionTime = 0;

        startEnergyBar = false;

        videos[0].TriggerOut();

        videos[1].TriggerOut();

        video_smileEnergyBar.Control.Stop();
        video_smileEnergyBar.gameObject.GetComponent<DisplayUGUI>().DOFade(0f, 0.5f).SetEase(Ease.OutCubic);
        image_SmileHint.SetTrigger("out");
        usefulFunc.FadeImg(image_SmileHint.gameObject.GetComponent<Image>(), 0f, 0.5f, "faceUIAnim", Ease.InOutCubic);


        seqOut = DOTween.Sequence().SetId("faceUIAnim");
        seqOut.Append(guestCompany.DOFade(0f, 0.5f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(guestJob.DOFade(0f, 0.5f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(guestName.DOFade(0f, 1f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(guestGender.DOFade(0f, 1f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(visitingRecordTitle.DOFade(0f, 1.5f).SetEase(Ease.OutSine).SetId("faceUIAnim"))
            .Join(visitingRecordText.DOFade(0f, 1.5f).SetEase(Ease.OutSine).SetId("faceUIAnim")).SetId("faceUIAnim");

        for (int i = 0; i < videos.Count; i++)
        {
            videos[i].gameObject.transform.parent.GetChild(0).GetComponent<MediaPlayer>().Control.Rewind();
            videos[i].gameObject.transform.parent.GetChild(1).GetComponent<MediaPlayer>().Control.Rewind();
        }
    }
}
