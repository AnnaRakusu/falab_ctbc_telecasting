using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using UnityEngine.PostProcessing;

public class DemoShowAllController : MonoBehaviour
{
    [Header("Area Introduction")]

    public string secondLanguage = "en";

    public RequestInformationData informationData;

    public Text title;
    public Text titleSecond;

    public Text description;
    public Text descriptionSecond;

    public float depthZValue = 500;

    public float fadeInOutTime = 2;
    public float inOutDifferTime = 0.3f;

    public float MainInfoDisplayTime = 2;

    private JSONNode informationDataJSON;

    [Header("Area Guests Slide Show")]

    private JSONNode guestsDataJSON;

    public VIPMainController vipmc;


    //Get JSON Data Reader And get the language setting
    private Vector3 titlePos;
    private Vector3 title2Pos;
    private Vector3 descPos;
    private Vector3 desc2Pos;

    #region Anna
    public SettingClass settingClass;
    public SerialSaver serialSaver;
    public TextMeshProUGUI personTime;
    public GameObject[] gameObjectsSetActiveOnAwake;
    public FrostedGlassController frostedGlassController;
    public UsefulFunc usefulFunc;
    public PostProcessingProfile postProcessingProfile;
    public Image blurImage;
    
    public bool isBtnShow;
    public bool isTriggerInOnAwake;
    public GameObject BTN;
    public Animator btnDeco;
    public Animator btnTicket;
    public GameObject[] particles = new GameObject[4];
    #endregion

    private void Awake()
    {
        InitializePartical();
        usefulFunc.SetAOZero(postProcessingProfile, blurImage);
        var tempColor = new Color(255, 255, 255, 0);
        blurImage.color = tempColor;

        title.color = ReturnColorAlphaToZero(title);
        titleSecond.color = ReturnColorAlphaToZero(titleSecond);

        description.color = ReturnColorAlphaToZero(description);
        descriptionSecond.color = ReturnColorAlphaToZero(descriptionSecond);

        for (int i = 0; i < gameObjectsSetActiveOnAwake.Length; i++)
            gameObjectsSetActiveOnAwake[i].SetActive(true);

        serialSaver.Load();
        personTime.text = "���[�H���G" + settingClass.falabSettings.dynamicAdjustment.personTime;

        if (isBtnShow && !isTriggerInOnAwake)
        {
            BTN.SetActive(true);
            btnDeco.ResetTrigger("in");
            btnDeco.ResetTrigger("out");
            btnTicket.ResetTrigger("in");
            btnTicket.ResetTrigger("out");
            btnTicket.transform.GetComponent<Button>().interactable = false;
        }
        else if(isBtnShow && isTriggerInOnAwake)
        {
            BTN.SetActive(true);
            btnDeco.SetTrigger("in");
            btnTicket.SetTrigger("in");
            btnTicket.transform.GetComponent<Button>().interactable = true;
        }
        else
        {
            BTN.SetActive(false);
            btnTicket.transform.GetComponent<Button>().interactable = false;
        }
    }

    private Color ReturnColorAlphaToZero(Text inputText)
    {
        return new Color(inputText.color.r, inputText.color.g, inputText.color.b, 0);
    }

    public ActivitiesMainController amc;

    //This should be called from RequestInformationData
    public void AssignInformations()
    {
        informationDataJSON = JSON.Parse( informationData.resultJsonString );

        int twOrder = ReturnLanguageDataOrder("tw");

        title.text = informationDataJSON[twOrder]["Title"];
        description.text = informationDataJSON[twOrder]["Description"];

        int secondOrder = ReturnLanguageDataOrder(secondLanguage);

        titleSecond.text = informationDataJSON[secondOrder]["Title"];
        descriptionSecond.text = informationDataJSON[secondOrder]["Description"];

        LayoutRebuilder.ForceRebuildLayoutImmediate(title.transform.parent.GetComponent<RectTransform>());

        titlePos = new Vector3(title.transform.localPosition.x, title.transform.localPosition.y, title.transform.localPosition.z);
        title2Pos = new Vector3(titleSecond.transform.localPosition.x, titleSecond.transform.localPosition.y, titleSecond.transform.localPosition.z);
        descPos = new Vector3(description.transform.localPosition.x, description.transform.localPosition.y, description.transform.localPosition.z);
        desc2Pos = new Vector3(descriptionSecond.transform.localPosition.x, descriptionSecond.transform.localPosition.y, descriptionSecond.transform.localPosition.z);

        if (!amc.isOnlyActivities)
        {
            if (!vipmc.startFromMap)
            {
                MakeInformationStartToPlay();
            }
            else
            {
                vipmc.GoToPlayMap();
            }
        }
    }

    private int ReturnLanguageDataOrder(string languageType)
    {
        int whichOne = 0;

        for (int i = 0; i < informationDataJSON.Count; i++) {
            if (informationDataJSON[i]["Language"] == languageType)
            {
                whichOne = i;
                break;
            }
        }

        return whichOne;
    }

    public void MakeInformationStartToPlay()
    {
        Debug.Log("first");
        BTN.SetActive(true);

        #region first blur city
        frostedGlassController.targetFrostedValue = settingClass.falabSettings.visualSetting.mode_SlideShow.targetFrostedValue_default;
        frostedGlassController.changeTime = 0f;
        usefulFunc.SetAOZero(postProcessingProfile, blurImage);
        frostedGlassController.BlurCity();
        #endregion

        title.transform.localPosition = titlePos;
        titleSecond.transform.localPosition = title2Pos;
        description.transform.localPosition = descPos;
        descriptionSecond.transform.localPosition = desc2Pos;

        LayoutRebuilder.ForceRebuildLayoutImmediate(title.transform.parent.GetComponent<RectTransform>());

        vipmc.notImportantRound = 0;

        title.rectTransform.DOLocalMoveZ(depthZValue, fadeInOutTime).From().SetEase(Ease.OutBack).OnStart(() => {
            title.DOFade(1, fadeInOutTime).SetEase(Ease.InOutSine);
        }).SetDelay(0);

        titleSecond.rectTransform.DOLocalMoveZ(depthZValue, fadeInOutTime).From().SetEase(Ease.OutBack).OnStart(() => {
            titleSecond.DOFade(1, fadeInOutTime).SetEase(Ease.InOutSine);
        }).SetDelay(inOutDifferTime);

        description.rectTransform.DOLocalMoveZ(depthZValue, fadeInOutTime).From().SetEase(Ease.OutBack).OnStart(() => {
            description.DOFade(1, fadeInOutTime).SetEase(Ease.InOutSine);
        }).SetDelay(inOutDifferTime * 2);

        descriptionSecond.rectTransform.DOLocalMoveZ(depthZValue, fadeInOutTime).From().SetEase(Ease.OutBack).OnStart(() => {
            descriptionSecond.DOFade(1, fadeInOutTime).SetEase(Ease.InOutSine);
        }).SetDelay(inOutDifferTime * 3).OnComplete(InformationPlayCounting);
    }

    void InformationPlayCounting()
    {
        DOVirtual.DelayedCall(MainInfoDisplayTime, () =>
        {
            MakeInformationStartToFade();
        });
    }

    void MakeInformationStartToFade()
    {
        title.rectTransform.DOLocalMoveZ(depthZValue, fadeInOutTime).SetEase(Ease.InBack).OnStart(() => {
            title.DOFade(0, fadeInOutTime).SetEase(Ease.InOutSine);
        }).SetDelay(inOutDifferTime * 3).OnComplete(StartToShowGuestsInformation);

        titleSecond.rectTransform.DOLocalMoveZ(depthZValue, fadeInOutTime).SetEase(Ease.InBack).OnStart(() => {
            titleSecond.DOFade(0, fadeInOutTime).SetEase(Ease.InOutSine);
        }).SetDelay(inOutDifferTime * 2);

        description.rectTransform.DOLocalMoveZ(depthZValue, fadeInOutTime).SetEase(Ease.InBack).OnStart(() => {
            description.DOFade(0, fadeInOutTime).SetEase(Ease.InOutSine);
        }).SetDelay(inOutDifferTime * 1);

        descriptionSecond.rectTransform.DOLocalMoveZ(depthZValue, fadeInOutTime).SetEase(Ease.InBack).OnStart(() =>
        {
            descriptionSecond.DOFade(0, fadeInOutTime).SetEase(Ease.InOutSine);
        }).SetDelay(0);
    }

    void StartToShowGuestsInformation()
    {

        BTN.SetActive(true);
        btnDeco.SetTrigger("in");
        btnTicket.SetTrigger("in");
        btnTicket.transform.GetComponent<Button>().interactable = true;

        #region Second Blur City
        frostedGlassController.targetFrostedValue = settingClass.falabSettings.visualSetting.mode_SlideShow.targetFrostedValue_changed;
        frostedGlassController.changeTime = 5f;
        usefulFunc.SetAOZero(postProcessingProfile, blurImage);
        frostedGlassController.BlurCity();
        #endregion

        Debug.Log("Guest!!!!");
        vipmc.StartToPlayVIPS();

        if (isBtnShow)
        {
            Debug.Log("in");
            btnDeco.SetTrigger("in");
            btnTicket.SetTrigger("in");
            btnTicket.transform.GetComponent<Button>().interactable = true;
        }

    }

    private void InitializePartical()
    {
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().glowingObjectGroupNumber = settingClass.falabSettings.visualSetting.paricleSetting.arc.glowingObjectGroupNumber;
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().effectRandomTime.minTime = settingClass.falabSettings.visualSetting.paricleSetting.arc.minTime;
        particles[0].transform.GetChild(1).GetChild(0).GetComponent<CTBCParticlesController>().effectRandomTime.maxTime = settingClass.falabSettings.visualSetting.paricleSetting.arc.maxTime;

        var mainNumber = particles[3].GetComponent<ParticleSystem>().main;
        mainNumber.startSize = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.number.minSize, settingClass.falabSettings.visualSetting.paricleSetting.number.maxSize);
        var emissionNumber = particles[3].GetComponent<ParticleSystem>().emission;
        emissionNumber.rateOverTime = new ParticleSystem.MinMaxCurve(settingClass.falabSettings.visualSetting.paricleSetting.number.minEmit, settingClass.falabSettings.visualSetting.paricleSetting.number.maxEmit);
    }
}
