using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class VIPPresets : MonoBehaviour
{
    public Text textCompany;
    public Text textJob;
    public OnlineTextureLoader imagePerson;
    public OnlineTextureLoader imageSign;

    public void SetCompany(string inputString)
    {
        textCompany.text = inputString;
    }

    public void SetJob(string inputString)
    {
        textJob.text = inputString;
    }

    public void SetPersonImage(string inputString)
    {
        imagePerson.imageUrl = inputString;
    }

    public void SetSignImage(string inputString)
    {
        imageSign.imageUrl = inputString;
    }

}
