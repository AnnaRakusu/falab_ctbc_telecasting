using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using System.Threading.Tasks;
using UnityEngine.UI;
using DG.Tweening;

public class ImageLoader : MonoBehaviour
{
    public RawImage thisImage;
    public string imageURL; 
    Texture2D _texture;
    
    public async void StartToLoadImage()
    {
        _texture = await GetRemoteTexture(imageURL);
        thisImage.texture = _texture;
        thisImage.color = new Color(1, 1, 1, 0);
        Debug.Log("_texture.width : " + _texture.width + " , _texture.height : " + _texture.height);
        thisImage.GetComponent<AspectRatioFitter>().aspectRatio = _texture.width * 1.0f / _texture.height;
    }

    public async Task<Texture2D> GetRemoteTexture(string url)
    {
        using (UnityWebRequest www = UnityWebRequestTexture.GetTexture(url))
        {
            // begin request:
            var asyncOp = www.SendWebRequest();

            // await until it's done: 
            while (asyncOp.isDone == false)
                await Task.Delay(1000 / 30);//30 hertz

            // read results:
            if (www.result == UnityWebRequest.Result.ConnectionError)
            // if( www.result!=UnityWebRequest.Result.Success )// for Unity >= 2020.1
            {
                #if DEBUG
                Debug.Log($"{www.error}, URL:{www.url}");
                #endif

                // nothing to return on error:
                return null;
            }
            else
            {
                // return valid results:
                return DownloadHandlerTexture.GetContent(www);
            }
        }
    }

    void OnDestroy() => Dispose();
    public void Dispose() => Object.Destroy(_texture);// memory released, leak otherwise
}
