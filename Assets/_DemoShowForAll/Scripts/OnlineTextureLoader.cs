using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using UnityEngine.UI;

public class OnlineTextureLoader : MonoBehaviour
{
    public string imageUrl;
    private RawImage mainImage;

    private void Start()
    {
        mainImage = GetComponent<RawImage>();
        StartToLoadTexture();
    }

    public void StartToLoadTexture()
    {
        StartCoroutine(GetTexture());
    }

    IEnumerator GetTexture()
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(imageUrl);
        yield return www.SendWebRequest();

        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {
            Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            mainImage.texture = myTexture;
        }
    }
}