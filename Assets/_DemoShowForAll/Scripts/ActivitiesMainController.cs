using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using RenderHeads.Media.AVProVideo;
using DG.Tweening;
using UnityEngine.UI;

[System.Serializable]
public class PlayingData
{
    public ImageLoader image;
    public MediaPlayer video;
}

public class ActivitiesMainController : MonoBehaviour
{
    public RequestActivitiesData rad;

    public JSONNode activitiesJSON;

    public Transform activitiesParent;

    public ImageLoader activityImage;
    public MediaPlayer activityVideo;

    public PlayingData[] playingData;

    public string baseURL = "http://future-action.com/FALab/CTBCEntryWall/resources/ActivitiesMedium/";

    public float imageDuration = 6;

    public GameObject BTN;
    public Animator btnDeco;
    public Animator btnTicket;
    public SettingClass settingClass;
    public DemoShowAllController demoShowAllController;

    public void OnDataReceived()
    {
        activitiesJSON = JSON.Parse(rad.resultJsonString);
        GenerateAllActivities();
        imageDuration = float.Parse(settingClass.falabSettings.dynamicAdjustment.slideShowInterval);
        
        if (demoShowAllController.isBtnShow)
            BTN.SetActive(true);
        else
            BTN.SetActive(false);
    }

    void GenerateAllActivities()
    {
        playingData = new PlayingData[ activitiesJSON.Count ];

        for (int i = 0; i < activitiesJSON.Count; i++)
        {
            playingData[i] = new PlayingData();

            if (activitiesJSON[i]["fileType"] == "image")
            {
                //_material.mainTexture
                ImageLoader temp = Instantiate(activityImage);
                temp.imageURL = baseURL + activitiesJSON[i]["fileName"];
                temp.transform.parent = activitiesParent;
                temp.transform.localPosition = Vector3.zero;
                temp.StartToLoadImage();
                playingData[i].image = temp;
            }
            else if (activitiesJSON[i]["fileType"] == "video")
            {
                MediaPlayer temp = Instantiate(activityVideo);
                temp.PlatformOptionsWindows.path = baseURL + activitiesJSON[i]["fileName"];
                temp.transform.parent = activitiesParent;
                temp.transform.localPosition = Vector3.zero;
                temp.gameObject.SetActive(true);
                playingData[i].video = temp;
            }
        }

        if (isOnlyActivities)
            DOVirtual.DelayedCall(1, StartToPlayActivities);
    }

    public bool isOnlyActivities = false;

    private int currentPlaying = 0;

    public void StartToPlayActivities()
    {
        if(activitiesJSON.Count != 0)
        {
            if (currentPlaying != activitiesJSON.Count)
            {
                if (playingData[currentPlaying].image != null)
                {
                    playingData[currentPlaying].image.thisImage.DOFade(1, 1f).SetEase(Ease.InOutSine).OnComplete(() =>
                    {
                        if (currentPlaying == 0)
                        {
                            ColorOnAll();
                        }

                        playingData[currentPlaying].image.thisImage.DOFade(0, 1f).SetDelay(imageDuration).SetEase(Ease.InOutSine).OnStart(() =>
                        {
                            currentPlaying++;
                            StartToPlayActivities();
                        });
                    });
                }
                else
                {
                    playingData[currentPlaying].video.Control.Rewind();
                    playingData[currentPlaying].video.Control.Play();
                    playingData[currentPlaying].video.GetComponent<DisplayUGUI>().DOFade(1, 1f).SetEase(Ease.InOutSine).SetId("ActVideo");
                    DOVirtual.DelayedCall(playingData[currentPlaying].video.Info.GetDurationMs() / 1000.0f, () =>
                    {
                        DOTween.Kill("ActVideo");
                        playingData[currentPlaying].video.GetComponent<DisplayUGUI>().DOFade(0, 1f).SetEase(Ease.InOutSine);
                        currentPlaying++;
                        StartToPlayActivities();
                    });
                }
            }
            else
            {
                //Playing Over
                if (!isOnlyActivities)
                {
                    currentPlaying = 0;
                    DemoShowAllController tempDemoShowAllControllerl = FindObjectOfType<DemoShowAllController>();
                    tempDemoShowAllControllerl.MakeInformationStartToPlay();
                    btnDeco.SetTrigger("out");
                    btnTicket.SetTrigger("out");
                    btnTicket.transform.GetComponent<Button>().interactable = false;
                }
                else
                {
                    currentPlaying = 0;
                    StartToPlayActivities();
                    BTN.SetActive(false);
                }
            }
        }
        else
        {
            //Directly Skip
            DemoShowAllController tempDemoShowAllControllerl = FindObjectOfType<DemoShowAllController>();
            tempDemoShowAllControllerl.MakeInformationStartToPlay();
        }
    }

    private void ColorOnAll()
    {
        for (int i = 0; i < activitiesJSON.Count; i++)
        {
            if (playingData[i].image != null)
            {
                playingData[i].image.thisImage.color = Color.white;
            }
            else
            {
                playingData[i].video.GetComponent<DisplayUGUI>().color = Color.white;
            }
        }
    }
}
