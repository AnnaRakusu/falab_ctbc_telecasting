using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RenderHeads.Media.AVProVideo;
using DG.Tweening;
using UnityEngine.UI;

[System.Serializable]
public class OnlyLoopVideoKeyFrame
{
    public int loopStartFrame;
}

public class AVProVideoDirectLoop : MonoBehaviour
{

    public OnlyLoopVideoKeyFrame videoKeyFrame;

    [Header("Simulated Animator Media Players")]
    public MediaPlayer loopMediaPlayer;

    [Header("Media Players UI")]
    public DisplayUGUI loopMediaPlayerUI;

    [Header("Settings")]
    public float inFadeInOutTime = 0.5f;
    public float outFadeInOutTime = 0.5f;
    public float fadeAlpha = 0.2f;

    // Start is called before the first frame update
    void Start()
    {
        BindingEventsToVideo();
        InitVideosToAlphaZero();
    }

    void BindingEventsToVideo()
    {
        loopMediaPlayer.Events.AddListener(OnVideoEvent);
    }

    void InitVideosToAlphaZero()
    {
        loopMediaPlayerUI.color = new Color(loopMediaPlayerUI.color.r, loopMediaPlayerUI.color.g, loopMediaPlayerUI.color.b, 0);
    }

    public void TriggerIn()
    {
        loopMediaPlayerUI.DOFade(fadeAlpha, inFadeInOutTime).SetEase(Ease.OutSine);
        loopMediaPlayer.Control.Play();
    }

    public void TriggerOut()
    {
        loopMediaPlayerUI.DOFade(0, outFadeInOutTime).SetEase(Ease.OutSine).OnComplete(OnLoopVideoOutCompleted);
    }

    void OnLoopVideoOutCompleted()
    {
        GoBackToLoopInitFrame();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void OnVideoEvent(MediaPlayer mp, MediaPlayerEvent.EventType et, ErrorCode errorCode)
    {
        switch (et)
        {
            case MediaPlayerEvent.EventType.FinishedPlaying:
                GoBackToLoopInitFrame();
                break;
            case MediaPlayerEvent.EventType.ReadyToPlay:
                GoBackToLoopInitFrame();
                break;
        }
        Debug.Log("Event: " + et.ToString());
    }

    void GoBackToLoopInitFrame()
    {
        float frameRate = loopMediaPlayer.Info.GetVideoFrameRate();
        float time = Helper.ConvertFrameToTimeSeconds(videoKeyFrame.loopStartFrame, frameRate);
        loopMediaPlayer.Control.Seek(time * 1000f);
    }
}
