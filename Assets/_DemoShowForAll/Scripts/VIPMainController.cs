using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;
using RenderHeads.Media.AVProVideo;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.PostProcessing;
public class VIPMainController : MonoBehaviour
{
    public RequestGuestData requestGuestData;

    public JSONNode guestJSON;

    public VIPPresets vipPresetPrefab;
    public VIPPresets vipNotImportantPresetPrefab;

    private List<VIPPresets> allImportantVIPS = new List<VIPPresets>();
    private List<VIPPresets> allNotImportantVIPS = new List<VIPPresets>();

    public Transform parentTransform;

    public AVProVideoDirectLoop vipLinesBall;
    public AVProVideoDirectLoop vipLines;
    public AVProVideoDirectLoop vipLinesGlowing;
    public AVProVideoDirectLoop vipLineAdd;

    public float vipStayTime = 5f;

    public AVProVideoDirectLoop notImportantBall_0;
    public AVProVideoDirectLoop notImportantBall_1;
    public AVProVideoDirectLoop notImportantBall_2;
    public AVProVideoDirectLoop notImportantBallAdd_0;
    public AVProVideoDirectLoop notImportantBallAdd_1;
    public AVProVideoDirectLoop notImportantBallAdd_2;

    private int notImporantCurrentNumber = 0;

    public MediaPlayer mapMp;

    private int notImportantCount = 0;
    public int notImportantRound = 0;

    public float mapDelayTime;
    public FrostedGlassController frostedGlassController;
    public UsefulFunc usefulFunc;
    public PostProcessingProfile postProcessingProfile;
    public Image blurImage;


    public void OnDataReceived()
    {
        guestJSON = JSON.Parse( requestGuestData.resultJsonString );
        GenerateAllImportantObjects();
    }

    void GenerateAllImportantObjects()
    {
        for (int i = 0; i < guestJSON.Count; i++)
        {
            if (guestJSON[i]["important"] == "0")
                notImportantCount++;
        }

        if(notImportantCount < 3)
        {
            if(notImportantCount == 2)
            {
                notImportantBall_0.transform.localPosition = new Vector2(-310, 100);
                notImportantBall_1.loopMediaPlayerUI.color = new Color(1, 1, 1, 0);
                notImportantBall_2.transform.localPosition = new Vector2(310, 100);
                notImportantBallAdd_0.transform.localPosition = new Vector2(-310, 100);
                notImportantBallAdd_1.loopMediaPlayerUI.color = new Color(1, 1, 1, 0);
                notImportantBallAdd_2.transform.localPosition = new Vector2(310, 100);
            }
            else if (notImportantCount == 1)
            {
                //notImportantBall_0.transform.localPosition = new Vector2(0, 100);
                notImportantBall_0.loopMediaPlayerUI.color = new Color(1, 1, 1, 0);
                notImportantBall_2.loopMediaPlayerUI.color = new Color(1, 1, 1, 0);
                notImportantBallAdd_0.loopMediaPlayerUI.color = new Color(1, 1, 1, 0);
                notImportantBallAdd_2.loopMediaPlayerUI.color = new Color(1, 1, 1, 0);
            }
        }

        for (int i = 0; i < guestJSON.Count; i++)
        {
            if(guestJSON[i]["important"] == "1")
            {
                VIPPresets tempGenerated = Instantiate(vipPresetPrefab) as VIPPresets;
                tempGenerated.SetCompany(guestJSON[i]["company"]);
                tempGenerated.SetJob(guestJSON[i]["job"]);
                tempGenerated.SetPersonImage("http://future-action.com/FALab/CTBCEntryWall/resources/UploadImages/" + guestJSON[i]["imageName"] + "_RMBG.png");
                tempGenerated.SetSignImage("http://future-action.com/FALab/CTBCEntryWall/resources/UploadImages/" + guestJSON[i]["imageName"] + "_SIGN.png");
                tempGenerated.transform.parent = parentTransform;
                tempGenerated.transform.localPosition = new Vector2(336, 213);
                allImportantVIPS.Add(tempGenerated);
            }
            else
            {
                VIPPresets tempGenerated = Instantiate(vipNotImportantPresetPrefab) as VIPPresets;
                tempGenerated.SetCompany(guestJSON[i]["company"]);
                tempGenerated.SetJob(guestJSON[i]["job"]);
                tempGenerated.SetPersonImage("http://future-action.com/FALab/CTBCEntryWall/resources/UploadImages/" + guestJSON[i]["imageName"] + "_RMBG.png");
                tempGenerated.SetSignImage("http://future-action.com/FALab/CTBCEntryWall/resources/UploadImages/" + guestJSON[i]["imageName"] + "_SIGN.png");
                tempGenerated.transform.parent = parentTransform;

                if(notImportantCount / 3 != notImporantCurrentNumber / 3)
                    tempGenerated.transform.localPosition = new Vector2(-620 + (notImporantCurrentNumber % 3) * 620, 100);
                else
                {
                    if (notImportantCount % 3 == 2)
                    {
                        if(notImporantCurrentNumber % 3 == 0)
                            tempGenerated.transform.localPosition = new Vector2(-310, 100);
                        else if (notImporantCurrentNumber % 3 == 1)
                            tempGenerated.transform.localPosition = new Vector2(310, 100);
                    }
                    else if (notImportantCount % 3 == 1)
                    {
                        tempGenerated.transform.localPosition = new Vector2(0, 100);
                    }
                }
                
                allNotImportantVIPS.Add(tempGenerated);
                notImporantCurrentNumber++;
            }
        }
    }

    public void StartToPlayVIPS()
    {
        if (allImportantVIPS.Count > 0)
        {
            DOVirtual.DelayedCall(0f, vipLines.TriggerIn);
            DOVirtual.DelayedCall(0.8f, vipLinesBall.TriggerIn);
            DOVirtual.DelayedCall(0.8f, vipLineAdd.TriggerIn);
            DOVirtual.DelayedCall(1.4f, vipLinesGlowing.TriggerIn);
            DOVirtual.DelayedCall(2, () => PlayImportantGuests(0));
        }
        else
        {
            if (notImporantCurrentNumber > 0)
            {
                GoToPlayNotImportant();
            }
            else
            {
                //Directly Go To Map!
                GoToPlayMap();
            }
        }
        
    }

    private void PlayImportantGuests(int whichOne)
    {
        allImportantVIPS[whichOne].imagePerson.GetComponent<RawImage>().DOFade(1, 1.2f);
        allImportantVIPS[whichOne].imageSign.GetComponent<RawImage>().DOFade(1, 1).SetDelay(0.8f);
        allImportantVIPS[whichOne].textCompany.DOFade(1, 1).SetDelay(1.5f);
        allImportantVIPS[whichOne].textJob.DOFade(1, 1).SetDelay(1.8f);

        DOVirtual.DelayedCall(vipStayTime + 2.8f, () =>
        {
            FadeOutImportantGuests(whichOne);
        });
    }

    void FadeOutImportantGuests(int whichOne)
    {
        allImportantVIPS[whichOne].imagePerson.GetComponent<RawImage>().DOFade(0, 1.2f).SetDelay(1.3f).OnComplete(() =>
        {
            if(whichOne < allImportantVIPS.Count - 1)
            {
                PlayImportantGuests(whichOne + 1);
            }
            else
            {
                if (notImporantCurrentNumber > 0)
                {
                    GoToPlayNotImportant();
                }
                else
                {
                    //Directly Go To Map!
                    GoToPlayMap();
                }
            }
        });
        allImportantVIPS[whichOne].imageSign.GetComponent<RawImage>().DOFade(0, 1).SetDelay(1f);
        allImportantVIPS[whichOne].textCompany.DOFade(0, 1).SetDelay(0.2f);
        allImportantVIPS[whichOne].textJob.DOFade(0, 1);
    }

    void GoToPlayNotImportant()
    {
        Debug.Log("Not Important!!");
        DOVirtual.DelayedCall(1.4f, vipLines.TriggerOut);
        DOVirtual.DelayedCall(0.8f, vipLinesBall.TriggerOut);
        DOVirtual.DelayedCall(0.8f, vipLineAdd.TriggerOut);
        DOVirtual.DelayedCall(0f, vipLinesGlowing.TriggerOut);
        DOVirtual.DelayedCall(2.4f, () => PlayNotImportant());
    }

    void PlayNotImportant()
    {
        if (notImportantRound == 0 && notImportantCount > 2)
        {
            notImportantBall_0.transform.localPosition = new Vector2(-620, 100);
            notImportantBall_1.transform.localPosition = new Vector2(0, 100);
            notImportantBall_2.transform.localPosition = new Vector2(620, 100);
            notImportantBallAdd_0.transform.localPosition = new Vector2(-620, 100);
            notImportantBallAdd_1.transform.localPosition = new Vector2(0, 100);
            notImportantBallAdd_2.transform.localPosition = new Vector2(620, 100);
        }

        if (notImportantCount - notImportantRound * 3 > 2)
        {
            notImportantBall_0.TriggerIn();
            notImportantBallAdd_0.TriggerIn();
            PlayNotImportantGuests(notImportantRound * 3);

            DOVirtual.DelayedCall(0.8f, () =>
            {
                notImportantBall_1.TriggerIn();
                notImportantBallAdd_1.TriggerIn();
                PlayNotImportantGuests(notImportantRound * 3 + 1);
            });

            DOVirtual.DelayedCall(1.6f, () =>
            {
                notImportantBall_2.TriggerIn();
                notImportantBallAdd_2.TriggerIn();
                PlayNotImportantGuests(notImportantRound * 3 + 2);
            });
        }
        else
        {
            if (notImportantRound != notImportantCount / 3)
            {
                if (notImportantCount - notImportantRound * 3 == 2)
                {
                    notImportantBall_0.TriggerIn();
                    notImportantBallAdd_0.TriggerIn();
                    PlayNotImportantGuests(notImportantRound * 3);

                    DOVirtual.DelayedCall(0.8f, () =>
                    {
                        notImportantBall_2.TriggerIn();
                        notImportantBallAdd_2.TriggerIn();
                        PlayNotImportantGuests(notImportantRound * 3 + 1);
                    });
                }
                else if (notImportantCount - notImportantRound * 3 == 1)
                {
                    notImportantBall_1.TriggerIn();
                    notImportantBallAdd_1.TriggerIn();
                    PlayNotImportantGuests(notImportantRound * 3);
                }
            }
            else
            {
                if (notImportantRound == 0)
                {
                    if (notImportantCount - notImportantRound * 3 == 2)
                    {
                        notImportantBall_0.TriggerIn();
                        notImportantBallAdd_0.TriggerIn();
                        
                        PlayNotImportantGuests(notImportantRound * 3);

                        DOVirtual.DelayedCall(0.8f, () =>
                        {
                            notImportantBall_2.TriggerIn();
                            notImportantBallAdd_2.TriggerIn();
                            PlayNotImportantGuests(notImportantRound * 3 + 1);
                        });
                    }
                    else if (notImportantCount - notImportantRound * 3 == 1)
                    {
                        notImportantBall_1.TriggerIn();
                        notImportantBallAdd_1.TriggerIn();

                        PlayNotImportantGuests(notImportantRound * 3);
                    }
                }
                else
                {
                    if (notImportantCount - notImportantRound * 3 == 2)
                    {
                        notImportantBall_0.transform.DOLocalMoveX(-310, 1.2f).SetEase(Ease.InBack);
                        notImportantBallAdd_0.transform.DOLocalMoveX(-310, 1.2f).SetEase(Ease.InBack);
                        notImportantBall_1.loopMediaPlayerUI.DOFade(0, 0.8f).SetEase(Ease.InSine);
                        notImportantBallAdd_1.loopMediaPlayerUI.DOFade(0, 0.8f).SetEase(Ease.InSine);
                        notImportantBall_2.transform.DOLocalMoveX(310, 1.2f).SetEase(Ease.InBack);
                        notImportantBallAdd_2.transform.DOLocalMoveX(310, 1.2f).SetEase(Ease.InBack);

                        DOVirtual.DelayedCall(1.9f, () =>
                        {
                            PlayNotImportantGuests(notImportantRound * 3);

                            DOVirtual.DelayedCall(0.8f, () =>
                            {
                                PlayNotImportantGuests(notImportantRound * 3 + 1);
                            });
                        });
                    }
                    else if (notImportantCount - notImportantRound * 3 == 1)
                    {
                        notImportantBall_0.loopMediaPlayerUI.DOFade(0, 0.8f).SetEase(Ease.InSine);
                        notImportantBall_2.loopMediaPlayerUI.DOFade(0, 0.8f).SetEase(Ease.InSine);
                        notImportantBallAdd_0.loopMediaPlayerUI.DOFade(0, 0.8f).SetEase(Ease.InSine);
                        notImportantBallAdd_2.loopMediaPlayerUI.DOFade(0, 0.8f).SetEase(Ease.InSine);

                        PlayNotImportantGuests(notImportantRound * 3);
                    }
                }
            }
        }
    }

    private void PlayNotImportantGuests(int whichOne)
    {
        allNotImportantVIPS[whichOne].imagePerson.GetComponent<RawImage>().DOFade(1, 1.2f);
        allNotImportantVIPS[whichOne].imageSign.GetComponent<RawImage>().DOFade(1, 1).SetDelay(0.8f);
        allNotImportantVIPS[whichOne].textCompany.DOFade(1, 1).SetDelay(1.5f);
        allNotImportantVIPS[whichOne].textJob.DOFade(1, 1).SetDelay(1.8f);

        DOVirtual.DelayedCall(vipStayTime + 2.8f, () =>
        {
            FadeOutNotImportantGuests(whichOne);
        });
    }

    void FadeOutNotImportantGuests(int whichOne)
    {
        allNotImportantVIPS[whichOne].imagePerson.GetComponent<RawImage>().DOFade(0, 1.2f).SetDelay(1.3f).OnComplete(() =>
        {
            Debug.Log("whichOne : " + whichOne + " , allNotImportantVIPS.Count : " + allNotImportantVIPS.Count);

            if (whichOne == allNotImportantVIPS.Count - 1)
            {
                if (whichOne % 3 == 2)
                {
                    notImportantBall_0.TriggerOut();
                    notImportantBall_1.TriggerOut();
                    notImportantBall_2.TriggerOut();
                    notImportantBallAdd_0.TriggerOut();
                    notImportantBallAdd_1.TriggerOut();
                    notImportantBallAdd_2.TriggerOut();
                }
                else if (whichOne % 3 == 1)
                {
                    notImportantBall_0.TriggerOut();
                    notImportantBall_2.TriggerOut();
                    notImportantBallAdd_0.TriggerOut();
                    notImportantBallAdd_2.TriggerOut();
                }
                else if (whichOne % 3 == 0)
                {
                    notImportantBall_1.TriggerOut();
                    notImportantBallAdd_1.TriggerOut();
                }

                DOVirtual.DelayedCall(1.8f, () => GoToPlayMap());
            }
            else if(whichOne % 3 == 2)
            {
                //Go To Play Next Round
                notImportantRound++;
                PlayNotImportant();
            }
        });

        allNotImportantVIPS[whichOne].imageSign.GetComponent<RawImage>().DOFade(0, 1).SetDelay(1f);
        allNotImportantVIPS[whichOne].textCompany.DOFade(0, 1).SetDelay(0.2f);
        allNotImportantVIPS[whichOne].textJob.DOFade(0, 1);
    }

    public bool startFromMap = false;
    
    public void GoToPlayMap()
    {
        usefulFunc.SetAOZero(postProcessingProfile, blurImage);
        frostedGlassController.BlurCity();

        DOVirtual.DelayedCall(mapDelayTime, () =>
        {
            Debug.Log("MAP!!!!!!");
            mapMp.Control.Rewind();
            mapMp.Control.Play();
            mapMp.GetComponent<DisplayUGUI>().DOFade(1, 1.2f);
            DOVirtual.DelayedCall(mapMp.Info.GetDurationMs() / 1000.0f - 1, () =>
            {
                mapMp.GetComponent<DisplayUGUI>().DOFade(0, 1f);
                DOVirtual.DelayedCall(1f, PlayActivities);
            });
        });
        //usefulFunc.ReverseAO(postProcessingProfile, blurImage);
        //frostedGlassController.ReverseBlurCity();
    }

    public ActivitiesMainController amc;

    void PlayActivities()
    {
        amc.StartToPlayActivities();
    }
}
