using UnityEngine;

public class GetRandomPos : MonoBehaviour
{
    public Transform spawnQuad;
    public Transform randomInsideSphere;

    private Plane infinitePlane;

    private void Awake()
    {
        infinitePlane = new Plane();
        infinitePlane.SetNormalAndPosition(Vector3.up, Vector3.zero);
    }

    public Vector3 GetRandomPoint()
    {
        Vector3 spawnCenter = spawnQuad.position;
        float spawnSphereSize = randomInsideSphere.GetComponent<Collider>().bounds.size.x / 2;
        //float spawnSphereSize = randomInsideSphere.localScale.magnitude;
        Vector3 randomPointOnPlane = infinitePlane.ClosestPointOnPlane(spawnCenter + spawnSphereSize * Random.insideUnitSphere);

        return randomPointOnPlane;
    }
}