using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

[System.Serializable]
public class TrailEffectTime
{
    public float minTime;
    public float maxTime;
}

[System.Serializable]
public class TrailEffectParticles
{
    public Transform startPoint;
    public Transform endingPoint;
    public TrailRenderer trailEffect;
    public float effectTime;
}

public class CTBCParticlesController : MonoBehaviour
{
    public GetRandomPos getRandomPos;

    public GameObject GlowingObjectPrefab;
    public int glowingObjectGroupNumber = 15;
    public TrailRenderer trailPrefab;

    public TrailEffectParticles[] trailEffectParticleGroups;

    public TrailEffectTime effectRandomTime;

    // Start is called before the first frame update
    void Start()
    {
        PreGenerateGlowingObjectGroups();
        StartPlayingAllGroups();
    }

    void PreGenerateGlowingObjectGroups()
    {
        trailEffectParticleGroups = new TrailEffectParticles[glowingObjectGroupNumber];

        GameObject glowObjectParent = new GameObject();
        glowObjectParent.name = "GlowObjectParent";

        for (int i = 0; i < trailEffectParticleGroups.Length; i++)
        {
            GameObject newGroup = new GameObject();
            newGroup.name = "Group_" + i; ;
            newGroup.transform.parent = glowObjectParent.transform;

            trailEffectParticleGroups[i] = new TrailEffectParticles();

            trailEffectParticleGroups[i].startPoint = InstantiateNewGlowingObjects(newGroup.transform);
            trailEffectParticleGroups[i].startPoint.name = "GlowObjectStartPoint_" + i;

            trailEffectParticleGroups[i].endingPoint = InstantiateNewGlowingObjects(newGroup.transform);
            trailEffectParticleGroups[i].endingPoint.name = "GlowObjectEndingPoint_" + i;

            trailEffectParticleGroups[i].trailEffect = InstantiateNewTrailRenders(newGroup.transform);
            trailEffectParticleGroups[i].trailEffect.name = "TrailEffect_" + i;
            trailEffectParticleGroups[i].trailEffect.transform.position = trailEffectParticleGroups[i].startPoint.position;
        }
    }

    Transform InstantiateNewGlowingObjects(Transform parentTransform)
    {
        Transform tempSpawnGlowing = Instantiate(GlowingObjectPrefab).transform;

        Vector3 randomPosition = getRandomPos.GetRandomPoint();
        tempSpawnGlowing.position = randomPosition;
        tempSpawnGlowing.parent = parentTransform;
        Color originalColor = tempSpawnGlowing.GetComponent<Renderer>().material.color;
        tempSpawnGlowing.GetComponent<Renderer>().material.color = new Color(originalColor.r, originalColor.g, originalColor.b, 0);

        return tempSpawnGlowing;
    }

    TrailRenderer InstantiateNewTrailRenders(Transform parentTransform)
    {
        TrailRenderer tempTrail = Instantiate(trailPrefab) as TrailRenderer;

        Color originalColor = tempTrail.GetComponent<Renderer>().materials[1].color;
        tempTrail.GetComponent<Renderer>().materials[1].color = new Color(originalColor.r, originalColor.g, originalColor.b, 0);
        tempTrail.transform.parent = parentTransform;

        return tempTrail;
    }

    void StartPlayingAllGroups()
    {
        for(int i = 0; i < trailEffectParticleGroups.Length; i++)
        {
            int whichOne = i;
            DOVirtual.DelayedCall(Random.Range(0f, (effectRandomTime.minTime + effectRandomTime.maxTime)), () => StartPlayingOneGroup(whichOne));
        }
    }

    void StartPlayingOneGroup(int whichGroup)
    {
        //trailEffectParticleGroups[i].startPoint

        trailEffectParticleGroups[whichGroup].trailEffect.gameObject.SetActive(true);

        Vector3 middleWayPoint_0 = trailEffectParticleGroups[whichGroup].startPoint.localPosition + (trailEffectParticleGroups[whichGroup].endingPoint.localPosition - trailEffectParticleGroups[whichGroup].startPoint.localPosition) * 0.333f;
        Vector3 middleWayPoint_1 = trailEffectParticleGroups[whichGroup].startPoint.localPosition + (trailEffectParticleGroups[whichGroup].endingPoint.localPosition - trailEffectParticleGroups[whichGroup].startPoint.localPosition) * 0.666f;
        float startEndPointsDistance = Vector3.Distance(trailEffectParticleGroups[whichGroup].startPoint.localPosition, trailEffectParticleGroups[whichGroup].endingPoint.localPosition);
        float linePathHight = startEndPointsDistance * 0.5f * Random.Range(0.4f, 0.6f);
        middleWayPoint_0 = new Vector3(middleWayPoint_0.x, middleWayPoint_0.y + linePathHight, middleWayPoint_0.z);
        middleWayPoint_1 = new Vector3(middleWayPoint_1.x, middleWayPoint_1.y + linePathHight, middleWayPoint_1.z);

        Vector3[] wayPointGroups = new Vector3[] { trailEffectParticleGroups[whichGroup].startPoint.localPosition, middleWayPoint_0, middleWayPoint_1, trailEffectParticleGroups[whichGroup].endingPoint.localPosition };
        float randomTime = Random.Range(effectRandomTime.minTime, effectRandomTime.maxTime);

        trailEffectParticleGroups[whichGroup].trailEffect.time = randomTime * Random.Range(0.4f, 0.6f);

        trailEffectParticleGroups[whichGroup].trailEffect.transform.DOLocalPath(wayPointGroups, randomTime, PathType.CatmullRom, PathMode.Full3D, 10).SetEase(Ease.InOutCubic)
            .OnStart(() =>
        {
            trailEffectParticleGroups[whichGroup].startPoint.GetComponent<Renderer>().material.DOFade(1, trailEffectParticleGroups[whichGroup].trailEffect.time * 0.55f).SetLoops(2, LoopType.Yoyo);
            trailEffectParticleGroups[whichGroup].trailEffect.GetComponent<Renderer>().materials[1].DOFade(0.5f, 0.2f);
        })
            .OnComplete(() =>
        {
            trailEffectParticleGroups[whichGroup].endingPoint.GetComponent<Renderer>().material.DOFade(1, trailEffectParticleGroups[whichGroup].trailEffect.time * 0.55f).SetLoops(2, LoopType.Yoyo).OnComplete(() =>
            {
                DOVirtual.DelayedCall(Random.Range(0.2f, 1.2f), () => StartPlayingOneGroup(whichGroup));

                trailEffectParticleGroups[whichGroup].trailEffect.gameObject.SetActive(false);

                Vector3 randomPosition = getRandomPos.GetRandomPoint();
                trailEffectParticleGroups[whichGroup].startPoint.position = randomPosition;
                trailEffectParticleGroups[whichGroup].trailEffect.transform.position = randomPosition;

                randomPosition = getRandomPos.GetRandomPoint();
                trailEffectParticleGroups[whichGroup].endingPoint.position = randomPosition;
            });
        });
    }
}
