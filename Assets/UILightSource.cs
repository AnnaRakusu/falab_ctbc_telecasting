﻿using UnityEngine;

[ExecuteInEditMode]
public class UILightSource : MonoBehaviour {
    public void Update() {
        Shader.SetGlobalVector("_UILightDirection", transform.forward);
    }
}
