using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadSceneSmoother : MonoBehaviour
{
    public Image imageBackground;
    public Image imageLogo;

    public float fadeInTime = 1f;
    public float fadeInLogoDelay = 0.8f;
    public float fadeInLogoTime = 1.5f;

    public float fadOutTime = 1f;
    public float fadeOutImageDelay = 1.3f;
    public float fadeOutLogoTime = 1.5f;

    public bool isAutoLoad = false;

    private void Start()
    {
        Resources.UnloadUnusedAssets();
        System.GC.Collect();

        if (isAutoLoad)
            FadeInOverChangeScene("Mode_Single Guest");

        FadeOutScene();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            FadeInScene("Mode_Single Guest");
        }
    }

    public void FadeInScene(string whichSceneName)
    {
        DOTween.Kill("LoadSceneSmoother");

        imageBackground.raycastTarget = true;
        imageLogo.raycastTarget = true;

        imageBackground.DOFade(1, fadeInTime).SetEase(Ease.InSine).SetId("LoadSceneSmoother");
        imageLogo.DOFade(1, fadeInLogoTime).SetDelay(fadeInLogoDelay).SetEase(Ease.InSine).SetId("LoadSceneSmoother").OnComplete(() => FadeInOverChangeScene(whichSceneName));
    }

    public void FadeInOverChangeScene(string whichSceneName)
    {
        SceneManager.LoadScene(whichSceneName);
    }

    private void FadeOutScene()
    {
        imageLogo.DOFade(0, fadeOutLogoTime).SetEase(Ease.InSine).SetId("LoadSceneSmoother");
        imageBackground.DOFade(0, fadOutTime).SetDelay(fadeOutImageDelay).SetEase(Ease.InSine).SetId("LoadSceneSmoother").OnComplete(() =>
        {
            imageBackground.raycastTarget = false;
            imageLogo.raycastTarget = false;
        });
    }
}
