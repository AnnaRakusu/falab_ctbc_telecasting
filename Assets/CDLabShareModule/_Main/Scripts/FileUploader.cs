﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Events;

public class FileUploader : MonoBehaviour {

    public QREncodeSetter qrEncoder;
    public string uploadBasedURL = "https://future-action.com/FALab/InnisfreeActivities/?id=";

    public bool shouldUseQRCode = false;

    public RandomNameGenerator secondFile;

    public void UploadImage(byte[] bytes, string fileName)
    {
        StartCoroutine(Cor_UploadImage(bytes, fileName));
    }

    public UnityEvent nextStep;
    public bool isPNG = false;

    //public MainController mc;

    IEnumerator Cor_UploadImage(byte[] bytes, string fileName)
    {
        // Create a Web Form
        var form = new WWWForm();
        form.AddField("frameCount", Time.frameCount.ToString());
        form.AddBinaryData("file", bytes, fileName + ((isPNG) ? ".png" : ".jpg"), ((isPNG) ? "image/png" : "image/jpeg"));

        // Upload to a cgi script
        //var w = new WWW("http://future-action.com/FALab/NikeHBL107/php/Upload.php", form);
        //yield return w;

        var request = UnityWebRequest.Post("http://future-action.com/FALab/CTBCEntryWall/php/Upload.php", form);

        yield return request.SendWebRequest();

        if (request.isNetworkError)
        {
            print(request.error);
            //Application.ExternalCall("debug", w.error);
        }
        else
        {
            //print("Finished Uploading Screenshot : " + w.text);
            //Application.ExternalCall("debug", "Finished Uploading Screenshot");
            if (shouldUseQRCode)
            {
                qrEncoder.Encode(uploadBasedURL + fileName + ".jpg");
                //mc.QRCodeIn();
            }
            else
            {
                nextStep.Invoke();
            }
        }
    }

    public void InvokeNext()
    {
        nextStep.Invoke();
    }

}
