﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Threading;

[System.Serializable]
public class FALabScreenSaverProperties
{
    public int resWidth = 1920;
    public int resHeight = 1080;
    [Tooltip("It's better to set it using the depth order sequence.")]
    public Camera[] shouldRenderCameras;
    [HideInInspector]
    public int currentTakeShotNum = 0;
}

public class FALabScreenSaver : MonoBehaviour {

    public FileUploader fileUploader;
    public RandomNameGenerator rng;
    public string specifiedString = "";

    #region approach path and send to RemoveBackground.cs - Anna
    public SetWelcomePanel setWelcomePanel;
    public string originalPhotoPath;
    public string rmbgPhotoPath;
    #endregion

    #region properties

    public FALabScreenSaverProperties ssProp;
    [Space(5)]
    public bool isGIF = false;
    public bool isPNG = false;
    public bool shouldSaveToDisk = false;

    public string saveFolderName = "GameScreenShot";
    public string playerPrefRoundName = "GameRound";
    public bool useRandomName = false;
    public string currentRoundRandomName = "";
    public string eachShotPreName = "GameShot";
    private int round;
    public int Round
    {
        get
        {
            return PlayerPrefs.GetInt(playerPrefRoundName);
        }
        set
        {
            PlayerPrefs.SetInt(playerPrefRoundName, value);
            round = value;
        }
    }

    private string dateFolderPath;

    public List<Texture2D> tempList;
    public List<string> savedNameList;

    #endregion

    string fileTypeString = ".jpg";

    private void Awake()
    {
        if (isPNG)
        {
            fileTypeString = ".png";
        }

        if (fileUploader == null && GetComponent<FileUploader>())
            fileUploader = GetComponent<FileUploader>();

       CheckFolderExistsOrNotCreateOne();
    }
    
    private void CheckFolderExistsOrNotCreateOne()
    {
        string tempSubFolderName = System.DateTime.Today.Date.ToString("d");
        tempSubFolderName = tempSubFolderName.Replace("/", "-");
        dateFolderPath = saveFolderName + "/" + tempSubFolderName;
        //GameScreenShot/5-2-2017

        if (Directory.Exists(dateFolderPath) && Directory.Exists(dateFolderPath + "/Round_0"))
        {
            Round = Round + 1;
        }
        else
        {
            Round = 0;
            Directory.CreateDirectory(dateFolderPath);
        }
    }

    public void MakePhoto()
    {
        Texture2D screenShot = new Texture2D(ssProp.resWidth, ssProp.resHeight, TextureFormat.RGBA32, false);
        RenderTexture rt = new RenderTexture(ssProp.resWidth, ssProp.resHeight, 24);

        RenderCamerasToRenderTexture(ref rt);
        GetTheScreenShotAndAddToTheTexture2DList(rt, ref screenShot);
        // SaveTheFinalToTheDiskFolder(screenShot, 0);

        ssProp.currentTakeShotNum++;

        //Open the file : System.Diagnostics.Process.Start(sFileName);
    }

    private void RenderCamerasToRenderTexture(ref RenderTexture rt)
    {
        for(int i = 0; i < ssProp.shouldRenderCameras.Length; i++)
        {
            if (ssProp.shouldRenderCameras[i] && ssProp.shouldRenderCameras[i].enabled)
            {
                ssProp.shouldRenderCameras[i].targetTexture = rt;
                ssProp.shouldRenderCameras[i].Render();
                ssProp.shouldRenderCameras[i].targetTexture = null;
            }
        }
    }

    private void GetTheScreenShotAndAddToTheTexture2DList(RenderTexture rt, ref Texture2D screenShot)
    {
        // get the screenshot
        RenderTexture prevActiveTex = RenderTexture.active;
        RenderTexture.active = rt;

        screenShot.ReadPixels(new Rect(0, 0, ssProp.resWidth, ssProp.resHeight), 0, 0);
        screenShot.Apply();

        // clean-up
        RenderTexture.active = prevActiveTex;
        Destroy(rt);

        tempList.Add(screenShot);
    }

    private void SaveTheFinalToTheDiskFolder(Texture2D screenShot, int savedNumber)
    {
        //return;
        byte[] btScreenShot;
        if (isPNG)
            btScreenShot = screenShot.EncodeToPNG();
        else
            btScreenShot = screenShot.EncodeToJPG();

        //Destroy(screenShot);

        string sDirName = dateFolderPath + "/Round_" + round;
        if (!Directory.Exists(sDirName))
            Directory.CreateDirectory(sDirName);

        if (rng != null)
            currentRoundRandomName = rng.ReturnRandomName() + specifiedString;
        else
            currentRoundRandomName = setWelcomePanel.getRandomFileName;

        string sFileName;
        if (!useRandomName)
            sFileName = eachShotPreName + "_" + savedNumber;
        else
            sFileName = currentRoundRandomName;

        //Debug.Log("sFileName : " + sFileName);

        if (shouldSaveToDisk) {
            File.WriteAllBytes(sDirName + "/" + sFileName + fileTypeString, btScreenShot);
            savedNameList.Add(sFileName + fileTypeString);
        }

        if (fileUploader != null)
        {
            fileUploader.UploadImage(btScreenShot, sFileName);
        }
        
        // Anna
        originalPhotoPath = sDirName + "/" + sFileName + fileTypeString;
        rmbgPhotoPath = sDirName + "/" + sFileName + "_RMBG" + fileTypeString;
    }

    public void SaveAllTheFinalToTheDiskFolder()
    {
        for(int i = 0; i < tempList.Count; i++)
        {
            SaveTheFinalToTheDiskFolder(tempList[i], i);
        }

        //tempList.Clear();

        if (isGIF)
        {
            UseScreenShotsToGenerateGIF();
        }
    }

    public void UseScreenShotsToGenerateGIF()
    {

    }

}
