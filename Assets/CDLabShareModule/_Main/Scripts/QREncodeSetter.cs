﻿/// <summary>
/// write by 52cwalk,if you have some question ,please contract lycwalk@gmail.com
/// </summary>
/// 
/// 

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof( QRCodeEncodeController ))]
public class QREncodeSetter : MonoBehaviour {
	public QRCodeEncodeController e_qrController;
	public RawImage qrCodeImage;
	public InputField m_inputfield;
	public Text infoText;
	// Use this for initialization
	void Start () {
        if (e_qrController == null)
            e_qrController = GetComponent<QRCodeEncodeController>();
        e_qrController.onQREncodeFinished += qrEncodeFinished;//Add Finished Event
	}

	void qrEncodeFinished(Texture2D tex)
	{
		if (tex != null && tex != null) {
			int width = tex.width;
			int height = tex.height;
			float aspect = width * 1.0f / height;
			qrCodeImage.texture = tex;
		} else {

		}
	}

    public FileUploader fileUploader;

	public void Encode(string valueStr)
	{
		if (e_qrController != null) {
			int errorlog = e_qrController.Encode(valueStr);
			if (errorlog == -13) {
				Debug.Log( "Must contain 12 digits,the 13th digit is automatically added !" );

			} else if (errorlog == -8) {
                Debug.Log( "Must contain 7 digits,the 8th digit is automatically added !" );

			} else if (errorlog == -128) {
                Debug.Log( "Contents length should be between 1 and 80 characters !");

			} else if (errorlog == -1) {
                Debug.Log( "Please select one code type !" );
			}
			else if (errorlog == 0) {
                Debug.Log( "Encode successfully !" );
                fileUploader.InvokeNext();

            }
		}
	}

	public void ClearCode()
	{
		qrCodeImage.texture = null;
	}

}
