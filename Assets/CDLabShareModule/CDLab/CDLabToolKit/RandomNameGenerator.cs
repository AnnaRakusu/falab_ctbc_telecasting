﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomNameGenerator : MonoBehaviour {

    public string randomName = "";

	// Use this for initialization
	void Start () {
        randomName = CDLabToolKit.GetRandomNumberString(25);
	}

    public string ReturnRandomName()
    {
        return randomName;
    }
}
