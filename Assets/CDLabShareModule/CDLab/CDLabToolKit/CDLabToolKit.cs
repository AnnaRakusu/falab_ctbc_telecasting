﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CDLabToolKit{

    const string glyphs = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"; //add the characters you want

    public static string GetRandomNumberString(int charAmount)
    {
        string tempString = "";

        for (int i = 0; i < charAmount; i++)
        {
            tempString += glyphs[Random.Range(0, glyphs.Length)];
        }

        return tempString;
    }
	
}
