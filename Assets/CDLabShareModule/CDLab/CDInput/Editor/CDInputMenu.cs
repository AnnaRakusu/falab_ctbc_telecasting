﻿using UnityEditor;
using UnityEngine;
public class CDInputMenu : MonoBehaviour
{
    // Add a menu item named "Do Something" to MyMenu in the menu bar.
    [MenuItem("CDLab/CDInput/Create CDInput")]
    static void CreateCDInput()
    {
        GameObject cdi = new GameObject();
        cdi.AddComponent<CDInput>();
        cdi.name = "CDInput";
    }

}