﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using UnityEngine.Events;
using UnityEditor.SceneManagement;

[CustomEditor(typeof(CDInput))]
public class CDInputEditor : Editor {

    void OnEnable()
    {
        _target = (CDInput) target;
    }

    public CDInput _target;

    #region SerializedProperty
    SerializedProperty isEnabled;
    SerializedProperty isShown;
    SerializedProperty kc;
    SerializedProperty whichOneAvail;
    SerializedProperty getKeyDowns;
    SerializedProperty getKeys;
    SerializedProperty getKeyUps;

    private void registerSerializedObject()
    {
        isEnabled = serializedObject.FindProperty("isEnabled");
        isShown = serializedObject.FindProperty("isShown");
        kc = serializedObject.FindProperty("kc");
        whichOneAvail = serializedObject.FindProperty("whichOneAvail");
        getKeyDowns = serializedObject.FindProperty("getKeyDowns");
        getKeys = serializedObject.FindProperty("getKeys");
        getKeyUps = serializedObject.FindProperty("getKeyUps");
    }
    #endregion

    public override void OnInspectorGUI()
    {
        registerSerializedObject();

        //DrawDefaultInspector(); GUILayout.Space(30);
        ShowCustomEditor();

        if (GUI.changed)
        {
            if (!EditorApplication.isPlaying)
                EditorSceneManager.MarkSceneDirty(UnityEngine.SceneManagement.SceneManager.GetActiveScene());
        }

        if (EditorApplication.isPlaying)
        {
            EditorUtility.SetDirty(_target);
        }
    }

    private string[] actionStrings = new string[3] { "GetKey", "GetKeyDown", "GetKeyUp"};

    private void ShowCustomEditor()
    {
        GUILayout.Space(10);

        GUI.backgroundColor = Color.cyan;
        if (GUILayout.Button("Add Input"))
        {
            isEnabled.arraySize++;
            isShown.arraySize++;
            kc.arraySize++;
            whichOneAvail.arraySize++;
            getKeyDowns.arraySize++;
            getKeys.arraySize++;
            getKeyUps.arraySize++;
            this.serializedObject.ApplyModifiedProperties();
            this.serializedObject.Update();
            isEnabled.GetArrayElementAtIndex(isEnabled.arraySize - 1).boolValue = true;
            isShown.GetArrayElementAtIndex(isShown.arraySize - 1).boolValue = true;
            this.serializedObject.ApplyModifiedProperties();
            this.serializedObject.Update();
        }
        GUI.backgroundColor = Color.white;

        for (int i = 0; i < isEnabled.arraySize; i++)
        {
            //AddSplitLine
            GUILayout.Space(5);
            addSplitLine();
            EditorGUILayout.BeginHorizontal();
            //isEnabled
            isEnabled.GetArrayElementAtIndex(i).boolValue = EditorGUILayout.Toggle(isEnabled.GetArrayElementAtIndex(i).boolValue, GUILayout.Width(15));

            //delete
            GUI.backgroundColor = new Color(0.9f, 0.5f, 0.5f);
            if (GUILayout.Button("X", GUILayout.Width(20)))
            {
                deleteArray(ref isEnabled, i);
                deleteArray(ref isShown, i);
                deleteArray(ref kc, i);
                deleteArray(ref whichOneAvail, i);
                deleteArray(ref getKeyDowns, i);
                deleteArray(ref getKeys, i);
                deleteArray(ref getKeyUps, i);
                this.serializedObject.ApplyModifiedProperties();
                this.serializedObject.Update();
                continue;
            }
            GUI.backgroundColor = Color.white;

            if (GUILayout.Button("U", GUILayout.Width(20)))
            {
                exchangeArray(ref isEnabled, i, "up");
                exchangeArray(ref isShown, i, "up");
                exchangeArray(ref kc, i, "up");
                exchangeArray(ref whichOneAvail, i, "up");
                exchangeArray(ref getKeyDowns, i, "up");
                exchangeArray(ref getKeys, i, "up");
                exchangeArray(ref getKeyUps, i, "up");
                this.serializedObject.ApplyModifiedProperties();
                this.serializedObject.Update();
                continue;
            }
            if (GUILayout.Button("D", GUILayout.Width(20)))
            {
                exchangeArray(ref isEnabled, i, "down");
                exchangeArray(ref isShown, i, "down");
                exchangeArray(ref kc, i, "down");
                exchangeArray(ref whichOneAvail, i, "down");
                exchangeArray(ref getKeyDowns, i, "down");
                exchangeArray(ref getKeys, i, "down");
                exchangeArray(ref getKeyUps, i, "down");
                this.serializedObject.ApplyModifiedProperties();
                this.serializedObject.Update();
                continue;
            }
            //isShown
            Color showBtnColor = (isShown.GetArrayElementAtIndex(i).boolValue) ? Color.gray : Color.white;
            string showBtnString = (isShown.GetArrayElementAtIndex(i).boolValue) ? "Hide" : "Show";
            GUI.color = showBtnColor;
            if (GUILayout.Button(showBtnString, GUILayout.Width(50)))
                isShown.GetArrayElementAtIndex(i).boolValue = !isShown.GetArrayElementAtIndex(i).boolValue;
            GUI.color = Color.white;
            this.serializedObject.ApplyModifiedProperties();
            this.serializedObject.Update();

            GUI.backgroundColor = Color.cyan;
            //keycode
            EditorGUILayout.PropertyField(kc.GetArrayElementAtIndex(i), GUIContent.none);
            GUI.backgroundColor = Color.white;

            EditorGUILayout.EndHorizontal();
            //which action
            whichOneAvail.GetArrayElementAtIndex(i).intValue = GUILayout.SelectionGrid(whichOneAvail.GetArrayElementAtIndex(i).intValue, actionStrings, 3);
            this.serializedObject.ApplyModifiedProperties();

            if (!isShown.GetArrayElementAtIndex(i).boolValue)
                continue;

            //Response
            if(whichOneAvail.GetArrayElementAtIndex(i).intValue == 0)
            {
                EditorGUILayout.PropertyField(getKeys.GetArrayElementAtIndex(i), GUIContent.none);
            }
            else if(whichOneAvail.GetArrayElementAtIndex(i).intValue == 1)
            {
                EditorGUILayout.PropertyField(getKeyDowns.GetArrayElementAtIndex(i), GUIContent.none);
            }
            else if(whichOneAvail.GetArrayElementAtIndex(i).intValue == 2)
            {
                EditorGUILayout.PropertyField(getKeyUps.GetArrayElementAtIndex(i), GUIContent.none);
            }
        }
        this.serializedObject.ApplyModifiedProperties();
        this.serializedObject.Update();
    }

    private void deleteArray(ref SerializedProperty sp, int whichOne)
    {
        //if(stateType.arraySize)
        for (int i = 0; i < sp.arraySize; i++)
        {
            if (i > whichOne)
                sp.MoveArrayElement(i, whichOne);
            //tempDelay[i - 1] = original[i];
        }
        sp.arraySize--;
    }

    private void exchangeArray(ref SerializedProperty sp, int whichOne, string upOrDown)
    {
        if(upOrDown == "up")
        {
            if (whichOne == 0)
                return;
            sp.MoveArrayElement(whichOne, whichOne - 1);
        }
        else
        {
            if (whichOne == sp.arraySize - 1)
                return;
            sp.MoveArrayElement(whichOne, whichOne + 1);
        }
    }

    private void addSplitLine()
    {
        GUILayout.Box("", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });
    }

}
