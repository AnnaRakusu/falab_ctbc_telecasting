﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

[AddComponentMenu("CDLab/CDInput/Add CDInput")]
public class CDInput : MonoBehaviour {

	public bool[] isEnabled;
    public bool[] isShown;
	public KeyCode[] kc;
    public int[] whichOneAvail;
    public UnityEvent[] getKeys;
    public UnityEvent[] getKeyDowns;
	public UnityEvent[] getKeyUps;

	// Use this for initialization
	void Start () {
	
	}

    // Update is called once per frame
    void Update() {

        for (int i = 0; i < isEnabled.Length; i++)
        {
            if (!isEnabled[i])
                continue;

            if (whichOneAvail[i] == 0)
                RunGetKey(i);
            else if (whichOneAvail[i] == 1)
                RunGetKeyDown(i);
            else if (whichOneAvail[i] == 2)
                RunGetKeyUp(i);
        }

	}

    void RunGetKey(int whichOne)
    {
        if (Input.GetKey(kc[whichOne]))
            getKeys[whichOne].Invoke();
    }

    void RunGetKeyDown(int whichOne)
    {
        if (Input.GetKeyDown(kc[whichOne]))
            getKeyDowns[whichOne].Invoke();
    }

    void RunGetKeyUp(int whichOne)
    {
        if (Input.GetKeyUp(kc[whichOne]))
            getKeyUps[whichOne].Invoke();
    }
}
